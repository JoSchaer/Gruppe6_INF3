
class LevenshteinDistanz(object):


    #has to be called by two for loops in order to check all lists completely
    #as soon as it finds their distance to be under 35% it calls the change
    def calculate_distance(self, reference_word, scrambled_word):
        #the two words that we want to compare/calculate the editiondistance for
        self.__scrambled_word = scrambled_word
        self.__reference_word = reference_word
        #the size of each word for the size of the matrix
        size_ref = len(self.__reference_word)+1
        size_scram = len(self.__scrambled_word)+1
        #the initialization for the matrix for the size of the words
        matrix = [[0 for x in range(size_scram)] for y in range(size_ref)]
        #[x for x in range(1,5)]
        for r in range(size_ref):
            matrix[r][0]= r
        for s in range(size_scram):
            matrix[0][s]= s
        #compares each letter one by one to the letter of the scrambled word
        for r in range(1, size_ref):
            for s in range(1, size_scram):
                #if the letters are the same and the lowest number is chosen and set in the matrix
                #if the top or side are the smallest +1, if it's the diagonal then nothing is added
                if reference_word[r-1] == scrambled_word[s-1]:
                    #del,rep,ins
                    matrix[r][s] = min((matrix[r-1][s])+1, (matrix[r-1][s-1]), (matrix[r][s-1])+1)
                #if the letter aren't the same the minimum ist found and always +1
                else:
                    # del,rep,ins
                    matrix[r][s] = min((matrix[r-1][s])+1, (matrix[r-1][s-1])+1, (matrix[r][s-1])+1)
        #if the editor-distance divided by the length of the word is <35% the word is edited
        self.__change = False
        if matrix[size_ref-1][size_scram-1]/size_scram < .35:
            self.__change = True
        return self.__change



