from threading import Thread
from FixIt import FixIt
from Cards import Cards
import time
from LevenshteinDistanz import LevenshteinDistanz
print("Welcome to Levenshteindistanz")

reference_name_list = []
scrambled_card_list = []
all_cards = []

#writes file in given Data
def write_file(filename, list_to_write):
    file = open(filename, "w")

    for line in list_to_write:
        file.write(line.get_name() + "|")
        file.write(line.get_mana() + "|")
        file.write(line.get_cmc() + "|")
        file.write(line.get_type_card() + "|")
        file.write(line.get_count() + "\n")
    file.close()

#reads every line of reference file
def read_in_reference():
    reference_file = open("reference.txt", "r")
    for line in reference_file:
        reference_name_list.append(line)
    reference_file.close()

#reads every line of scrambled file
def read_in_scrambled():
    scrambled_file = open("scrambled.txt", "r")
    for line in scrambled_file:
        scrambled_card_list.append(line)

#reads reference data in
read_in_reference_thread = Thread(target=read_in_reference(),args=())
read_in_reference_thread.start()


#reads scrambled data in
read_in_scrambled_thread = Thread(target=read_in_scrambled(),args=())
read_in_scrambled_thread.start()
read_in_scrambled_thread.join()
read_in_reference_thread.join()

for act_scrambled_card in scrambled_card_list:
    card = Cards(act_scrambled_card)
    all_cards.append(card)

fix = FixIt()

#Non parallel fixing
fixed_cards_nonparallel = []
start = time.time()
for single_card in all_cards:
    fix.fix_it(reference_name_list,single_card)

write_file("non_parallel.txt", all_cards)
print("Timer:", time.time()-start)

'''
#Parallel fixing
thread_list = []
start = time.time()
for act_card in all_cards:
    thread_t = Thread(fix.fix_it(reference_name_list, act_card))
    thread_list.append(thread_t)
    thread_t.start()

for t in thread_list:
    t.join()

write_file("parallel.txt", all_cards)
print("Timer:", time.time()-start)
'''

