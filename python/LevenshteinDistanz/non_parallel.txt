Mighty Leap
|1|1|Vanguard|43

Cloud Cover
|{2}{W}{U}|4|Enchantment|19

Capsize
|{1}{U}{U}|3|Instant|59

Seafarer's Quay
|1|1|Land|48

Greed
|{2}{U}{U}|4|Instant|3

Kraken's Eye
|{2}|2|Artifact|27

Bring Low
|{3}{R}|4|Instant|39

Greed
|{W}|1|Sorcery|16

Leap
|{2}{R}{R}|4|Enchantment|15

Leap
|{B}|1|Instant|12

Greed
|{3}{R}|4|Sorcery|31

Enervate
|{2}{R}{R}|4|Enchantment|48

Char
|1|1|Land|21

Pox
|{1}{U}|2|Instant|5

Crash
|{2}{R}|3|Instant|40

Leap
|{4}{R}{R}|6|Sorcery|51

Greed
|{G}|1|Instant|8

Greed
|{4}{B}|5|Sorcery|58

Mirrorpool
|1|1|Land|26

Greed
|{1}{U}{B}|3|Instant|26

Greed
|{6}{B}|7|Enchantment|31

Greed
|1|1|Land|3

Pox
|{B}{B}{B}|3|Sorcery|44

Greed
|{3}{W}{W}|5|Instant|26

Enervate
|{U}|1|Enchantment|7

Nix
|{6}{B}|7|Sorcery|6

Greed
|{4}|4|Artifact|16

Greed
|{2}{B}|3|Enchantment|38

Greed
|{4}{W}|5|Instant|1

Leap
|1|1|Land|33

Splinter
|{2}{G}{G}|4|Sorcery|50

Far
|{3}{B}{B}|5|Enchantment|29

Greed
|1|1|Land|15

F??k|{R}{R}|2|Instant|29

Leap
|{1}|1|Artifact|18

Leap
|{1}{U}|2|Instant|26

Leap
|{2}{R}{R}|4|Sorcery|23

Greed
|{1}{R}|2|Sorcery|14

Seedtime
|{1}{G}|2|Instant|16

Greed
|{4}{B}|5|Sorcery|10

Fork
|{3}{W}|4|Instant|45

Far
|{U}|1|Instant|41

Far
|{3}|3|Artifact|23

Greed
|{1}{U}|2|Enchantment|15

Far
|{3}{R}{R}{R}{R}|7|Sorcery|1

Greed
|{1}{U}|2|Instant|26

Greed
|{1}{G}|2|Sorcery|7

Enervate
|{4}{W}{W}|6|Sorcery|48

Greed
|{2}{U}{U}{U}{U}|6|Enchantment|39

When
|1|1|Legendary Land|24

Pox
|{2}{R}{R}|4|Sorcery|56

Greed
|{W}{B}|2|Sorcery|39

Greed
|{1}{G}{G}|3|Sorcery|12

Greed
|{2}|2|Artifact|39

Leap
|{2}{W}|3|Instant|17

Greed
|{4}{B}{B}|6|Sorcery|57

Karoo
|{4}|4|Artifact|51

Greed
|{6}|6|Artifact|55

Greed
|1|1|Land|5

Serenity
|{1}{W}|2|Enchantment|26

Enervate
|{X}{X}{R}|1|Sorcery|5

Leap
|{2}{U}{U}|4|Legendary Enchantment Artifact|53

Greed
|{4}|4|Artifact|18

Leap
|{4}|4|Artifact|36

Leap
|{B}|1|Sorcery|20

Leap
|{B}|1|Instant|12

Greed
|{3}|3|Artifact|41

Enervate
|{1}|1|Artifact|37

Greed
|{R}{R}{G}{G}{G}{W}{W}|7|Sorcery|38

Mighty Leap
|1|1|Vanguard|40

Fiery Fall
|{5}{R}|6|Instant|11

Greed
|{2}|2|Artifact|60

Greed
|{2}{B}|3|Sorcery|60

Greed
|{1}{B}|2|Instant|21

Pox
|{1}{W}|2|Instant|60

Greed
|{1}{R}{G}|3|Artifact|35

Leap
|{1}{W}|2|Enchantment|6

Greed
|1|1|Land|60

Greed
|{1}{R}|2|Instant|30

Greed
|{1}{B}|2|Sorcery|27

Enervate
|{2}|2|Artifact|56

Leap
|{2}{U}{U}|4|Sorcery|50

Leap
|{3}{U}|4|Enchantment|44

Greed
|{3}{B}|4|Sorcery|38

Leap
|{2}{B}|3|Legendary Enchantment|43

Pox
|{2}|2|Artifact|58

Greed
|{2}{B}|3|Enchantment|58

Leap
|{2}{B}|3|Sorcery|19

Leap
|1|1|Land|28

Greed
|{0}|0|Artifact|39

Enervate
|{2}{U}|3|Enchantment|22

Swat
|{1}{B}{G}|3|Enchantment|58

Clip Wings
|{1}{G}|2|Instant|41

Pox
|{3}{W}|4|Sorcery|48

Pox
|{2}{U}|3|Instant|60

Greed
|{3}|3|Legendary Artifact|28

Leap
|{1}{W}|2|Instant|5

Morale
|{1}{W}{W}|3|Instant|57

Leap
|{G}{W}|2|Sorcery|46

Greed
|{W}|1|Sorcery|34

Leap
|{2}|2|Artifact|53

Leap
|{2}{U}|3|Legendary Enchantment|17

Greed
|{2}{U}|3|Instant|34

Greed
|{2}{W}{W}|4|Enchantment|41

Enervate
|{1}{U}{B}|3|Sorcery|26

Greed
|{3}{W}|4|Enchantment|60

Leap
|{G}|1|Instant|6

Break Through the Line
|{3}{G}|4|Enchantment|19

Greed
|{1}{W}|2|Instant|31

Jilt
|{1}{R}|2|Sorcery|50

Enervate
|{1}{R}|2|Instant|57

Tsunami
|{3}{G}|4|Sorcery|54

Inside Out
|{W}|1|Sorcery|14

Nice
|{4}|4|Artifact|25

Greed
|{2}|2|Artifact|30

Leap
|1|1|Land|52

Leap
|{X}{1}{U}|2|Instant|58

Greed
|{5}|5|Artifact|37

Leap
|{3}|3|Artifact|6

Mouth
|{G}|1|Instant|2

Greed
|{2}{R}|3|Instant|24

Leap
|{2}{W}|3|Sorcery|47

Mudslide
|{R}|1|Sorcery|27

Leap
|{2}{B}{B}|4|Enchantment|52

Greed
|{2}{G}|3|Instant|13

Greed
|{5}|5|Artifact|12

Leap
|{1}{R/G}|2|Instant|30

Pox
|{3}|3|Artifact|25

Greed
|{4}|4|Artifact|46

Greed
|{1}{G}|2|Sorcery|39

Karoo
|{2}{R}|3|Instant|9

Pox
|{1}{R}|2|Instant|46

Pox
|{5}{R}{R}|7|Sorcery|44

Leap
|{4}{B}|5|Instant|9

Far
|{X}{X}{U}|1|Sorcery|36

Greed
|{X}{4}{B}|5|Sorcery|7

Greed
|{4}|4|Artifact|28

Greed
|{1}{W}|2|Instant|10

Greed
|1|1|Land|30

Leap
|{6}|6|Artifact|15

Leap
|{1}{R}{R}|3|Sorcery|20

Char
|{3}{R}|4|Sorcery|12

Mouth
|{1}{R}|2|Instant|34

Leap
|1|1|Conspiracy|51

Feed
|{B}|1|Sorcery|19

Maze's End
|{1}{U}|2|Instant|3

Greed
|{B}|1|Instant|16

Leap
|{2}{B}|3|Instant|48

Pox
|1|1|Land|50

Char
|{6}{R}|7|Sorcery|4

Enervate
|1|1|Land|15

Flame Jet
|{1}{R}|2|Sorcery|9

Leap
|{U}|1|Enchantment|29

Char
|{2}{R}|3|Enchantment|24

Enervate
|1|1|Scheme|13

Greed
|{1}{G}{G}|3|Sorcery|42

Greed
|1|1|Land|17

Gamble
|{U}|1|Instant|24

Pox
|{2}{W}{W}{B}|5|Sorcery|22

Pox
|{1}{W}{W}|3|Enchantment|59

Greed
|{4}{R}|5|Sorcery|21

Greed
|{3}{W}|4|Sorcery|15

Leap
|{3}{W}|4|Sorcery|39

Leap
|{2}{G}|3|Sorcery|50

Leap
|{4}{B}{B}|6|Sorcery|15

Leap
|{2}{R}{R}|4|Sorcery|32

Greed
|{4}|4|Artifact|39

Greed
|{1}{R}|2|Instant|5

Mouth
|{4}|4|Artifact|17

Greed
|{3}{U}{U}|5|Enchantment|44

Greed
|{3}|3|Artifact|45

Greed
|{3}{B}|4|Instant|12

Pox
|{1}{W}{W}|3|Instant|13

Far
|{2}|2|Artifact|4

Leap
|{U}{B}|2|Instant|60

Greed
|{G}|1|Enchantment|48

Flash
|{2}{W}|3|Instant|8

Hide
|{1}{W}|2|Instant|53

?han?el|{G}{G}|2|Sorcery|21

Greed
|{G}|1|Enchantment|29

Greed
|{1}{W}|2|Instant|3

Greed
|{1}{G}|2|Enchantment|19

Nice
|{2}{W}|3|Enchantment|16

Ab?ru?|{U}|1|Instant|55

Greed
|{2}{W}|3|Enchantment|53

Far
|{1}{R}{R}|3|Instant|16

Greed
|{2}{G}|3|Enchantment|51

Leap
|{5}{G}{G}{G}|8|Sorcery|42

Greed
|{2}{B}{B}|4|Instant|45

Greed
|{1}{B}{R}|3|Instant|33

Leap
|{G}|1|Enchantment|60

Leap
|{U}|1|Sorcery|29

Mulch
|{1}{U}|2|Instant|56

Greed
|{1}|1|Artifact|22

Leap
|{R}|1|Sorcery|49

Leap
|{2}{R}{R}|4|Sorcery|9

Leap
|1|1|Snow Land|49

Far
|{6}{R}{R}|8|Sorcery|19

Greed
|{3}{B}{B}|5|Instant|41

Nice
|{W}|1|Instant|10

Greed
|{6}{B}|7|Sorcery|14

Leap
|{3}{G}|4|Sorcery|13

Greed
|{1}{G}|2|Enchantment|25

Leap
|{3}{U}{U}|5|Sorcery|16

Greed
|1|1|Land|59

Leap
|{4}|4|Artifact|5

Leap
|{B}{B}|2|Enchantment|59

Enervate
|{3}{U}{U}|5|Sorcery|14

Greed
|1|1|Land|19

Greed
|{3}{U}{B}|5|Sorcery|52

Greed
|{3}{R}{R}|5|Sorcery|34

Enervate
|{3}{U}{R}{W}|6|Instant|10

Greed
|{2}{W}|3|Instant|5

Pox
|{3}{R}{R}|5|Sorcery|22

Nix
|{X}{U}|1|Instant|41

Riches
|{5}{U}{U}|7|Sorcery|2

Seek
|{R}{R}|2|Enchantment|35

Ow
|{1}{G}|2|Instant|13

Enervate
|{3}|3|Artifact|4

Greed
|1|1|Land|36

Greed
|{3}{G}{G}|5|Instant|22

Greed
|{W}|1|Instant|39

Char
|{2}{R}{R}|4|Sorcery|39

Enervate
|{2}{B}{B}|4|Sorcery|4

Ey eSpy|{U}|1|Sorcery|33

Char
|{U}{U}|2|Enchantment|50

Enervate
|{2}{W}{W}|4|Enchantment|28

Exclude
|{2}{U}|3|Instant|29

Leap
|{2}{G}|3|Enchantment|31

Greed
|{2}{U}|3|Instant|28

Leap
|{1}{U/R}{U/R}|3|Enchantment|15

Greed
|{B}|1|Artifact|50

Greed
|{B}|1|Sorcery|22

Lecehes|{1}{W}{W}|3|Sorcery|47

Capsize
|{R}{G}|2|Instant|44

Greed
|{R}|1|Instant|36

Leap
|{U}|1|Sorcery|27

Leap
|{4}|4|Artifact|45

Greed
|{R}|1|Instant|38

Greed
|{1}{U}{U}|3|Instant|12

Odds
|1|1|Land|39

Char
|{1}{W}{W}|3|Instant|22

Leap
|{1}|1|Artifact|55

Leap
|{1}{G}|2|Instant|2

Greed
|1|1|Land|53

Enervate
|{3}{U}|4|Sorcery|42

Greed
|{1}{G}|2|Enchantment|24

Oust
|{1}{R}|2|Instant|26

Hide
|1|1|Land|51

Greed
|{3}{W}{W}|5|Enchantment|29

Leap
|{2}{B}{B}|4|Sorcery|24

Leap
|{X}{G/U}{G/U}|2|Instant|31

Enervate
|{2}{G}|3|Instant|9

Greed
|{1}{G}|2|Enchantment|1

Vitco?y|{2}{W}|3|Sorcery|8

Greed
|{3}{U}{U}|5|Sorcery|15

Swat
|{2}{G}|3|Instant|37

??rfend|{1}{W}|2|Instant|1

Enervate
|{2}{R}|3|Instant|50

Greed
|{2}{R}|3|Instant|18

Leap
|1|1|Land|13

Leap
|{X}{W}{U}|2|Instant|12

Pox
|{3}|3|Artifact|30

Leap
|{2}{R}|3|Instant|48

Lull
|{4}|4|Artifact|11

Leap
|{1}{G}|2|Instant|2

Pox
|{W}{W}|2|Enchantment|47

Char
|1|1|Land|29

Crash
|{1}{R}{R}|3|Enchantment|42

Greed
|{3}{U}|4|Instant|43

Char
|{R}{G}{W}|3|Instant|51

Greed
|{2}{R}|3|Enchantment|54

Arena
|{3}|3|Artifact|60

Jump
|{U}|1|Instant|10

Pox
|{2}{B}|3|Sorcery|55

Hide
|{5}{B}{B}|7|Instant|6

Leap
|{5}{B}|6|Instant|44

Pox
|{G}|1|Sorcery|33

Greed
|1|1|Scheme|26

Greed
|{4}{W}|5|Instant|10

Char
|{U}|1|Enchantment|25

Pox
|{6}|6|Artifact|31

Bring Low
|1|1|Land|58

Greed
|{2}{U}{U}|4|Enchantment|59

Greed
|{4}|4|Artifact|6

Fork
|{3}|3|Artifact|15

Greed
|{4}{W}{W}|6|Sorcery|20

Leap
|{1}{W}|2|Instant|46

?ush|{3}{G}|4|Sorcery|48

Enervate
|{1}|1|Artifact|10

Greed
|{2}{W}|3|Instant|1

Pox
|{4}{R}|5|Instant|32

Greed
|1|1|Vanguard|1

Pox
|{3}{R}|4|Enchantment|3

Pox
|{1}{R}|2|Sorcery|59

Pox
|{3}{R}{R}|5|Sorcery|34

Leap
|{2}{U}{U}|4|Sorcery|1

Pox
|{B}|1|Instant|12

Greed
|{3}{R}|4|Instant|34

Enervate
|{2}|2|Artifact|55

Greed
|{2}|2|Artifact|2

Providence
|{5}{W}{W}|7|Sorcery|30

P?ek|{U}|1|Instant|31

Enervate
|{X}{U}|1|Instant|15

Greed
|1|1|Land|36

Leap
|{R}|1|Sorcery|46

Leap
|{2}{R}|3|Enchantment|6

Greed
|{U}{B}{R}|3|Instant|57

Greed
|1|1|Ongoing Scheme|48

Greed
|{4}{R}{G}|6|Sorcery|9

Enervate
|{2}{U}{U}|4|Instant|24

Greed
|{W}|1|Instant|36

Greed
|{X}{R}{R}|2|Sorcery|8

Far
|{2}{B}{B}{B}|5|Sorcery|40

Waylay
|{2}{W}|3|Instant|58

Greed
|{3}{B}{B}|5|Enchantment|2

Greed
|{1}{R}|2|Instant|12

Greed
|1|1|Land|33

Enervate
|{2}|2|Artifact|55

Greed
|1|1|Land|51

When
|{2}{G}|3|Sorcery|54

Tolaria
|{2}{U}{U}|4|Instant|47

Mouth
|1|1|Land|35

Rsetock|{3}{G}{G}|5|Sorcery|51

Leap
|{3}|3|Artifact|20

Greed
|{4}{U}{U}|6|Instant|21

Greed
|{2}{W}|3|Sorcery|17

Leap
|{2}{G}{G}|4|World Enchantment|22

Greed
|{5}{U/R}{U/R}|7|Sorcery|6

Greed
|{2}{W}|3|Enchantment|24

Leap
|{B}|1|Instant|48

Greed
|{B}|1|Instant|41

Enervate
|1|1|Land|22

Greed
|{2}{R}|3|Sorcery|45

Greed
|{2}{G}|3|Enchantment|60

Greed
|{G}{U}{R}|3|Instant|34

Capsize
|{2}{W}|3|Enchantment|30

Leap
|{1}{G}|2|Instant|12

Pox
|{2}{U}{B}|4|Enchantment|55

Greed
|{R}|1|Sorcery|51

Pox
|{U}|1|Sorcery|25

Leap
|1|1|Land|42

Leap
|{1}{R}|2|Sorcery|30

Greed
|{G}|1|Sorcery|37

Far
|{2}|2|Artifact|17

Enervate
|{1}{W}|2|Enchantment|42

Boil
|{3}{R}|4|Instant|28

Pox
|{2}{G}|3|Sorcery|16

Greed
|{2}{G}|3|Sorcery|32

Leap
|{2}{W}{W}|4|Sorcery|39

Pox
|{3}{U}|4|Sorcery|54

Ertai
|{2}{W}|3|Instant|7

Pox
|{1}{R}|2|Instant|39

Greed
|{1}{R}|2|Instant|59

Temporary Truce
|{2}{W}|3|Instant|50

Enervate
|{4}{R}|5|Enchantment|42

Char
|{1}|1|Artifact|48

Portent
|{3}{U}|4|Instant|52

Greed
|{3}{U}|4|Sorcery|10

??umb?e|{G}|1|Instant|44

Greed
|{3}{B}|4|Instant|14

Greed
|{3}{R}{R}|5|Enchantment|51

Jilt
|{W}|1|Instant|16

Enervate
|1|1|Legendary Land|58

Greed
|{3}{B}|4|Instant|5

Enervate
|{4}{U}{U}|6|Sorcery|46

Greed
|{1}{W}|2|Instant|32

Enervate
|{2}{R}{W}|4|Enchantment|9

No Mercy
|{2}{W}{W}|4|Snow Enchantment|10

Leap
|{2}{G}{G}|4|Sorcery|8

Enervate
|{W}|1|Enchantment|18

Leap
|{2}{U}{R}|4|Instant|5

Greed
|{3}{W}{W}|5|Instant|44

Leap
|{1}{G}|2|Enchantment|1

Greed
|{1}{W}|2|Sorcery|47

Leap
|{2}{G}|3|Instant|40

Simoon
|{2}{B}|3|Enchantment|16

Arena
|{4}{W}|5|Instant|53

Greed
|{1}{G}|2|Enchantment|27

Greed
|{2}{G}{G}|4|Sorcery|3

Enervate
|{2}|2|Artifact|22

Leap
|1|1|Vanguard|25

Greed
|{U}{U}{R}|3|Instant|7

Breakthrough
|{1}{R}|2|Sorcery|24

Mise
|{1}{R}|2|Sorcery|32

Greed
|{2}{W}{W}|4|Sorcery|28

Far
|{R}|1|Sorcery|26

Leap
|{3}{U}{U}|5|Enchantment|25

Greed
|{4}{U}{U}|6|Sorcery|51

Leap
|{1}{U}|2|Instant|49

Leap
|{1}{R}|2|Instant|52

Enervate
|1|1|Land|10

Ovinize
|{1}{U}|2|Instant|58

Pox
|{1}|1|Artifact|29

Leap
|{R}{W}{B}|3|Instant|58

Enervate
|{4}{W}{W}|6|Enchantment|22

Pox
|1|1|Conspiracy|22

Pox
|{3}|3|Artifact|36

Greed
|{3}|3|Artifact|12

Omen
|{3}{B}{R}|5|Sorcery|6

Leap
|{5}{R}{R}|7|Sorcery|49

Enervate
|1|1|Vanguard|18

Greed
|{2}{G}{G}|4|Sorcery|42

Pure
|{1}{B}{R}{G}|4|Sorcery|5

Char
|{0}|0|Artifact|46

Leap
|{1}{G}|2|Enchantment|13

Greed
|{2}{U}{U}|4|Sorcery|8

Pox
|{4}{R}|5|Sorcery|21

Greed
|{U}{U}|2|Instant|2

Greed
|{3}{W}|4|Instant|26

Pox
|{B}|1|Sorcery|8

Leap
|{R}|1|Instant|51

Enervate
|{7}|7|Instant|57

Hide
|{3}{B}{R}|5|Instant|54

Leap
|{1}{W}|2|Enchantment|11

Zap
|{U}|1|Enchantment|1

Greed
|1|1|Land|19

Leap
|{2}{G}|3|Instant|54

Greed
|{3}|3|Artifact|39

Greed
|{1}{U}|2|Sorcery|56

Enervate
|{W}|1|Instant|37

Greed
|{2}{U}|3|Sorcery|9

Leap
|{3}{G}|4|Instant|17

Greed
|{2}{G}{G}|4|Enchantment|26

Greed
|{3}{W}|4|Sorcery|32

Leap
|{2}{U}{B}{R}|5|Sorcery|27

Greed
|{2}{R}|3|Enchantment|18

Leap
|{2}{U}|3|Sorcery|43

Greed
|{3}|3|Artifact|7

Greed
|{G}|1|Sorcery|26

Greed
|{2}{U}|3|Instant|36

Greed
|{X}{2}{U}{B}|4|Sorcery|34

Mighty Leap
|1|1|Vanguard|11

Pox
|{U}|1|Sorcery|6

Leap
|{2}|2|Artifact|9

Leap
|{3}{G}|4|Sorcery|8

Greed
|{3}|3|Artifact|27

Greed
|{4}{U/B}|5|Sorcery|55

Arena
|{1}{G}|2|Sorcery|17

Far
|1|1|Land|30

Greed
|{3}{G}{G}|5|Instant|11

Greed
|{U}|1|Instant|36

Leap
|{3}{G}|4|Sorcery|57

Greed
|{2}{R}|3|Enchantment|34

Greed
|{B}|1|Enchantment|40

Greed
|{B}{B}|2|Enchantment|23

Pox
|{W}|1|Instant|55

Release
|{4}{R}{W}|6|Sorcery|46

Greed
|{3}{W}|4|Enchantment|10

Greed
|{2}{R}{R}|4|Sorcery|45

Greed
|{7}{W}|8|Sorcery|23

Leap
|{1}{W}|2|Instant|50

Greed
|1|1|Land|1

Leap
|1|1|Land|4

Char
|{U}|1|Sorcery|40

Hit
|{1}{G}|2|Instant|9

Sift
|{1}{R}|2|Instant|6

Leap
|{B}|1|Instant|45

Greed
|{4}{R}|5|Instant|6

Greed
|{2}{R}|3|Sorcery|50

Enervate
|{1}|1|Artifact|36

Dramatic Rescue
|{U}|1|Instant|30

Leap
|{X}{U}{U}|2|Sorcery|25

Greed
|{1}{G}|2|Enchantment|19

Greed
|{3}|3|Artifact|44

Leap
|{X}{B}|1|Sorcery|54

Pox
|{4}{B}|5|Enchantment|55

Leap
|{G/P}|1|Instant|29

Leap
|{3}|3|Artifact|46

Greed
|{W}|1|Instant|41

Mult??i|1|1|Vanguard|59

Pox
|1|1|Land|53

Leap
|{W}|1|Sorcery|60

Greed
|1|1||10

Greed
|{1}{G}|2|Sorcery|17

Greed
|1|1|Land|30

Greed
|{2}|2|Artifact|9

Greed
|{7}{U}|8|Sorcery|5

Greed
|{W}|1|Instant|54

Mouth
|{X}{R}{W}|2|Sorcery|16

Greed
|{2}|2|Artifact|30

Greed
|{W}|1|Enchantment|20

Leap
|{6}|6|Artifact|25

Pox
|{U}{U}|2|Instant|5

Greed
|{W}|1|Sorcery|21

Greed
|{3}|3|Artifact|49

Greed
|{4}|4|Artifact|17

Greed
|{4}{R}{R}|6|Enchantment|57

Greed
|{R}{G}|2|Instant|45

Greed
|{2}{R}|3|Sorcery|52

Enervate
|{3}{R}{R}{G}{G}|7|Sorcery|35

Greed
|{1}{W}|2|Sorcery|16

Leap
|{3}{B}|4|Sorcery|23

Leap
|{2}{G}{G}{G}|5|Instant|46

Greed
|{3}|3|Artifact|38

Greed
|{3}{U}|4|Instant|45

Greed
|{1}{R}|2|Enchantment|23

Greed
|1|1|Land|36

Greed
|{1}|1|Artifact|2

Fight
|{1}{R/G}|2|Instant|57

Leap
|{1}{B}|2|Sorcery|13

Greed
|{W}|1|Instant|25

Swat
|{X}{U}|1|Instant|35

Leap
|{1}{R}{R}|3|Sorcery|34

Greed
|{3}{B}{B}|5|Sorcery|42

Arena
|{X}{B}|1|Sorcery|2

Greed
|1|1|Land|41

Greed
|{2}{U}|3|Sorcery|36

Greed
|{3}{G}|4|Enchantment|54

Greed
|{3}{B}|4|Enchantment|9

Pox
|{1}{U}|2|Instant|33

Greed
|{B}{B}|2|Instant|39

Greed
|{1}{B}|2|Sorcery|9

Enervate
|1|1|Land|53

Greed
|{3}{G}{W}|5|Instant|20

Mise
|{1}{B}{B}|3|Sorcery|25

Implode
|{4}{R}|5|Sorcery|33

Pox
|{2}{U}|3|Instant|38

Pox
|{4}{R}|5|Sorcery|20

Snag
|{4}{B}{R}|6|Sorcery|22

Greed
|1|1|Legendary Land|46

Pox
|{4}|4|Artifact|2

Greed
|{W}|1|Enchantment|49

Leap
|{2}{G}|3|Instant|53

Leap
|{W}|1|Instant|37

Greed
|{2}{U}{U}|4|Sorcery|23

Greed
|{2}{G}|3|Instant|44

Pox
|{7}{W}{W}|9|Sorcery|21

Leap
|{1}{R}{R}|3|Instant|14

Greed
|{3}{U/P}|4|Sorcery|56

Leap
|{4}|4|Artifact|1

Greed
|{6}{U}{U}|8|Instant|31

Greed
|{2}{W}|3|Instant|18

Enervate
|{1}|1|Artifact|59

Leap
|{X}{G}|1|Instant|25

Greed
|1|1|Land|34

Le?d|{3}{G}|4|Sorcery|37

Greed
|{1}{B}|2|Enchantment|16

Leap
|{W}|1|Instant|18

Pox
|1|1|Land|13

Leap
|1|1|Land|33

Flare
|{W}|1|Instant|39

Greed
|1|1|Land|36

Pox
|{1}{U}|2|Instant|27

Greed
|{B}|1|Sorcery|48

Greed
|{5}|5|Artifact|58

Inicte|{R}|1|Instant|36

Mouth
|{2}{G}{G}|4|Sorcery|37

Greed
|1|1|Land|40

Leap
|{5}|5|Artifact|49

Char
|{B}{B}|2|Instant|21

Greed
|{3}|3|Artifact|50

Greed
|{R/G}|1|Instant|42

Greed
|{G}|1|Sorcery|22

Greed
|1|1|Land|11

Leap
|{R}|1|Instant|46

Pox
|1|1|Land|19

Leap
|{1}{U}|2|Instant|18

Char
|{3}|3|Artifact|58

Greed
|{1}{G}|2|Instant|33

Leap
|{2}{B}|3|Instant|60

Greed
|{2}{U}|3|Sorcery|46

Greed
|{5}{W}{W}|7|Sorcery|52

Greed
|{1}{R}|2|Legendary Enchantment|35

Oust
|{2}{U}|3|Instant|40

Greener Pastures
|{B}|1|Sorcery|29

Greed
|{1}{R}{W}|3|Enchantment|53

Death Stroke
|{2}{R}|3|Enchantment|6

Greed
|{B}{G}{W}|3|Enchantment|27

Greed
|{2}{W}|3|Legendary Enchantment|49

Leap
|{3}{R}|4|Sorcery|39

Leap
|{5}|5|Legendary Artifact|18

Char
|{1}{U}{B}|3|Sorcery|56

Pox
|{1}{W}{W}{U}|4|Instant|35

Greed
|{3}{G}|4|Sorcery|39

Greed
|{U}|1|Sorcery|59

Pox
|{1}{B}{B}|3|Enchantment|4

Leap
|{R}{G}{W}|3|Enchantment|2

Pox
|{4}|4|Artifact|14

Desipse|{B}|1|Sorcery|2

Leap
|{1}{U}|2|Instant|15

Greed
|{4}|4|Artifact|38

Leap
|{2}|2|Artifact|29

Leap
|{2}{U}|3|Sorcery|50

Greed
|{4}{W}{W}|6|Sorcery|53

Greed
|{3}|3|Artifact|41

Far
|{5}|5|Artifact|45

Tremble
|{1}{R}|2|Sorcery|22

Leap
|{2}|2|Artifact|50

Leap
|{3}{U}|4|Instant|49

Enervate
|{4}{B}|5|Enchantment|17

Greed
|{W}|1|Enchantment|55

Oust
|{1}{R}|2|Instant|7

Greed
|{4}{U}{U}|6|Sorcery|7

Ow
|{W}|1|Instant|26

Barrin
|1|1|Vanguard|30

Greed
|{1}{B}|2|Enchantment|60

Loss
|{2}{B}|3|Instant|38

Enervate
|{1}{U}{U}{B}|4|Instant|51

Ow
|{4}{U}|5|Instant|33

Enervate
|1|1|Land|56

Greed
|{R}|1|Instant|7

Greed
|{2}{W}|3|Enchantment|13

Trial of Solidarity
|{3}{W}|4|Instant|58

Leap
|{4}{G}|5|Sorcery|8

Greed
|{1}{R}|2|Instant|35

Greed
|{3}|3|Artifact|16

Pox
|{3}{U}{U}|5|Instant|36

Leap
|{2}{U}|3|Instant|22

Greed
|{5}|5|Artifact|24

Enervate
|{3}|3|Artifact|21

Greed
|{B}|1|Instant|51

Pox
|{3}{G/P}|4|Artifact|49

Greed
|{W}|1|Instant|39

Greed
|{4}{U}|5|Sorcery|55

Leap
|{B}|1|Sorcery|17

Greed
|{G}|1|Instant|56

Greed
|{1}{R}|2|Instant|52

Mighty Leap
|1|1|Legendary Land|57

Greed
|{1}{G}|2|Enchantment|57

Leap
|{2}{U}|3|Instant|31

Swat
|{X}{B}|1|Sorcery|47

Pox
|{1}{G}|2|Instant|1

Greed
|{U}|1|Instant|59

Char
|{1}{W}|2|Instant|2

Enervate
|{2}{W}|3|Sorcery|42

Greed
|{2}{R}|3|Instant|13

Greed
|{1}{R}|2|Sorcery|4

Greed
|1|1|Land|5

Leap
|{1}{R}|2|Instant|12

Down
|{1}|1|Artifact|8

Greed
|{2}{W}{W}|4|Enchantment|9

Enervate
|{4}{G}|5|Enchantment|45

Greed
|{2}{U}{U}|4|Sorcery|4

Flxu|{2}{U}|3|Sorcery|21

Leap
|{2}|2|Artifact|17

Greed
|{X}{R}|1|Sorcery|18

Far
|{4}{U}|5|Enchantment|10

Greed
|{3}|3|Artifact|39

Greed
|{2}|2|Artifact|45

Greed
|{1}{W}|2|Enchantment|14

Leap
|{2}{R}{R}|4|Enchantment|52

Greed
|{R}|1|Instant|46

Hide
|{X}{1}{B}|2|Sorcery|25

Greed
|{1}{G}|2|Enchantment|51

Greed
|1|1|Land|54

Greed
|{5}{U}|6|Sorcery|26

Pox
|{3}{G}|4|Sorcery|35

Leap
|{3}{G}|4|Sorcery|59

Leap
|{4}|4|Artifact|19

Leap
|1|1|Land|60

Greed
|{1}{G}|2|Sorcery|26

Pox
|{1}{B}{B}|3|Enchantment|5

Greed
|{2}{G}{G}|4|Enchantment|50

Karoo
|{1}|1|Artifact|52

Leap
|{3}|3|Artifact|7

Leap
|{3}{U}{U}|5|Enchantment|30

Greed
|{5}|5|Artifact|56

Pox
|{4}{R}|5|Enchantment|32

Leap
|{4}|4|Artifact|12

Greed
|{G}|1|Enchantment|39

Pox
|1|1|Land|55

Greed
|{4}|4|Artifact|19
