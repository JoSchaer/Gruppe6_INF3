
class Cards(object):


    #each card is saved the same way, the line is read in and all attributes are giving their values
    def __init__(self, line_information):
        self.__name, self.__mana, self.__cmc, self.__type_card, self.__count = line_information.split("|")

    def get_name(self):
        return self.__name

    def set_name(self, name):
        self.__name = name

    def get_mana(self):
        return self.__mana

    def get_cmc(self):
        return self.__cmc

    def get_type_card(self):
        return self.__type_card

    def get_count(self):
        return self.__count

    def get_information(self):
        return self.__information
