from Cards import Cards
from LevenshteinDistanz import LevenshteinDistanz

class FixIt(object):

    __fixed_cards = []
    __new_name=''
    __stop = ''

    #replace scrambled name with new correct name of the objekt by using levenshteinDistance
    def fix_it(self, reference_list, card):
        ld = LevenshteinDistanz()
        print("This is the scrambled name", card.get_name())
        card_name = 0
        self.__stop = True

        while card_name in range(len(reference_list)):
            if self.__stop:
                if ld.calculate_distance(reference_list[card_name], card.get_name()):
                    self.__new_name = reference_list[card_name]
                    card.set_name(self.__new_name)
                    self.__stop = False
                    print("This is the fixed name", card.get_name())
            card_name = card_name+1
