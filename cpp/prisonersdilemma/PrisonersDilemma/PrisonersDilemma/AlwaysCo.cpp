#include "stdafx.h"
#include "AlwaysCo.h"
#include "Agent.h"
#include <string>
using namespace std;

AlwaysCo::AlwaysCo()
{
	this ->setName("AlwaysCoorporate");
}


AlwaysCo::~AlwaysCo()
{
}


//Always Cooperates 
bool AlwaysCo::next()
{
	return true;
}

//last round decision of other Agent in lastDecicionOp
void AlwaysCo::result(bool other)
{
	this->setLastDecicionOp(other);
}
