#pragma once
#include "Agent.h"

class AlwaysDe: public Agent {
	
public:
	AlwaysDe();
	~AlwaysDe();
	bool next();
	void result(bool other);

};

