#include "stdafx.h"
#include "AntiOp.h"
#include "Agent.h"
#include <string>
using namespace std;

AntiOp::AntiOp()
{
	this->setName("AntiOpponent");
	this->setLastDecicionOp(true);
}


AntiOp::~AntiOp()
{
}

//Always decide for Opposite like other Agent last round
bool AntiOp::next()
{
	return (!this->getLastDecicionOp());
}

void AntiOp::result(bool other)
{
	this->setLastDecicionOp(other);
}
