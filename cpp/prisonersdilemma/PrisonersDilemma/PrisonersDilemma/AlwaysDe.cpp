#include "stdafx.h"
#include "AlwaysDe.h"
#include "Agent.h"
#include <string>
using namespace std;

AlwaysDe::AlwaysDe()
{
	this->setName("AlwaysDefect");
}


AlwaysDe::~AlwaysDe()
{
}

//Alway defects
bool AlwaysDe::next()
{
	return false;
}


void AlwaysDe::result(bool other)
{
	this->setLastDecicionOp(other);
}
