#include "stdafx.h"
#include "LikeOp.h"
#include "Agent.h"
#include <string>
using namespace std;

LikeOp::LikeOp()
{
	this->setName("LikeOpponent");
	this->setLastDecicionOp(true);

}


LikeOp::~LikeOp()
{
}

//Always does same as other Agent in last round
bool LikeOp::next()
{
	return this ->getLastDecicionOp();
}

void LikeOp::result(bool other)
{
	this->setLastDecicionOp(other);
}
