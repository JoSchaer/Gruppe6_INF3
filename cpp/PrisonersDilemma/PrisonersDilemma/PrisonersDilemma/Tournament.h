#pragma once
#include "Agent.h"
#include <vector>

using namespace std;


class Tournament
{

private:
	int numberOfRounds;

	vector <Agent*> agents;
	vector <int> roundResultsFirstAgent;
	vector <int> roundResultsSecondAgent;

	const int resultTrueTrue[2] = { -2,-2 };
	const int resultFalseFalse[2] = { -4,-4 };
	const int resultTrueFalse[2] = { -1,-6 };
	const int resultFalseTrue[2] = { -6,-1 };

	void setRoundresults(int resultA1, int resultA2);
	void winnerList();
	void roundWinner(Agent* agent1, Agent* agent2);

public:
	Tournament();
	Tournament(int numberOfRounds,vector<Agent*> agents);
	~Tournament();
	void run();
	void compete(int n, Agent* firstAgent, Agent* secondAgent);

};

