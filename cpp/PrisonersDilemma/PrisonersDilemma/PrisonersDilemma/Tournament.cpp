#include "stdafx.h"
#include "Tournament.h"
#include "Agent.h"
#include "AlwaysCo.h"
#include "AlwaysDe.h"
#include "AntiOp.h"
#include "Change.h"
#include "LikeOp.h"
#include <iostream>

using namespace std;
Tournament::Tournament()
{
}

Tournament::Tournament(int numberOfRounds,vector<Agent*> agents) {
	if (agents.size() < 2) {
		cout << "There are not enough Agents inside. Can't start the Game" << endl;
	}
	else {
		
		this->agents = agents;
		this->numberOfRounds = numberOfRounds;
		run();
	}
}


Tournament::~Tournament()
{
}

void Tournament::run() 
{
	//iterate over the agents vector and match every agent to each other

	for (int i = 0; i < this->agents.size(); i++) {
		
			for (int j = 0; j < this->agents.size(); j++) {

				cout << this->agents.at(i)->getName();
				cout << "	VS	";
				cout << this->agents.at(j)->getName() << endl;
				cout << "\n";

				compete(numberOfRounds, this->agents.at(i), this->agents.at(j));
			}
	}
	winnerList();
}

void Tournament::compete (int n, Agent* firstAgent, Agent* secondAgent)
{
	
	//repeat grilling n times
	for (int i = 0; i < n; i++) {
		//Decision of the first Agent
		bool isFirstAgentDefect = firstAgent->next();
		

		//Decision of te Second Agent
		bool isSecondAgentDefect = secondAgent->next();


		//Both Agents took same decision
		if (isFirstAgentDefect == isSecondAgentDefect) 
		{

			//Agents cooperate
			if (!isFirstAgentDefect) {
				setRoundresults(-2, -2);
				firstAgent->setRoundCount(-2);
				secondAgent->setRoundCount(-2);
			}
			else {
				setRoundresults(-4, -4);
				firstAgent->setRoundCount(-4);
				secondAgent->setRoundCount(-4);
			}
		}
		else 
		{
			if (!isFirstAgentDefect) {
				setRoundresults(-1, -6);
				firstAgent->setRoundCount(-1);
				secondAgent->setRoundCount(-6);
			}
			else {
				setRoundresults(-6, -1);
				firstAgent->setRoundCount(-6);
				secondAgent->setRoundCount(-1);
			}

			
		}
		
		firstAgent->result(isSecondAgentDefect);
		secondAgent->result(isFirstAgentDefect);
	}

	

	//Console Output
	
	for (int j = 0; j < roundResultsFirstAgent.size(); j++) 
	{
		
		cout << roundResultsFirstAgent.at(j);
		cout << "	|	";
		cout << roundResultsSecondAgent.at(j) << endl;
		firstAgent->setCounter(roundResultsFirstAgent.at(j));
		secondAgent->setCounter(roundResultsSecondAgent.at(j));
		
	}
	roundWinner(firstAgent, secondAgent);
	
	firstAgent->clearRoundCount();
	secondAgent->clearRoundCount();

	roundResultsFirstAgent.clear();
	roundResultsSecondAgent.clear();
	
	cout << "\n\n";

}


void Tournament::setRoundresults(int resultA1, int restulA2) {
	roundResultsFirstAgent.push_back(resultA1);
	roundResultsSecondAgent.push_back(restulA2);
}


void Tournament::roundWinner(Agent* agent1, Agent* agent2) 
{
	if (agent1 != agent2) {
		cout << agent1->getRoundCount();
		cout << "	|	";
		cout << agent2->getRoundCount() << endl;
	}
	else
	{
		cout << agent1->getRoundCount() / 2;
		cout << "	|	";
		cout << agent2->getRoundCount() /2 << endl;
	}
	
}
void Tournament::winnerList() 
{
	//Internet https://www.geeksforgeeks.org/bubble-sort/
	for (int i = 1; i < agents.size(); i++) {
		for (int j = 0; j < agents.size() - i; j++) {
			if (agents.at(j)->getCounter() < agents.at(j + 1)->getCounter()) {
				Agent* temp = agents.at(j);
				agents.at(j) = agents.at(j + 1);
				agents.at(j + 1) = temp;
			}
		}
	}
	

	for (int  k = 0; k < this->agents.size(); k++)
	{
		cout << this->agents.at(k)->getName();
		cout << " have ";
		cout << this->agents.at(k)->getCounter();
		cout << " points." << endl;
	}
}
/*
	argc -> numberOfItems in *argv[]
	*argv[] vector wit pointer to strings
*/
int main(int argc, char *argv[]) {

	int rounds =(int)*argv[1] - '0';
	
	
	AlwaysCo* alwaysCo = new AlwaysCo();
	AlwaysDe* alwaysDe = new AlwaysDe();
	AntiOp* antiOp = new AntiOp();
	Change* change = new Change();
	LikeOp* likeOP = new LikeOp();

	vector<Agent*> agents;
	agents.push_back(alwaysCo);
	agents.push_back(alwaysDe);
	agents.push_back(antiOp);
	agents.push_back(change);
	agents.push_back(likeOP);

	Tournament* tournament = new Tournament(rounds, agents);
	cout << "\n\nProgram Done. Press Any Key to left.";
	cin.get();
	delete tournament;
	delete alwaysCo;
	delete alwaysDe;
	delete antiOp;
	delete change;
	delete likeOP;
	return 0;
}
