#include "stdafx.h"
#include "Agent.h"

void Agent::setLastDecicionOp(bool lastDC)
{
	this->lastDecicionOp = lastDC;
}

bool Agent::getLastDecicionOp()
{
	return this ->lastDecicionOp;
}

void Agent::setName(string name)
{
	this->name = name;
}

//setter for points of Agent increases direct
void Agent::setCounter(int count)
{
	this->counter  += count;
}


int Agent::getCounter()
{
	return this->counter;
}


string Agent::getName()
{
	return this->name;
}

void Agent::setRoundCount(int value) {
	this->roundCount += value;
}

int Agent::getRoundCount()
{
	return this ->roundCount;
}

void Agent::clearRoundCount()
{
	roundCount = 0;
}

