#pragma once
#include <string>
using namespace std;

class Agent
{

private:
	bool lastDecicionOp;
	int counter = 0;
	string name;
	int roundCount = 0;
public:
	void setLastDecicionOp(bool lastDC);
	bool getLastDecicionOp();
	void setName(string name);
	void setCounter(int count);
	void setRoundCount(int value); //for results after every round
	int getRoundCount();
	void clearRoundCount();
	int getCounter();
	string getName();

	//virtual methods implemented in other Agent Classes
	virtual bool next() = 0; 
	virtual void result(bool other) = 0;
};

