#include "stdafx.h"
#include "Cell.h"
#include <vector>
using namespace std;

class Cell
{
	int x,y, counter;
	bool alive;
	Cell *self;

	vector<Cell> neighbours(Cell &cell)
	{
		vector<Cell> directNeighbours;
		int xMin, xMax, yMin, yMax;

		if (width() == 0)
		{
			xMin = 0;
		}
		else if (height() == 0)
		{
			yMin == 0;
		}
		else if (width() == x+1 )
		{
			xMax = width();
		}
		else if (height() == x+1)
		{
			yMax = height();
		}
		else
		{
			xMin = x - 1;
			xMax = x + 1;
			yMin = y - 1;
			yMax = y + 1;
		}

		for (int i = xMin; i = xMax; i++)
		{
			for (int j = yMin; j = yMax; j++)
			{
				directNeighbours.push_back(Cell(i,j));
			}
		}
		return directNeighbours;
	}

	bool Cell::next()
	{
		int counter(0);
		vector<Cell> neighbourAlive = neighbours(*self);
		for (int h = 0; h <= neighbourAlive.size; h++)
		{
			if (alive)
			{
				counter++;
			}
		}
		if (counter < 2 || counter > 3)
		{
			setAlive(false);
		}
		else if (counter > 3)
		{
			setAlive(true);
		}
		else
		{
			setAlive(true);
		}
		return alive;
	}

	std::vector<Cell> Cell::neighbour()
	{
		return std::vector<Cell>();
	}
	Cell::Cell(){}
	Cell::Cell(int width, int height){
		this->x =x;
		this->y=y;
	}
	Cell::Cell(int width, int height, bool alive)
	{
		this->x = width;
		this->y = height;
		this->alive = alive;
	}


	Cell::~Cell(){}

	void Cell::setAlive(bool nextAlive)
	{
		this->alive = nextAlive;
	}
	bool Cell::isAlive(int x, int y)
	{
		if (alive)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	int Cell::width(){
		return x;
	}
	int Cell::height(){
		return y;
	}
	void Cell::setWidth(int width){
		this->x =width;
	}
	void Cell::setHeight(int height){
		this->y =height;
	}
};
