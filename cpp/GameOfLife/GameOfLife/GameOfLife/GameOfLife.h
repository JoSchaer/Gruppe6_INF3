
#include "Cell.h"

class GameOfLife{
    private:
	char width, height, possibility;
    Cell ** board;

    public:
    void update();
    void draw();
    GameOfLife();
    GameOfLife(char width, char height, char possibility);
    ~GameOfLife();
    
};