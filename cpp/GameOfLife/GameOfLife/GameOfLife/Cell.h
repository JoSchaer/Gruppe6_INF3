#include <vector>

using namespace std;

class Cell
{

private:
	int x, y, xMin, xMax, yMin, yMax;
	bool alive;
	vector<Cell> neighbourAlive;
	int counter = 0;
	Cell *self;
public:
	Cell();
	Cell(int width, int height);
	Cell(int width, int height, bool alive);
	~Cell();
    bool next();
    vector<Cell>neighbours(Cell &cell);
	void setAlive(bool nextAlive);
	bool isAlive(int x, int y);
	int width();
	int height();
	
};