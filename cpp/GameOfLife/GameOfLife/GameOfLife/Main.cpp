// GameOfLife.cpp: Definiert den Einstiegspunkt für die Konsolenanwendung.
//
#include "stdafx.h"
#include "Cell.h"
#include "GameOfLife.h"
#include <iostream>
#include <thread>
#include <chrono>
#include <stdlib.h>

using namespace std;

int main(int argc, char *argv[])
{
	if (argc < 4)
	{
		GameOfLife game (*argv[1], *argv[2], *argv[3]);
		//Draw mehtod has to updated first then this can be fixed
		game.draw();
		game.update();
		//Programm sleep 2 Seconds
		std::this_thread::sleep_for(2s);
	}
	else
	{
		cout << "You entered too many numbers. Reduce to three." << endl;
		return 1;
	}
	return 0;
};
