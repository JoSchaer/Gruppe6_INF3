note
	description : "project application root class"
	date        : "$Date$"
	revision    : "$Revision$"

class -- main class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature -- Status
	p: PRODUCER
feature {NONE} -- Initialization

	make
			-- Run application.
		do
			create p.producer("Producer1")
			-- print ()
		end

end
