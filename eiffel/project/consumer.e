note
	description: "Summary description for {CONSUMER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CONSUMER
feature -- Status
	name: STRING
	w: WAREHOUSE
feature -- Setter
	consumer(n:STRING)
		do
			name:= n
		end
	consume
		do
			w.remove_from_warehouse
		end
end

