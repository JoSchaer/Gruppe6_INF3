note
	description: "Summary description for {PRODUCER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PRODUCER
create

	producer

feature -- Status

	name: STRING

	w:WAREHOUSE

feature -- Setters

	producer (n: STRING)

		require
			name_exists: not n.is_empty
		do
			name := n
		ensure
			name_set: name = n
		end
		invariant
		name_exists:not name.is_empty

	produce(p: PRODUCT)
		do
			w.fill_warehouse(PRODUCT)
		end

end

