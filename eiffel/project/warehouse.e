note
	description: "Summary description for {WAREHOUSE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	WAREHOUSE
feature
	warehouse: ARRAYED_LIST [PRODUCT]
feature
	fill_warehouse (p: PRODUCT)
		do
			warehouse.extend(p)
		end
	remove_from_warehouse (p: PRODUCT)
		do
			warehouse.remove -- test, may have problem with cursor placing
		end
end
