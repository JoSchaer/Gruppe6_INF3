note
	description: "DINING_PHILOSOPHERS_PROBLEM application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION
	
inherit
	ARGUMENTS

create
	make

feature

	make
		local -- Method Variables
			first_chopstick: CHOPSTICK
			left_chopstick: CHOPSTICK
			right_chopstick: CHOPSTICK
			philosopher: PHILOSOPHER
			count: INTEGER

		do
			print ("The Philosopher meeting starts%N")
			create philospher_list.make
			from
				count := 1
				create first_chopstick.make
				left_chopstick := first_chopstick
			until
				count>number_of_philosophers
			loop
				if count<number_of_philosophers then
					create right_chopstick.make
				else
					right_chopstick := first_chopstick
				end
				create philosopher.make (left_chopstick, right_chopstick, rounds, count)
				philospher_list.extend (philosopher)
				count := count +1
			end
			-- an agent is an object that represents a routine
			philospher_list.do_all (agent start_philosophers)
			philospher_list.do_all (agent join_philosophers)
		end

feature {NONE}

	number_of_philosophers: INTEGER = 5

	rounds: INTEGER = 5

	philospher_list: LINKED_LIST [PHILOSOPHER]

	start_philosophers (a_philosopher: PHILOSOPHER)
		-- launchs philosopher calls execute from philosopher
		do
			a_philosopher.launch
		end

	join_philosophers (a_philosopher: PHILOSOPHER)
		-- join_all at the end of the execution of the
		-- parent thread if you do not want it to die before
		-- its child, otherwise they may prematurely terminate.
		do
			a_philosopher.join_all
		end
end
