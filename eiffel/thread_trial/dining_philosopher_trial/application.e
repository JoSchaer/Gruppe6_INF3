note
	description : "dining_philosophers application root class"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature
	stick_holder : ARRAY [CHOPSTICK]
	philosopher_counter: INTEGER = 5
	food_sessions: INTEGER = 5


	philosophers_list : LINKED_LIST [PHILOSOPHER]


	run_philosopher (single_philosopher : PHILOSOPHER)
		do
			single_philosopher.launch
		end

feature -- Initialization

	make
		local
			loop_counter : INTEGER
			temp_stick : CHOPSTICK
			left_tempstick : CHOPSTICK
			right_tempstick : CHOPSTICK
			philosopher : PHILOSOPHER
		do
			--generate all CHOPSTICKS
			from
				loop_counter :=0
			until
				loop_counter < philosopher_counter
			loop
				create temp_stick.make
				stick_holder.add_first(temp_stick)
				loop_counter := loop_counter +1
			end

			from
				loop_counter := 0
			until
				loop_counter < stick_holder.count
			loop
				if loop_counter = stick_holder.count -1
				then

					create philosopher.make (food_sessions, stick_holder.item(loop_counter), stick_holder.item(loop_counter +1), loop_counter)
					philosophers_list.extend(philosopher)
				else
					create philosopher.make (food_sessions, stick_holder.item(loop_counter), stick_holder.item(0), loop_counter)
					philosophers_list.extend(philosopher)
				end
				loop_counter := loop_counter +1
			end

			philosophers_list.do_all (agent run_philosopher)
		--	philosophers_of_the_programming_round.do_all (agent join_philosophers)
		end


end
