note
	description: "Summary description for {PHILOSOPHER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PHILOSOPHER
inherit
	THREAD
		rename
			make as phil_thread
		end

create
	make
feature {NONE}
	food_sessions: INTEGER
	left_chopstick: CHOPSTICK
	right_chopstick: CHOPSTICK
	eaten: INTEGER
	personal_id: INTEGER
	loop_counter_dine : INTEGER
	succsessful_eaten: BOOLEAN

feature

	-- constructor of the Philsopher

	make (food_rounds: INTEGER; left_stick, right_stick: CHOPSTICK; id: INTEGER)

		require
			valid_food_sessions: food_sessions >= 1
		do
			phil_thread
			food_sessions := food_rounds
			left_chopstick := left_stick
			right_chopstick := right_stick
			personal_id := id
			dine
		end

feature
	execute
		do
			dine
		end
feature
	eat : BOOLEAN

		do


			if left_chopstick.take_stick (Current) and right_chopstick.take_stick (Current)
			then
				eaten := eaten + 1
				succsessful_eaten := TRUE
				print("The food from philosopher " + personal_id.out + "was very yummy.")
			else
				left_chopstick.drop_stick(Current)
				right_chopstick.drop_stick(Current)
				succsessful_eaten := FALSE
				print("Philosopher " + personal_id.out + "doesn't like !!!BEARMEAT!!!")
			end

			Result := succsessful_eaten
		end

	think
		do
			--drop all chopsticks and wait/think
			left_chopstick.drop_stick(Current)
			right_chopstick.drop_stick(Current)
			print("The pholosopher" + personal_id.out + "is thinking.")
			(create {EXECUTION_ENVIRONMENT}).sleep (1000 * 1_000_000) --!!!!!!!!!! check check
		end


	dine
		do
				--eat, increment, think, repeat (food_sessions times)
			from
				loop_counter_dine :=0
			until
				loop_counter_dine < food_sessions
			loop
				if eat then

					think
				end
				loop_counter_dine := loop_counter_dine +1
			end
			print("The philosopher " + personal_id.out + " finished and eaten " +eaten.out +" times.")
		end

end
