note
	description: "Summary description for {BUFFER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BUFFER
create
	make
feature
	size: INTEGER
	buffer: LINKED_LIST[CAR]
	car_count: INTEGER_32
	full: BOOLEAN
	fill_count: SEMAPHORE
	empty_count: SEMAPHORE
	buffer_Mutex: SEMAPHORE
feature
	make
		do
			car_count:= 5
			create buffer.make
			create fill_count.make (0)
			create empty_count.make (car_count)
			create buffer_Mutex.make (1)
		end
feature
	push(e: CAR)
	-- adds element to buffer if buffer is full exception
		do
			empty_count.wait
			buffer_Mutex.wait
			if not CURRENT.full
			then
				buffer.force (e)
				print ("Car"+ e.toString.out+"at the spot"+ buffer.last.out+"was parked%N")
				print("There are his many spots occupied:"+ buffer.count.out+"%N")
			else
				print ("Parking is full%N")
			end
			buffer_Mutex.post
			empty_count.post
		end
	pop
	-- takes an element out of buffer, if buffer empty -> exception
		do
			fill_count.wait
			buffer_Mutex.wait
			if CURRENT.full
			then
				buffer.start
				print ("Car"+ buffer.at(1).toString.out+"at the spot"+buffer.last.out+"was removed%N")
				buffer.remove
			else
				print("Parking is empty%N")
			end
			buffer_Mutex.post
			fill_count.post
		end
	buffer_full:BOOLEAN
	-- checks if buffer is full
		do
			Result:= buffer.count = 10
		end
	buffer_empty:BOOLEAN
	-- checks if buffer is empty
		do
			Result:= buffer.count = 0
		end
end
