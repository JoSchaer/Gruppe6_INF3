note
	description : "producer_consumer application root class"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE}-- Initialization
	make
	local
		parking: BUFFER
		consumer_list: LINKED_LIST[CONSUMER]
		producer_list: LINKED_LIST[PRODUCER]
		consumer_one: CONSUMER
		producer_one: PRODUCER
		parking_index: INTEGER
		do
			create consumer_list.make
			create producer_list.make
			create parking.make
			from
				parking_index:=1
			until
				parking_index>4
			loop
				create consumer_one.make (parking_index, parking, producer_list)
				create producer_one.make (parking_index, parking, consumer_list)
				consumer_one.launch
				producer_one.launch
				consumer_list.force (consumer_one)
				producer_list.force (producer_one)
				parking_index := parking_index+1
			end
			print ("Threads have begun%N")
			{THREAD}.join_all
		end

end
