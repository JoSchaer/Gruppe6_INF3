note
	description: "Summary description for {CONSUMER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CONSUMER
inherit
	THREAD
	rename
		make as consumer_thread
	end
create
	make

feature
	make(name:INTEGER; new_buffer:BUFFER; new_producer:LINKED_LIST[PRODUCER])
		do
			consumer_thread
			buffer:= new_buffer
			producers:= new_producer
			consumer_number:= name
			awake:= TRUE
			loop_moving:= TRUE
		end
feature
	buffer: BUFFER
	consumer_number: INTEGER
	awake:BOOLEAN
	producers: LINKED_LIST[PRODUCER]
	buffer_size: INTEGER
	loop_moving: BOOLEAN

	execute --methode from thread, doing the "consuming" of the car
		local
			x: INTEGER
			i:INTEGER
			asleep: DOUBLE --the amount of time to send the thread to sleep
			r:RANDOM

		do
			from

			until
				not loop_moving
			loop
				if awake -- checks if consumer is awake/"consuming"
				then
					create r.make
					x:=1
					r.set_seed (2)
					asleep := r.double_item*1_000_000_000_000

					if buffer.buffer_empty
					then
			   			if buffer.full
			   			then
			   				from -- waking up producers
			   					i:=1
			   				until
			   					i>=producers.count
			   				loop
			   					producers.at (i).set_awake (TRUE)
			   					i:=i+1
			   				end
			    		end
			   			buffer.pop -- removes car from parking

					else
						set_awake(FALSE)
					end
					current.sleep (asleep.truncated_to_integer_64)
				end
			end
		end

	set_awake(wake:BOOLEAN)
		do
			awake:=wake
		end

end
