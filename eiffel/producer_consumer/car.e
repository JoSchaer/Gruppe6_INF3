note
	description: "Summary description for {CAR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CAR
create
	make

feature
	make(car_id: INTEGER)
	do
		new_id:= car_id
	end
feature
	new_id: INTEGER

	toString: STRING
		do
			Result:= new_id.out
		end
end
