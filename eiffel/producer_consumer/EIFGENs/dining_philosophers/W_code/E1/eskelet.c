#include "eif_eiffel.h"

#ifdef __cplusplus
extern "C" {
#endif

extern const char *names2[];
static const uint32 types2 [] =
{
SK_UINT32,
};

static const uint16 attr_flags2 [] =
{1,};

static const EIF_TYPE_INDEX g_atype2_0 [] = {134,0xFFFF};

static const EIF_TYPE_INDEX *gtypes2 [] = {
g_atype2_0,
};

static const int32 cn_attr2 [] =
{
38,
};

extern const char *names8[];
static const uint32 types8 [] =
{
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags8 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype8_0 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype8_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype8_2 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype8_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes8 [] = {
g_atype8_0,
g_atype8_1,
g_atype8_2,
g_atype8_3,
};

static const int32 cn_attr8 [] =
{
203,
204,
205,
202,
};

extern const char *names10[];
static const uint32 types10 [] =
{
SK_REF,
};

static const uint16 attr_flags10 [] =
{0,};

static const EIF_TYPE_INDEX g_atype10_0 [] = {99,0xFFFF};

static const EIF_TYPE_INDEX *gtypes10 [] = {
g_atype10_0,
};

static const int32 cn_attr10 [] =
{
315,
};

extern const char *names12[];
static const uint32 types12 [] =
{
SK_REF,
};

static const uint16 attr_flags12 [] =
{0,};

static const EIF_TYPE_INDEX g_atype12_0 [] = {0xFF01,87,0xFFFF};

static const EIF_TYPE_INDEX *gtypes12 [] = {
g_atype12_0,
};

static const int32 cn_attr12 [] =
{
394,
};

extern const char *names13[];
static const uint32 types13 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags13 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype13_0 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype13_1 [] = {187,0xFF01,0,0xFF01,0xFFF9,1,120,0xFF01,101,0xFFFF};
static const EIF_TYPE_INDEX g_atype13_2 [] = {187,0xFF01,0,0xFF01,0xFFF9,2,120,0xFF01,101,0xFF01,101,0xFFFF};
static const EIF_TYPE_INDEX g_atype13_3 [] = {187,0xFF01,0,0xFF01,0xFFF9,1,120,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype13_4 [] = {204,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype13_5 [] = {241,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype13_6 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype13_7 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype13_8 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype13_9 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype13_10 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype13_11 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes13 [] = {
g_atype13_0,
g_atype13_1,
g_atype13_2,
g_atype13_3,
g_atype13_4,
g_atype13_5,
g_atype13_6,
g_atype13_7,
g_atype13_8,
g_atype13_9,
g_atype13_10,
g_atype13_11,
};

static const int32 cn_attr13 [] =
{
398,
399,
400,
401,
402,
403,
404,
407,
408,
409,
410,
411,
};

extern const char *names14[];
static const uint32 types14 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags14 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype14_0 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype14_1 [] = {187,0xFF01,0,0xFF01,0xFFF9,1,120,0xFF01,101,0xFFFF};
static const EIF_TYPE_INDEX g_atype14_2 [] = {187,0xFF01,0,0xFF01,0xFFF9,2,120,0xFF01,101,0xFF01,101,0xFFFF};
static const EIF_TYPE_INDEX g_atype14_3 [] = {187,0xFF01,0,0xFF01,0xFFF9,1,120,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype14_4 [] = {204,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype14_5 [] = {241,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype14_6 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype14_7 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype14_8 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype14_9 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype14_10 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype14_11 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes14 [] = {
g_atype14_0,
g_atype14_1,
g_atype14_2,
g_atype14_3,
g_atype14_4,
g_atype14_5,
g_atype14_6,
g_atype14_7,
g_atype14_8,
g_atype14_9,
g_atype14_10,
g_atype14_11,
};

static const int32 cn_attr14 [] =
{
398,
399,
400,
401,
402,
403,
404,
407,
408,
409,
410,
411,
};

extern const char *names15[];
static const uint32 types15 [] =
{
SK_REF,
SK_BOOL,
SK_POINTER,
};

static const uint16 attr_flags15 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype15_0 [] = {86,0xFFFF};
static const EIF_TYPE_INDEX g_atype15_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype15_2 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes15 [] = {
g_atype15_0,
g_atype15_1,
g_atype15_2,
};

static const int32 cn_attr15 [] =
{
433,
428,
432,
};

extern const char *names27[];
static const uint32 types27 [] =
{
SK_REF,
SK_BOOL,
SK_POINTER,
};

static const uint16 attr_flags27 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype27_0 [] = {0xFF01,87,0xFFFF};
static const EIF_TYPE_INDEX g_atype27_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype27_2 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes27 [] = {
g_atype27_0,
g_atype27_1,
g_atype27_2,
};

static const int32 cn_attr27 [] =
{
608,
596,
595,
};

extern const char *names28[];
static const uint32 types28 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags28 [] =
{0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype28_0 [] = {0xFF01,87,0xFFFF};
static const EIF_TYPE_INDEX g_atype28_1 [] = {0xFF01,11,0xFFFF};
static const EIF_TYPE_INDEX g_atype28_2 [] = {0xFF01,11,0xFFFF};
static const EIF_TYPE_INDEX g_atype28_3 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype28_4 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype28_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype28_6 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype28_7 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype28_8 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype28_9 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes28 [] = {
g_atype28_0,
g_atype28_1,
g_atype28_2,
g_atype28_3,
g_atype28_4,
g_atype28_5,
g_atype28_6,
g_atype28_7,
g_atype28_8,
g_atype28_9,
};

static const int32 cn_attr28 [] =
{
608,
615,
616,
596,
620,
614,
617,
618,
619,
595,
};

extern const char *names33[];
static const uint32 types33 [] =
{
SK_UINT64,
SK_INT64,
};

static const uint16 attr_flags33 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype33_0 [] = {131,0xFFFF};
static const EIF_TYPE_INDEX g_atype33_1 [] = {152,0xFFFF};

static const EIF_TYPE_INDEX *gtypes33 [] = {
g_atype33_0,
g_atype33_1,
};

static const int32 cn_attr33 [] =
{
666,
665,
};

extern const char *names34[];
static const uint32 types34 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_UINT64,
SK_INT64,
};

static const uint16 attr_flags34 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype34_0 [] = {0xFF01,304,131,0xFFFF};
static const EIF_TYPE_INDEX g_atype34_1 [] = {0xFF01,304,131,0xFFFF};
static const EIF_TYPE_INDEX g_atype34_2 [] = {0xFF01,304,131,0xFFFF};
static const EIF_TYPE_INDEX g_atype34_3 [] = {0xFF01,304,131,0xFFFF};
static const EIF_TYPE_INDEX g_atype34_4 [] = {131,0xFFFF};
static const EIF_TYPE_INDEX g_atype34_5 [] = {152,0xFFFF};

static const EIF_TYPE_INDEX *gtypes34 [] = {
g_atype34_0,
g_atype34_1,
g_atype34_2,
g_atype34_3,
g_atype34_4,
g_atype34_5,
};

static const int32 cn_attr34 [] =
{
690,
691,
692,
693,
666,
665,
};

extern const char *names35[];
static const uint32 types35 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_UINT64,
SK_INT64,
};

static const uint16 attr_flags35 [] =
{0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype35_0 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype35_1 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype35_2 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype35_3 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype35_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype35_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype35_6 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype35_7 [] = {131,0xFFFF};
static const EIF_TYPE_INDEX g_atype35_8 [] = {152,0xFFFF};

static const EIF_TYPE_INDEX *gtypes35 [] = {
g_atype35_0,
g_atype35_1,
g_atype35_2,
g_atype35_3,
g_atype35_4,
g_atype35_5,
g_atype35_6,
g_atype35_7,
g_atype35_8,
};

static const int32 cn_attr35 [] =
{
696,
697,
694,
695,
703,
711,
712,
666,
665,
};

extern const char *names36[];
static const uint32 types36 [] =
{
SK_REF,
SK_REF,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_UINT64,
SK_UINT64,
SK_UINT64,
SK_INT64,
};

static const uint16 attr_flags36 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype36_0 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype36_1 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype36_2 [] = {155,0xFFFF};
static const EIF_TYPE_INDEX g_atype36_3 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype36_4 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype36_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype36_6 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype36_7 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype36_8 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype36_9 [] = {131,0xFFFF};
static const EIF_TYPE_INDEX g_atype36_10 [] = {131,0xFFFF};
static const EIF_TYPE_INDEX g_atype36_11 [] = {131,0xFFFF};
static const EIF_TYPE_INDEX g_atype36_12 [] = {152,0xFFFF};

static const EIF_TYPE_INDEX *gtypes36 [] = {
g_atype36_0,
g_atype36_1,
g_atype36_2,
g_atype36_3,
g_atype36_4,
g_atype36_5,
g_atype36_6,
g_atype36_7,
g_atype36_8,
g_atype36_9,
g_atype36_10,
g_atype36_11,
g_atype36_12,
};

static const int32 cn_attr36 [] =
{
696,
697,
726,
694,
695,
727,
703,
711,
712,
666,
729,
730,
665,
};

extern const char *names37[];
static const uint32 types37 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_UINT64,
SK_UINT64,
SK_UINT64,
SK_INT64,
};

static const uint16 attr_flags37 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype37_0 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype37_1 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype37_2 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype37_3 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype37_4 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype37_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype37_6 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype37_7 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype37_8 [] = {131,0xFFFF};
static const EIF_TYPE_INDEX g_atype37_9 [] = {131,0xFFFF};
static const EIF_TYPE_INDEX g_atype37_10 [] = {131,0xFFFF};
static const EIF_TYPE_INDEX g_atype37_11 [] = {152,0xFFFF};

static const EIF_TYPE_INDEX *gtypes37 [] = {
g_atype37_0,
g_atype37_1,
g_atype37_2,
g_atype37_3,
g_atype37_4,
g_atype37_5,
g_atype37_6,
g_atype37_7,
g_atype37_8,
g_atype37_9,
g_atype37_10,
g_atype37_11,
};

static const int32 cn_attr37 [] =
{
696,
697,
694,
695,
747,
703,
711,
712,
666,
745,
746,
665,
};

extern const char *names38[];
static const uint32 types38 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_UINT64,
SK_INT64,
SK_REAL64,
SK_REAL64,
SK_REAL64,
};

static const uint16 attr_flags38 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype38_0 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype38_1 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype38_2 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype38_3 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype38_4 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype38_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype38_6 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype38_7 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype38_8 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype38_9 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype38_10 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype38_11 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype38_12 [] = {131,0xFFFF};
static const EIF_TYPE_INDEX g_atype38_13 [] = {152,0xFFFF};
static const EIF_TYPE_INDEX g_atype38_14 [] = {146,0xFFFF};
static const EIF_TYPE_INDEX g_atype38_15 [] = {146,0xFFFF};
static const EIF_TYPE_INDEX g_atype38_16 [] = {146,0xFFFF};

static const EIF_TYPE_INDEX *gtypes38 [] = {
g_atype38_0,
g_atype38_1,
g_atype38_2,
g_atype38_3,
g_atype38_4,
g_atype38_5,
g_atype38_6,
g_atype38_7,
g_atype38_8,
g_atype38_9,
g_atype38_10,
g_atype38_11,
g_atype38_12,
g_atype38_13,
g_atype38_14,
g_atype38_15,
g_atype38_16,
};

static const int32 cn_attr38 [] =
{
696,
697,
694,
695,
759,
760,
761,
762,
703,
711,
712,
758,
666,
665,
755,
756,
757,
};

extern const char *names39[];
static const uint32 types39 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags39 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype39_0 [] = {0xFF01,248,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype39_1 [] = {178,0xFF01,248,122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes39 [] = {
g_atype39_0,
g_atype39_1,
};

static const int32 cn_attr39 [] =
{
771,
772,
};

extern const char *names40[];
static const uint32 types40 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags40 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype40_0 [] = {0xFF01,248,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype40_1 [] = {178,0xFF01,248,122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes40 [] = {
g_atype40_0,
g_atype40_1,
};

static const int32 cn_attr40 [] =
{
771,
772,
};

extern const char *names41[];
static const uint32 types41 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags41 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype41_0 [] = {0xFF01,248,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype41_1 [] = {178,0xFF01,248,122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes41 [] = {
g_atype41_0,
g_atype41_1,
};

static const int32 cn_attr41 [] =
{
771,
772,
};

extern const char *names44[];
static const uint32 types44 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags44 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype44_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype44_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype44_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype44_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype44_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype44_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype44_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes44 [] = {
g_atype44_0,
g_atype44_1,
g_atype44_2,
g_atype44_3,
g_atype44_4,
g_atype44_5,
g_atype44_6,
};

static const int32 cn_attr44 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names45[];
static const uint32 types45 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags45 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype45_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype45_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype45_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype45_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype45_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype45_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype45_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes45 [] = {
g_atype45_0,
g_atype45_1,
g_atype45_2,
g_atype45_3,
g_atype45_4,
g_atype45_5,
g_atype45_6,
};

static const int32 cn_attr45 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names46[];
static const uint32 types46 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags46 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype46_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype46_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype46_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype46_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype46_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype46_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype46_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes46 [] = {
g_atype46_0,
g_atype46_1,
g_atype46_2,
g_atype46_3,
g_atype46_4,
g_atype46_5,
g_atype46_6,
};

static const int32 cn_attr46 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names47[];
static const uint32 types47 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags47 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype47_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype47_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype47_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype47_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype47_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype47_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype47_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes47 [] = {
g_atype47_0,
g_atype47_1,
g_atype47_2,
g_atype47_3,
g_atype47_4,
g_atype47_5,
g_atype47_6,
};

static const int32 cn_attr47 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names48[];
static const uint32 types48 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags48 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype48_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes48 [] = {
g_atype48_0,
g_atype48_1,
g_atype48_2,
g_atype48_3,
g_atype48_4,
g_atype48_5,
g_atype48_6,
};

static const int32 cn_attr48 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names49[];
static const uint32 types49 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags49 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype49_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes49 [] = {
g_atype49_0,
g_atype49_1,
g_atype49_2,
g_atype49_3,
g_atype49_4,
g_atype49_5,
g_atype49_6,
};

static const int32 cn_attr49 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names50[];
static const uint32 types50 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags50 [] =
{0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype50_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_5 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_6 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_7 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_8 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_9 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes50 [] = {
g_atype50_0,
g_atype50_1,
g_atype50_2,
g_atype50_3,
g_atype50_4,
g_atype50_5,
g_atype50_6,
g_atype50_7,
g_atype50_8,
g_atype50_9,
};

static const int32 cn_attr50 [] =
{
812,
813,
822,
827,
831,
838,
829,
814,
832,
833,
};

extern const char *names51[];
static const uint32 types51 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags51 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype51_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_6 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_7 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes51 [] = {
g_atype51_0,
g_atype51_1,
g_atype51_2,
g_atype51_3,
g_atype51_4,
g_atype51_5,
g_atype51_6,
g_atype51_7,
};

static const int32 cn_attr51 [] =
{
812,
813,
822,
827,
831,
829,
814,
849,
};

extern const char *names52[];
static const uint32 types52 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags52 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype52_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype52_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype52_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype52_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype52_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype52_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype52_6 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype52_7 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes52 [] = {
g_atype52_0,
g_atype52_1,
g_atype52_2,
g_atype52_3,
g_atype52_4,
g_atype52_5,
g_atype52_6,
g_atype52_7,
};

static const int32 cn_attr52 [] =
{
812,
813,
822,
827,
831,
829,
814,
851,
};

extern const char *names53[];
static const uint32 types53 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags53 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype53_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype53_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype53_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype53_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype53_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype53_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype53_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes53 [] = {
g_atype53_0,
g_atype53_1,
g_atype53_2,
g_atype53_3,
g_atype53_4,
g_atype53_5,
g_atype53_6,
};

static const int32 cn_attr53 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names54[];
static const uint32 types54 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags54 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype54_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype54_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype54_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype54_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype54_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype54_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype54_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes54 [] = {
g_atype54_0,
g_atype54_1,
g_atype54_2,
g_atype54_3,
g_atype54_4,
g_atype54_5,
g_atype54_6,
};

static const int32 cn_attr54 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names55[];
static const uint32 types55 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags55 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype55_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype55_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype55_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype55_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype55_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype55_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype55_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes55 [] = {
g_atype55_0,
g_atype55_1,
g_atype55_2,
g_atype55_3,
g_atype55_4,
g_atype55_5,
g_atype55_6,
};

static const int32 cn_attr55 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names56[];
static const uint32 types56 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags56 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype56_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype56_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype56_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype56_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype56_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype56_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype56_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes56 [] = {
g_atype56_0,
g_atype56_1,
g_atype56_2,
g_atype56_3,
g_atype56_4,
g_atype56_5,
g_atype56_6,
};

static const int32 cn_attr56 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names57[];
static const uint32 types57 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags57 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype57_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes57 [] = {
g_atype57_0,
g_atype57_1,
g_atype57_2,
g_atype57_3,
g_atype57_4,
g_atype57_5,
g_atype57_6,
};

static const int32 cn_attr57 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names58[];
static const uint32 types58 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags58 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype58_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes58 [] = {
g_atype58_0,
g_atype58_1,
g_atype58_2,
g_atype58_3,
g_atype58_4,
g_atype58_5,
g_atype58_6,
};

static const int32 cn_attr58 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names59[];
static const uint32 types59 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags59 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype59_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_6 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_7 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes59 [] = {
g_atype59_0,
g_atype59_1,
g_atype59_2,
g_atype59_3,
g_atype59_4,
g_atype59_5,
g_atype59_6,
g_atype59_7,
};

static const int32 cn_attr59 [] =
{
812,
813,
822,
827,
831,
829,
814,
854,
};

extern const char *names60[];
static const uint32 types60 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags60 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype60_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes60 [] = {
g_atype60_0,
g_atype60_1,
g_atype60_2,
g_atype60_3,
g_atype60_4,
g_atype60_5,
g_atype60_6,
};

static const int32 cn_attr60 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names61[];
static const uint32 types61 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags61 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype61_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype61_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype61_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype61_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype61_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype61_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype61_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes61 [] = {
g_atype61_0,
g_atype61_1,
g_atype61_2,
g_atype61_3,
g_atype61_4,
g_atype61_5,
g_atype61_6,
};

static const int32 cn_attr61 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names62[];
static const uint32 types62 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags62 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype62_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype62_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype62_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype62_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype62_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype62_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype62_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes62 [] = {
g_atype62_0,
g_atype62_1,
g_atype62_2,
g_atype62_3,
g_atype62_4,
g_atype62_5,
g_atype62_6,
};

static const int32 cn_attr62 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names63[];
static const uint32 types63 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags63 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype63_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype63_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype63_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype63_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype63_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype63_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype63_6 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype63_7 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes63 [] = {
g_atype63_0,
g_atype63_1,
g_atype63_2,
g_atype63_3,
g_atype63_4,
g_atype63_5,
g_atype63_6,
g_atype63_7,
};

static const int32 cn_attr63 [] =
{
812,
813,
822,
827,
831,
829,
814,
856,
};

extern const char *names64[];
static const uint32 types64 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags64 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype64_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype64_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype64_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype64_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype64_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype64_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype64_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes64 [] = {
g_atype64_0,
g_atype64_1,
g_atype64_2,
g_atype64_3,
g_atype64_4,
g_atype64_5,
g_atype64_6,
};

static const int32 cn_attr64 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names65[];
static const uint32 types65 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags65 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype65_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes65 [] = {
g_atype65_0,
g_atype65_1,
g_atype65_2,
g_atype65_3,
g_atype65_4,
g_atype65_5,
g_atype65_6,
};

static const int32 cn_attr65 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names66[];
static const uint32 types66 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags66 [] =
{0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype66_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_6 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_7 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_8 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes66 [] = {
g_atype66_0,
g_atype66_1,
g_atype66_2,
g_atype66_3,
g_atype66_4,
g_atype66_5,
g_atype66_6,
g_atype66_7,
g_atype66_8,
};

static const int32 cn_attr66 [] =
{
812,
813,
822,
827,
831,
829,
814,
857,
860,
};

extern const char *names67[];
static const uint32 types67 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags67 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype67_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes67 [] = {
g_atype67_0,
g_atype67_1,
g_atype67_2,
g_atype67_3,
g_atype67_4,
g_atype67_5,
g_atype67_6,
};

static const int32 cn_attr67 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names68[];
static const uint32 types68 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags68 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype68_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes68 [] = {
g_atype68_0,
g_atype68_1,
g_atype68_2,
g_atype68_3,
g_atype68_4,
g_atype68_5,
g_atype68_6,
};

static const int32 cn_attr68 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names69[];
static const uint32 types69 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags69 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype69_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes69 [] = {
g_atype69_0,
g_atype69_1,
g_atype69_2,
g_atype69_3,
g_atype69_4,
g_atype69_5,
g_atype69_6,
};

static const int32 cn_attr69 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names70[];
static const uint32 types70 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags70 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype70_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes70 [] = {
g_atype70_0,
g_atype70_1,
g_atype70_2,
g_atype70_3,
g_atype70_4,
g_atype70_5,
g_atype70_6,
};

static const int32 cn_attr70 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names71[];
static const uint32 types71 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags71 [] =
{0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype71_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_5 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_6 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_7 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_8 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes71 [] = {
g_atype71_0,
g_atype71_1,
g_atype71_2,
g_atype71_3,
g_atype71_4,
g_atype71_5,
g_atype71_6,
g_atype71_7,
g_atype71_8,
};

static const int32 cn_attr71 [] =
{
812,
813,
822,
827,
831,
861,
862,
829,
814,
};

extern const char *names72[];
static const uint32 types72 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags72 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype72_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes72 [] = {
g_atype72_0,
g_atype72_1,
g_atype72_2,
g_atype72_3,
g_atype72_4,
g_atype72_5,
g_atype72_6,
};

static const int32 cn_attr72 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names73[];
static const uint32 types73 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags73 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype73_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes73 [] = {
g_atype73_0,
g_atype73_1,
g_atype73_2,
g_atype73_3,
g_atype73_4,
g_atype73_5,
g_atype73_6,
};

static const int32 cn_attr73 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names74[];
static const uint32 types74 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags74 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype74_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes74 [] = {
g_atype74_0,
g_atype74_1,
g_atype74_2,
g_atype74_3,
g_atype74_4,
g_atype74_5,
g_atype74_6,
};

static const int32 cn_attr74 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names75[];
static const uint32 types75 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags75 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype75_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes75 [] = {
g_atype75_0,
g_atype75_1,
g_atype75_2,
g_atype75_3,
g_atype75_4,
g_atype75_5,
g_atype75_6,
};

static const int32 cn_attr75 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names76[];
static const uint32 types76 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags76 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype76_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes76 [] = {
g_atype76_0,
g_atype76_1,
g_atype76_2,
g_atype76_3,
g_atype76_4,
g_atype76_5,
g_atype76_6,
};

static const int32 cn_attr76 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names77[];
static const uint32 types77 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags77 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype77_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes77 [] = {
g_atype77_0,
g_atype77_1,
g_atype77_2,
g_atype77_3,
g_atype77_4,
g_atype77_5,
g_atype77_6,
};

static const int32 cn_attr77 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names78[];
static const uint32 types78 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags78 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype78_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes78 [] = {
g_atype78_0,
g_atype78_1,
g_atype78_2,
g_atype78_3,
g_atype78_4,
g_atype78_5,
g_atype78_6,
};

static const int32 cn_attr78 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names79[];
static const uint32 types79 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags79 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype79_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes79 [] = {
g_atype79_0,
g_atype79_1,
g_atype79_2,
g_atype79_3,
g_atype79_4,
g_atype79_5,
g_atype79_6,
};

static const int32 cn_attr79 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names80[];
static const uint32 types80 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags80 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype80_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_6 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_7 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes80 [] = {
g_atype80_0,
g_atype80_1,
g_atype80_2,
g_atype80_3,
g_atype80_4,
g_atype80_5,
g_atype80_6,
g_atype80_7,
};

static const int32 cn_attr80 [] =
{
812,
813,
822,
827,
831,
829,
866,
814,
};

extern const char *names81[];
static const uint32 types81 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags81 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype81_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes81 [] = {
g_atype81_0,
g_atype81_1,
g_atype81_2,
g_atype81_3,
g_atype81_4,
g_atype81_5,
g_atype81_6,
};

static const int32 cn_attr81 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names82[];
static const uint32 types82 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags82 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype82_0 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_1 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_2 [] = {43,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_3 [] = {89,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_4 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes82 [] = {
g_atype82_0,
g_atype82_1,
g_atype82_2,
g_atype82_3,
g_atype82_4,
g_atype82_5,
g_atype82_6,
};

static const int32 cn_attr82 [] =
{
812,
813,
822,
827,
831,
829,
814,
};

extern const char *names85[];
static const uint32 types85 [] =
{
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags85 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype85_0 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype85_1 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes85 [] = {
g_atype85_0,
g_atype85_1,
};

static const int32 cn_attr85 [] =
{
877,
938,
};

extern const char *names87[];
static const uint32 types87 [] =
{
SK_BOOL,
SK_INT32,
SK_POINTER,
SK_UINT64,
};

static const uint16 attr_flags87 [] =
{0,0,1,1,};

static const EIF_TYPE_INDEX g_atype87_0 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype87_1 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype87_2 [] = {161,0xFFFF};
static const EIF_TYPE_INDEX g_atype87_3 [] = {131,0xFFFF};

static const EIF_TYPE_INDEX *gtypes87 [] = {
g_atype87_0,
g_atype87_1,
g_atype87_2,
g_atype87_3,
};

static const int32 cn_attr87 [] =
{
962,
961,
960,
1044,
};

extern const char *names88[];
static const uint32 types88 [] =
{
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags88 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype88_0 [] = {161,0xFFFF};
static const EIF_TYPE_INDEX g_atype88_1 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes88 [] = {
g_atype88_0,
g_atype88_1,
};

static const int32 cn_attr88 [] =
{
1047,
1055,
};

extern const char *names90[];
static const uint32 types90 [] =
{
SK_REF,
SK_INT32,
};

static const uint16 attr_flags90 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype90_0 [] = {0xFF01,86,0xFFFF};
static const EIF_TYPE_INDEX g_atype90_1 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes90 [] = {
g_atype90_0,
g_atype90_1,
};

static const int32 cn_attr90 [] =
{
1081,
1084,
};

extern const char *names91[];
static const uint32 types91 [] =
{
SK_REF,
SK_CHAR8,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags91 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype91_0 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_1 [] = {155,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_2 [] = {140,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_3 [] = {128,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_4 [] = {137,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_5 [] = {125,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_6 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_7 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_8 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_9 [] = {143,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_10 [] = {131,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_11 [] = {152,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_12 [] = {146,0xFFFF};

static const EIF_TYPE_INDEX *gtypes91 [] = {
g_atype91_0,
g_atype91_1,
g_atype91_2,
g_atype91_3,
g_atype91_4,
g_atype91_5,
g_atype91_6,
g_atype91_7,
g_atype91_8,
g_atype91_9,
g_atype91_10,
g_atype91_11,
g_atype91_12,
};

static const int32 cn_attr91 [] =
{
1101,
1100,
1111,
1106,
1110,
1105,
1108,
1102,
1114,
1112,
1107,
1104,
1113,
};

extern const char *names93[];
static const uint32 types93 [] =
{
SK_INT32,
};

static const uint16 attr_flags93 [] =
{0,};

static const EIF_TYPE_INDEX g_atype93_0 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes93 [] = {
g_atype93_0,
};

static const int32 cn_attr93 [] =
{
1180,
};

extern const char *names94[];
static const uint32 types94 [] =
{
SK_INT32,
};

static const uint16 attr_flags94 [] =
{0,};

static const EIF_TYPE_INDEX g_atype94_0 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes94 [] = {
g_atype94_0,
};

static const int32 cn_attr94 [] =
{
1182,
};

extern const char *names96[];
static const uint32 types96 [] =
{
SK_REF,
SK_INT32,
};

static const uint16 attr_flags96 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype96_0 [] = {0xFF01,86,0xFFFF};
static const EIF_TYPE_INDEX g_atype96_1 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes96 [] = {
g_atype96_0,
g_atype96_1,
};

static const int32 cn_attr96 [] =
{
1204,
1208,
};

extern const char *names97[];
static const uint32 types97 [] =
{
SK_INT32,
};

static const uint16 attr_flags97 [] =
{0,};

static const EIF_TYPE_INDEX g_atype97_0 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes97 [] = {
g_atype97_0,
};

static const int32 cn_attr97 [] =
{
1230,
};

extern const char *names98[];
static const uint32 types98 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_CHAR8,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_POINTER,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags98 [] =
{0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype98_0 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_1 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_2 [] = {86,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_3 [] = {155,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_4 [] = {155,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_6 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_7 [] = {140,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_8 [] = {128,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_9 [] = {137,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_10 [] = {125,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_11 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_12 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_13 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_14 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_15 [] = {143,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_16 [] = {161,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_17 [] = {131,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_18 [] = {152,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_19 [] = {146,0xFFFF};

static const EIF_TYPE_INDEX *gtypes98 [] = {
g_atype98_0,
g_atype98_1,
g_atype98_2,
g_atype98_3,
g_atype98_4,
g_atype98_5,
g_atype98_6,
g_atype98_7,
g_atype98_8,
g_atype98_9,
g_atype98_10,
g_atype98_11,
g_atype98_12,
g_atype98_13,
g_atype98_14,
g_atype98_15,
g_atype98_16,
g_atype98_17,
g_atype98_18,
g_atype98_19,
};

static const int32 cn_attr98 [] =
{
1101,
1340,
1342,
1100,
1261,
877,
1408,
1111,
1106,
1110,
1105,
1108,
1102,
1114,
1398,
1112,
1262,
1107,
1104,
1113,
};

extern const char *names99[];
static const uint32 types99 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_CHAR8,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_POINTER,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags99 [] =
{0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype99_0 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_1 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_2 [] = {86,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_3 [] = {86,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_4 [] = {155,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_5 [] = {155,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_6 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_7 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_8 [] = {140,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_9 [] = {128,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_10 [] = {137,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_11 [] = {125,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_12 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_13 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_14 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_15 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_16 [] = {143,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_17 [] = {161,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_18 [] = {131,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_19 [] = {152,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_20 [] = {146,0xFFFF};

static const EIF_TYPE_INDEX *gtypes99 [] = {
g_atype99_0,
g_atype99_1,
g_atype99_2,
g_atype99_3,
g_atype99_4,
g_atype99_5,
g_atype99_6,
g_atype99_7,
g_atype99_8,
g_atype99_9,
g_atype99_10,
g_atype99_11,
g_atype99_12,
g_atype99_13,
g_atype99_14,
g_atype99_15,
g_atype99_16,
g_atype99_17,
g_atype99_18,
g_atype99_19,
g_atype99_20,
};

static const int32 cn_attr99 [] =
{
1101,
1340,
1342,
1412,
1100,
1261,
877,
1408,
1111,
1106,
1110,
1105,
1108,
1102,
1114,
1398,
1112,
1262,
1107,
1104,
1113,
};

extern const char *names100[];
static const uint32 types100 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_CHAR8,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_POINTER,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags100 [] =
{0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype100_0 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_1 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_2 [] = {86,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_3 [] = {155,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_4 [] = {155,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_6 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_7 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_8 [] = {140,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_9 [] = {128,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_10 [] = {137,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_11 [] = {125,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_12 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_13 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_14 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_15 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_16 [] = {143,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_17 [] = {161,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_18 [] = {131,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_19 [] = {152,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_20 [] = {146,0xFFFF};

static const EIF_TYPE_INDEX *gtypes100 [] = {
g_atype100_0,
g_atype100_1,
g_atype100_2,
g_atype100_3,
g_atype100_4,
g_atype100_5,
g_atype100_6,
g_atype100_7,
g_atype100_8,
g_atype100_9,
g_atype100_10,
g_atype100_11,
g_atype100_12,
g_atype100_13,
g_atype100_14,
g_atype100_15,
g_atype100_16,
g_atype100_17,
g_atype100_18,
g_atype100_19,
g_atype100_20,
};

static const int32 cn_attr100 [] =
{
1101,
1340,
1342,
1100,
1261,
877,
1408,
1422,
1111,
1106,
1110,
1105,
1108,
1102,
1114,
1398,
1112,
1262,
1107,
1104,
1113,
};

extern const char *names102[];
static const uint32 types102 [] =
{
SK_INT32,
};

static const uint16 attr_flags102 [] =
{0,};

static const EIF_TYPE_INDEX g_atype102_0 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes102 [] = {
g_atype102_0,
};

static const int32 cn_attr102 [] =
{
1460,
};

extern const char *names105[];
static const uint32 types105 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags105 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype105_0 [] = {0xFF01,101,0xFFFF};
static const EIF_TYPE_INDEX g_atype105_1 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype105_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype105_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes105 [] = {
g_atype105_0,
g_atype105_1,
g_atype105_2,
g_atype105_3,
};

static const int32 cn_attr105 [] =
{
1622,
1460,
1621,
1623,
};

extern const char *names106[];
static const uint32 types106 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags106 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype106_0 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype106_1 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype106_2 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes106 [] = {
g_atype106_0,
g_atype106_1,
g_atype106_2,
};

static const int32 cn_attr106 [] =
{
1629,
1460,
1630,
};

extern const char *names107[];
static const uint32 types107 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags107 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype107_0 [] = {0xFF01,456,140,0xFFFF};
static const EIF_TYPE_INDEX g_atype107_1 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype107_2 [] = {86,0xFFFF};
static const EIF_TYPE_INDEX g_atype107_3 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype107_4 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes107 [] = {
g_atype107_0,
g_atype107_1,
g_atype107_2,
g_atype107_3,
g_atype107_4,
};

static const int32 cn_attr107 [] =
{
1632,
1687,
1688,
1661,
1662,
};

extern const char *names108[];
static const uint32 types108 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags108 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype108_0 [] = {0xFF01,456,140,0xFFFF};
static const EIF_TYPE_INDEX g_atype108_1 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype108_2 [] = {86,0xFFFF};
static const EIF_TYPE_INDEX g_atype108_3 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype108_4 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes108 [] = {
g_atype108_0,
g_atype108_1,
g_atype108_2,
g_atype108_3,
g_atype108_4,
};

static const int32 cn_attr108 [] =
{
1632,
1687,
1688,
1661,
1662,
};

extern const char *names112[];
static const uint32 types112 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags112 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype112_0 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype112_1 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype112_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype112_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes112 [] = {
g_atype112_0,
g_atype112_1,
g_atype112_2,
g_atype112_3,
};

static const int32 cn_attr112 [] =
{
1718,
1716,
1719,
1720,
};

extern const char *names115[];
static const uint32 types115 [] =
{
SK_REF,
};

static const uint16 attr_flags115 [] =
{0,};

static const EIF_TYPE_INDEX g_atype115_0 [] = {0xFF01,216,0xFF01,27,0xFFFF};

static const EIF_TYPE_INDEX *gtypes115 [] = {
g_atype115_0,
};

static const int32 cn_attr115 [] =
{
1767,
};

extern const char *names116[];
static const uint32 types116 [] =
{
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags116 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype116_0 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_2 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_4 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes116 [] = {
g_atype116_0,
g_atype116_1,
g_atype116_2,
g_atype116_3,
g_atype116_4,
};

static const int32 cn_attr116 [] =
{
877,
1841,
1843,
1856,
1857,
};

extern const char *names118[];
static const uint32 types118 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags118 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype118_0 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_1 [] = {0xFF01,178,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_2 [] = {0xFF01,178,0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_3 [] = {0xFF01,248,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_4 [] = {0xFF01,597,158,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_5 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_6 [] = {171,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_7 [] = {170,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_8 [] = {170,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_9 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_10 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_11 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_12 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_13 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_14 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_15 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_16 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_17 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype118_18 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes118 [] = {
g_atype118_0,
g_atype118_1,
g_atype118_2,
g_atype118_3,
g_atype118_4,
g_atype118_5,
g_atype118_6,
g_atype118_7,
g_atype118_8,
g_atype118_9,
g_atype118_10,
g_atype118_11,
g_atype118_12,
g_atype118_13,
g_atype118_14,
g_atype118_15,
g_atype118_16,
g_atype118_17,
g_atype118_18,
};

static const int32 cn_attr118 [] =
{
1962,
1997,
1998,
1999,
2000,
2012,
2013,
2053,
2054,
877,
1996,
2002,
1970,
2001,
2003,
2006,
2007,
2011,
2047,
};

extern const char *names120[];
static const uint32 types120 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
};

static const uint16 attr_flags120 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype120_0 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_1 [] = {167,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_2 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes120 [] = {
g_atype120_0,
g_atype120_1,
g_atype120_2,
};

static const int32 cn_attr120 [] =
{
2103,
2107,
2102,
};

extern const char *names122[];
static const uint32 types122 [] =
{
SK_INT32,
};

static const uint16 attr_flags122 [] =
{0,};

static const EIF_TYPE_INDEX g_atype122_0 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes122 [] = {
g_atype122_0,
};

static const int32 cn_attr122 [] =
{
2309,
};

extern const char *names123[];
static const uint32 types123 [] =
{
SK_INT32,
};

static const uint16 attr_flags123 [] =
{0,};

static const EIF_TYPE_INDEX g_atype123_0 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes123 [] = {
g_atype123_0,
};

static const int32 cn_attr123 [] =
{
2309,
};

extern const char *names124[];
static const uint32 types124 [] =
{
SK_INT32,
};

static const uint16 attr_flags124 [] =
{0,};

static const EIF_TYPE_INDEX g_atype124_0 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes124 [] = {
g_atype124_0,
};

static const int32 cn_attr124 [] =
{
2309,
};

extern const char *names125[];
static const uint32 types125 [] =
{
SK_INT16,
};

static const uint16 attr_flags125 [] =
{0,};

static const EIF_TYPE_INDEX g_atype125_0 [] = {125,0xFFFF};

static const EIF_TYPE_INDEX *gtypes125 [] = {
g_atype125_0,
};

static const int32 cn_attr125 [] =
{
2361,
};

extern const char *names126[];
static const uint32 types126 [] =
{
SK_INT16,
};

static const uint16 attr_flags126 [] =
{0,};

static const EIF_TYPE_INDEX g_atype126_0 [] = {125,0xFFFF};

static const EIF_TYPE_INDEX *gtypes126 [] = {
g_atype126_0,
};

static const int32 cn_attr126 [] =
{
2361,
};

extern const char *names127[];
static const uint32 types127 [] =
{
SK_INT16,
};

static const uint16 attr_flags127 [] =
{0,};

static const EIF_TYPE_INDEX g_atype127_0 [] = {125,0xFFFF};

static const EIF_TYPE_INDEX *gtypes127 [] = {
g_atype127_0,
};

static const int32 cn_attr127 [] =
{
2361,
};

extern const char *names128[];
static const uint32 types128 [] =
{
SK_INT8,
};

static const uint16 attr_flags128 [] =
{0,};

static const EIF_TYPE_INDEX g_atype128_0 [] = {128,0xFFFF};

static const EIF_TYPE_INDEX *gtypes128 [] = {
g_atype128_0,
};

static const int32 cn_attr128 [] =
{
2413,
};

extern const char *names129[];
static const uint32 types129 [] =
{
SK_INT8,
};

static const uint16 attr_flags129 [] =
{0,};

static const EIF_TYPE_INDEX g_atype129_0 [] = {128,0xFFFF};

static const EIF_TYPE_INDEX *gtypes129 [] = {
g_atype129_0,
};

static const int32 cn_attr129 [] =
{
2413,
};

extern const char *names130[];
static const uint32 types130 [] =
{
SK_INT8,
};

static const uint16 attr_flags130 [] =
{0,};

static const EIF_TYPE_INDEX g_atype130_0 [] = {128,0xFFFF};

static const EIF_TYPE_INDEX *gtypes130 [] = {
g_atype130_0,
};

static const int32 cn_attr130 [] =
{
2413,
};

extern const char *names131[];
static const uint32 types131 [] =
{
SK_UINT64,
};

static const uint16 attr_flags131 [] =
{0,};

static const EIF_TYPE_INDEX g_atype131_0 [] = {131,0xFFFF};

static const EIF_TYPE_INDEX *gtypes131 [] = {
g_atype131_0,
};

static const int32 cn_attr131 [] =
{
2465,
};

extern const char *names132[];
static const uint32 types132 [] =
{
SK_UINT64,
};

static const uint16 attr_flags132 [] =
{0,};

static const EIF_TYPE_INDEX g_atype132_0 [] = {131,0xFFFF};

static const EIF_TYPE_INDEX *gtypes132 [] = {
g_atype132_0,
};

static const int32 cn_attr132 [] =
{
2465,
};

extern const char *names133[];
static const uint32 types133 [] =
{
SK_UINT64,
};

static const uint16 attr_flags133 [] =
{0,};

static const EIF_TYPE_INDEX g_atype133_0 [] = {131,0xFFFF};

static const EIF_TYPE_INDEX *gtypes133 [] = {
g_atype133_0,
};

static const int32 cn_attr133 [] =
{
2465,
};

extern const char *names134[];
static const uint32 types134 [] =
{
SK_UINT32,
};

static const uint16 attr_flags134 [] =
{0,};

static const EIF_TYPE_INDEX g_atype134_0 [] = {134,0xFFFF};

static const EIF_TYPE_INDEX *gtypes134 [] = {
g_atype134_0,
};

static const int32 cn_attr134 [] =
{
2513,
};

extern const char *names135[];
static const uint32 types135 [] =
{
SK_UINT32,
};

static const uint16 attr_flags135 [] =
{0,};

static const EIF_TYPE_INDEX g_atype135_0 [] = {134,0xFFFF};

static const EIF_TYPE_INDEX *gtypes135 [] = {
g_atype135_0,
};

static const int32 cn_attr135 [] =
{
2513,
};

extern const char *names136[];
static const uint32 types136 [] =
{
SK_UINT32,
};

static const uint16 attr_flags136 [] =
{0,};

static const EIF_TYPE_INDEX g_atype136_0 [] = {134,0xFFFF};

static const EIF_TYPE_INDEX *gtypes136 [] = {
g_atype136_0,
};

static const int32 cn_attr136 [] =
{
2513,
};

extern const char *names137[];
static const uint32 types137 [] =
{
SK_UINT16,
};

static const uint16 attr_flags137 [] =
{0,};

static const EIF_TYPE_INDEX g_atype137_0 [] = {137,0xFFFF};

static const EIF_TYPE_INDEX *gtypes137 [] = {
g_atype137_0,
};

static const int32 cn_attr137 [] =
{
2561,
};

extern const char *names138[];
static const uint32 types138 [] =
{
SK_UINT16,
};

static const uint16 attr_flags138 [] =
{0,};

static const EIF_TYPE_INDEX g_atype138_0 [] = {137,0xFFFF};

static const EIF_TYPE_INDEX *gtypes138 [] = {
g_atype138_0,
};

static const int32 cn_attr138 [] =
{
2561,
};

extern const char *names139[];
static const uint32 types139 [] =
{
SK_UINT16,
};

static const uint16 attr_flags139 [] =
{0,};

static const EIF_TYPE_INDEX g_atype139_0 [] = {137,0xFFFF};

static const EIF_TYPE_INDEX *gtypes139 [] = {
g_atype139_0,
};

static const int32 cn_attr139 [] =
{
2561,
};

extern const char *names140[];
static const uint32 types140 [] =
{
SK_UINT8,
};

static const uint16 attr_flags140 [] =
{0,};

static const EIF_TYPE_INDEX g_atype140_0 [] = {140,0xFFFF};

static const EIF_TYPE_INDEX *gtypes140 [] = {
g_atype140_0,
};

static const int32 cn_attr140 [] =
{
2610,
};

extern const char *names141[];
static const uint32 types141 [] =
{
SK_UINT8,
};

static const uint16 attr_flags141 [] =
{0,};

static const EIF_TYPE_INDEX g_atype141_0 [] = {140,0xFFFF};

static const EIF_TYPE_INDEX *gtypes141 [] = {
g_atype141_0,
};

static const int32 cn_attr141 [] =
{
2610,
};

extern const char *names142[];
static const uint32 types142 [] =
{
SK_UINT8,
};

static const uint16 attr_flags142 [] =
{0,};

static const EIF_TYPE_INDEX g_atype142_0 [] = {140,0xFFFF};

static const EIF_TYPE_INDEX *gtypes142 [] = {
g_atype142_0,
};

static const int32 cn_attr142 [] =
{
2610,
};

extern const char *names143[];
static const uint32 types143 [] =
{
SK_REAL32,
};

static const uint16 attr_flags143 [] =
{0,};

static const EIF_TYPE_INDEX g_atype143_0 [] = {143,0xFFFF};

static const EIF_TYPE_INDEX *gtypes143 [] = {
g_atype143_0,
};

static const int32 cn_attr143 [] =
{
2659,
};

extern const char *names144[];
static const uint32 types144 [] =
{
SK_REAL32,
};

static const uint16 attr_flags144 [] =
{0,};

static const EIF_TYPE_INDEX g_atype144_0 [] = {143,0xFFFF};

static const EIF_TYPE_INDEX *gtypes144 [] = {
g_atype144_0,
};

static const int32 cn_attr144 [] =
{
2659,
};

extern const char *names145[];
static const uint32 types145 [] =
{
SK_REAL32,
};

static const uint16 attr_flags145 [] =
{0,};

static const EIF_TYPE_INDEX g_atype145_0 [] = {143,0xFFFF};

static const EIF_TYPE_INDEX *gtypes145 [] = {
g_atype145_0,
};

static const int32 cn_attr145 [] =
{
2659,
};

extern const char *names146[];
static const uint32 types146 [] =
{
SK_REAL64,
};

static const uint16 attr_flags146 [] =
{0,};

static const EIF_TYPE_INDEX g_atype146_0 [] = {146,0xFFFF};

static const EIF_TYPE_INDEX *gtypes146 [] = {
g_atype146_0,
};

static const int32 cn_attr146 [] =
{
2686,
};

extern const char *names147[];
static const uint32 types147 [] =
{
SK_REAL64,
};

static const uint16 attr_flags147 [] =
{0,};

static const EIF_TYPE_INDEX g_atype147_0 [] = {146,0xFFFF};

static const EIF_TYPE_INDEX *gtypes147 [] = {
g_atype147_0,
};

static const int32 cn_attr147 [] =
{
2686,
};

extern const char *names148[];
static const uint32 types148 [] =
{
SK_REAL64,
};

static const uint16 attr_flags148 [] =
{0,};

static const EIF_TYPE_INDEX g_atype148_0 [] = {146,0xFFFF};

static const EIF_TYPE_INDEX *gtypes148 [] = {
g_atype148_0,
};

static const int32 cn_attr148 [] =
{
2686,
};

extern const char *names149[];
static const uint32 types149 [] =
{
SK_CHAR32,
};

static const uint16 attr_flags149 [] =
{0,};

static const EIF_TYPE_INDEX g_atype149_0 [] = {149,0xFFFF};

static const EIF_TYPE_INDEX *gtypes149 [] = {
g_atype149_0,
};

static const int32 cn_attr149 [] =
{
2713,
};

extern const char *names150[];
static const uint32 types150 [] =
{
SK_CHAR32,
};

static const uint16 attr_flags150 [] =
{0,};

static const EIF_TYPE_INDEX g_atype150_0 [] = {149,0xFFFF};

static const EIF_TYPE_INDEX *gtypes150 [] = {
g_atype150_0,
};

static const int32 cn_attr150 [] =
{
2713,
};

extern const char *names151[];
static const uint32 types151 [] =
{
SK_CHAR32,
};

static const uint16 attr_flags151 [] =
{0,};

static const EIF_TYPE_INDEX g_atype151_0 [] = {149,0xFFFF};

static const EIF_TYPE_INDEX *gtypes151 [] = {
g_atype151_0,
};

static const int32 cn_attr151 [] =
{
2713,
};

extern const char *names152[];
static const uint32 types152 [] =
{
SK_INT64,
};

static const uint16 attr_flags152 [] =
{0,};

static const EIF_TYPE_INDEX g_atype152_0 [] = {152,0xFFFF};

static const EIF_TYPE_INDEX *gtypes152 [] = {
g_atype152_0,
};

static const int32 cn_attr152 [] =
{
2745,
};

extern const char *names153[];
static const uint32 types153 [] =
{
SK_INT64,
};

static const uint16 attr_flags153 [] =
{0,};

static const EIF_TYPE_INDEX g_atype153_0 [] = {152,0xFFFF};

static const EIF_TYPE_INDEX *gtypes153 [] = {
g_atype153_0,
};

static const int32 cn_attr153 [] =
{
2745,
};

extern const char *names154[];
static const uint32 types154 [] =
{
SK_INT64,
};

static const uint16 attr_flags154 [] =
{0,};

static const EIF_TYPE_INDEX g_atype154_0 [] = {152,0xFFFF};

static const EIF_TYPE_INDEX *gtypes154 [] = {
g_atype154_0,
};

static const int32 cn_attr154 [] =
{
2745,
};

extern const char *names155[];
static const uint32 types155 [] =
{
SK_CHAR8,
};

static const uint16 attr_flags155 [] =
{0,};

static const EIF_TYPE_INDEX g_atype155_0 [] = {155,0xFFFF};

static const EIF_TYPE_INDEX *gtypes155 [] = {
g_atype155_0,
};

static const int32 cn_attr155 [] =
{
2796,
};

extern const char *names156[];
static const uint32 types156 [] =
{
SK_CHAR8,
};

static const uint16 attr_flags156 [] =
{0,};

static const EIF_TYPE_INDEX g_atype156_0 [] = {155,0xFFFF};

static const EIF_TYPE_INDEX *gtypes156 [] = {
g_atype156_0,
};

static const int32 cn_attr156 [] =
{
2796,
};

extern const char *names157[];
static const uint32 types157 [] =
{
SK_CHAR8,
};

static const uint16 attr_flags157 [] =
{0,};

static const EIF_TYPE_INDEX g_atype157_0 [] = {155,0xFFFF};

static const EIF_TYPE_INDEX *gtypes157 [] = {
g_atype157_0,
};

static const int32 cn_attr157 [] =
{
2796,
};

extern const char *names158[];
static const uint32 types158 [] =
{
SK_BOOL,
};

static const uint16 attr_flags158 [] =
{0,};

static const EIF_TYPE_INDEX g_atype158_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes158 [] = {
g_atype158_0,
};

static const int32 cn_attr158 [] =
{
2837,
};

extern const char *names159[];
static const uint32 types159 [] =
{
SK_BOOL,
};

static const uint16 attr_flags159 [] =
{0,};

static const EIF_TYPE_INDEX g_atype159_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes159 [] = {
g_atype159_0,
};

static const int32 cn_attr159 [] =
{
2837,
};

extern const char *names160[];
static const uint32 types160 [] =
{
SK_BOOL,
};

static const uint16 attr_flags160 [] =
{0,};

static const EIF_TYPE_INDEX g_atype160_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes160 [] = {
g_atype160_0,
};

static const int32 cn_attr160 [] =
{
2837,
};

extern const char *names161[];
static const uint32 types161 [] =
{
SK_POINTER,
};

static const uint16 attr_flags161 [] =
{0,};

static const EIF_TYPE_INDEX g_atype161_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes161 [] = {
g_atype161_0,
};

static const int32 cn_attr161 [] =
{
2849,
};

extern const char *names162[];
static const uint32 types162 [] =
{
SK_POINTER,
};

static const uint16 attr_flags162 [] =
{0,};

static const EIF_TYPE_INDEX g_atype162_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes162 [] = {
g_atype162_0,
};

static const int32 cn_attr162 [] =
{
2849,
};

extern const char *names163[];
static const uint32 types163 [] =
{
SK_POINTER,
};

static const uint16 attr_flags163 [] =
{0,};

static const EIF_TYPE_INDEX g_atype163_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes163 [] = {
g_atype163_0,
};

static const int32 cn_attr163 [] =
{
2849,
};

extern const char *names164[];
static const uint32 types164 [] =
{
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags164 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype164_0 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype164_1 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes164 [] = {
g_atype164_0,
g_atype164_1,
};

static const int32 cn_attr164 [] =
{
3011,
3012,
};

extern const char *names165[];
static const uint32 types165 [] =
{
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags165 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype165_0 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype165_1 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes165 [] = {
g_atype165_0,
g_atype165_1,
};

static const int32 cn_attr165 [] =
{
3011,
3012,
};

extern const char *names166[];
static const uint32 types166 [] =
{
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags166 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype166_0 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype166_1 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes166 [] = {
g_atype166_0,
g_atype166_1,
};

static const int32 cn_attr166 [] =
{
3011,
3012,
};

extern const char *names167[];
static const uint32 types167 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags167 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype167_0 [] = {0xFF01,638,149,0xFFFF};
static const EIF_TYPE_INDEX g_atype167_1 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype167_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype167_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes167 [] = {
g_atype167_0,
g_atype167_1,
g_atype167_2,
g_atype167_3,
};

static const int32 cn_attr167 [] =
{
3053,
3011,
3012,
3056,
};

extern const char *names168[];
static const uint32 types168 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags168 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype168_0 [] = {0xFF01,638,149,0xFFFF};
static const EIF_TYPE_INDEX g_atype168_1 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype168_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype168_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype168_4 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes168 [] = {
g_atype168_0,
g_atype168_1,
g_atype168_2,
g_atype168_3,
g_atype168_4,
};

static const int32 cn_attr168 [] =
{
3053,
3011,
3012,
3056,
3063,
};

extern const char *names169[];
static const uint32 types169 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags169 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype169_0 [] = {0xFF01,638,149,0xFFFF};
static const EIF_TYPE_INDEX g_atype169_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype169_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype169_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype169_4 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes169 [] = {
g_atype169_0,
g_atype169_1,
g_atype169_2,
g_atype169_3,
g_atype169_4,
};

static const int32 cn_attr169 [] =
{
3053,
877,
3011,
3012,
3056,
};

extern const char *names170[];
static const uint32 types170 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags170 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype170_0 [] = {0xFF01,531,155,0xFFFF};
static const EIF_TYPE_INDEX g_atype170_1 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype170_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype170_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes170 [] = {
g_atype170_0,
g_atype170_1,
g_atype170_2,
g_atype170_3,
};

static const int32 cn_attr170 [] =
{
3147,
3011,
3012,
3150,
};

extern const char *names171[];
static const uint32 types171 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags171 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype171_0 [] = {0xFF01,531,155,0xFFFF};
static const EIF_TYPE_INDEX g_atype171_1 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype171_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype171_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype171_4 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes171 [] = {
g_atype171_0,
g_atype171_1,
g_atype171_2,
g_atype171_3,
g_atype171_4,
};

static const int32 cn_attr171 [] =
{
3147,
3011,
3012,
3150,
3155,
};

extern const char *names172[];
static const uint32 types172 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags172 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype172_0 [] = {0xFF01,531,155,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_4 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes172 [] = {
g_atype172_0,
g_atype172_1,
g_atype172_2,
g_atype172_3,
g_atype172_4,
};

static const int32 cn_attr172 [] =
{
3147,
877,
3011,
3012,
3150,
};

extern const char *names173[];
static const uint32 types173 [] =
{
SK_REF,
SK_BOOL,
SK_POINTER,
};

static const uint16 attr_flags173 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype173_0 [] = {86,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_2 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes173 [] = {
g_atype173_0,
g_atype173_1,
g_atype173_2,
};

static const int32 cn_attr173 [] =
{
433,
428,
432,
};

extern const char *names174[];
static const uint32 types174 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_CHAR8,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_POINTER,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags174 [] =
{0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype174_0 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_1 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_2 [] = {86,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_3 [] = {155,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_4 [] = {155,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_6 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_7 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_8 [] = {140,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_9 [] = {128,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_10 [] = {137,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_11 [] = {125,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_12 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_13 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_14 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_15 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_16 [] = {143,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_17 [] = {161,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_18 [] = {131,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_19 [] = {152,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_20 [] = {146,0xFFFF};

static const EIF_TYPE_INDEX *gtypes174 [] = {
g_atype174_0,
g_atype174_1,
g_atype174_2,
g_atype174_3,
g_atype174_4,
g_atype174_5,
g_atype174_6,
g_atype174_7,
g_atype174_8,
g_atype174_9,
g_atype174_10,
g_atype174_11,
g_atype174_12,
g_atype174_13,
g_atype174_14,
g_atype174_15,
g_atype174_16,
g_atype174_17,
g_atype174_18,
g_atype174_19,
g_atype174_20,
};

static const int32 cn_attr174 [] =
{
1101,
1340,
1342,
1100,
1261,
877,
1408,
1422,
1111,
1106,
1110,
1105,
1108,
1102,
1114,
1398,
1112,
1262,
1107,
1104,
1113,
};

extern const char *names176[];
static const uint32 types176 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags176 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype176_0 [] = {0xFF01,176,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_1 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_2 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_3 [] = {175,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_4 [] = {0xFF01,616,158,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_5 [] = {204,175,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_6 [] = {204,0xFF01,177,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_7 [] = {0xFF01,0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_8 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_9 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_10 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_11 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_12 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_13 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_14 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes176 [] = {
g_atype176_0,
g_atype176_1,
g_atype176_2,
g_atype176_3,
g_atype176_4,
g_atype176_5,
g_atype176_6,
g_atype176_7,
g_atype176_8,
g_atype176_9,
g_atype176_10,
g_atype176_11,
g_atype176_12,
g_atype176_13,
g_atype176_14,
};

static const int32 cn_attr176 [] =
{
3296,
3297,
3301,
3303,
3304,
3305,
3306,
3309,
3308,
3314,
3315,
3316,
3299,
3300,
3302,
};

extern const char *names177[];
static const uint32 types177 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags177 [] =
{0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype177_0 [] = {175,0xFFFF};
static const EIF_TYPE_INDEX g_atype177_1 [] = {175,0xFFFF};
static const EIF_TYPE_INDEX g_atype177_2 [] = {175,0xFFFF};
static const EIF_TYPE_INDEX g_atype177_3 [] = {216,0xFF01,0xFFF9,2,120,0xFF01,175,204,0xFF01,0xFFF9,2,120,0xFF01,177,0xFF01,177,0xFFFF};
static const EIF_TYPE_INDEX g_atype177_4 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype177_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype177_6 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype177_7 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype177_8 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype177_9 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype177_10 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes177 [] = {
g_atype177_0,
g_atype177_1,
g_atype177_2,
g_atype177_3,
g_atype177_4,
g_atype177_5,
g_atype177_6,
g_atype177_7,
g_atype177_8,
g_atype177_9,
g_atype177_10,
};

static const int32 cn_attr177 [] =
{
3360,
3361,
3387,
3389,
3357,
3358,
3359,
3385,
3386,
3355,
3356,
};

extern const char *names178[];
static const uint32 types178 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags178 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype178_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype178_1 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype178_2 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes178 [] = {
g_atype178_0,
g_atype178_1,
g_atype178_2,
};

static const int32 cn_attr178 [] =
{
3400,
3398,
3399,
};

extern const char *names180[];
static const uint32 types180 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags180 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype180_0 [] = {0xFF01,180,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype180_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype180_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype180_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype180_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype180_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype180_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes180 [] = {
g_atype180_0,
g_atype180_1,
g_atype180_2,
g_atype180_3,
g_atype180_4,
g_atype180_5,
g_atype180_6,
};

static const int32 cn_attr180 [] =
{
1789,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names184[];
static const uint32 types184 [] =
{
SK_REF,
};

static const uint16 attr_flags184 [] =
{0,};

static const EIF_TYPE_INDEX g_atype184_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes184 [] = {
g_atype184_0,
};

static const int32 cn_attr184 [] =
{
2139,
};

extern const char *names185[];
static const uint32 types185 [] =
{
SK_POINTER,
};

static const uint16 attr_flags185 [] =
{0,};

static const EIF_TYPE_INDEX g_atype185_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes185 [] = {
g_atype185_0,
};

static const int32 cn_attr185 [] =
{
2849,
};

extern const char *names186[];
static const uint32 types186 [] =
{
SK_POINTER,
};

static const uint16 attr_flags186 [] =
{0,};

static const EIF_TYPE_INDEX g_atype186_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes186 [] = {
g_atype186_0,
};

static const int32 cn_attr186 [] =
{
2849,
};

extern const char *names187[];
static const uint32 types187 [] =
{
SK_REF,
};

static const uint16 attr_flags187 [] =
{0,};

static const EIF_TYPE_INDEX g_atype187_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes187 [] = {
g_atype187_0,
};

static const int32 cn_attr187 [] =
{
2139,
};

extern const char *names188[];
static const uint32 types188 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags188 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype188_0 [] = {0xFF02,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype188_1 [] = {0xFFF9,0,120,0xFFFF};
static const EIF_TYPE_INDEX g_atype188_2 [] = {249,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype188_3 [] = {249,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype188_4 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype188_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype188_6 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype188_7 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype188_8 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype188_9 [] = {161,0xFFFF};
static const EIF_TYPE_INDEX g_atype188_10 [] = {161,0xFFFF};
static const EIF_TYPE_INDEX g_atype188_11 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes188 [] = {
g_atype188_0,
g_atype188_1,
g_atype188_2,
g_atype188_3,
g_atype188_4,
g_atype188_5,
g_atype188_6,
g_atype188_7,
g_atype188_8,
g_atype188_9,
g_atype188_10,
g_atype188_11,
};

static const int32 cn_attr188 [] =
{
2874,
2890,
2894,
2902,
2882,
2897,
2883,
2896,
2898,
2892,
2893,
2895,
};

extern const char *names189[];
static const uint32 types189 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags189 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype189_0 [] = {0xFF02,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype189_1 [] = {0xFFF9,0,120,0xFFFF};
static const EIF_TYPE_INDEX g_atype189_2 [] = {249,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype189_3 [] = {249,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype189_4 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype189_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype189_6 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype189_7 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype189_8 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype189_9 [] = {161,0xFFFF};
static const EIF_TYPE_INDEX g_atype189_10 [] = {161,0xFFFF};
static const EIF_TYPE_INDEX g_atype189_11 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes189 [] = {
g_atype189_0,
g_atype189_1,
g_atype189_2,
g_atype189_3,
g_atype189_4,
g_atype189_5,
g_atype189_6,
g_atype189_7,
g_atype189_8,
g_atype189_9,
g_atype189_10,
g_atype189_11,
};

static const int32 cn_attr189 [] =
{
2874,
2890,
2894,
2902,
2882,
2897,
2883,
2896,
2898,
2892,
2893,
2895,
};

extern const char *names190[];
static const uint32 types190 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags190 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype190_0 [] = {0xFF02,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype190_1 [] = {0xFFF9,0,120,0xFFFF};
static const EIF_TYPE_INDEX g_atype190_2 [] = {249,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype190_3 [] = {249,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype190_4 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype190_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype190_6 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype190_7 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype190_8 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype190_9 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype190_10 [] = {161,0xFFFF};
static const EIF_TYPE_INDEX g_atype190_11 [] = {161,0xFFFF};
static const EIF_TYPE_INDEX g_atype190_12 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes190 [] = {
g_atype190_0,
g_atype190_1,
g_atype190_2,
g_atype190_3,
g_atype190_4,
g_atype190_5,
g_atype190_6,
g_atype190_7,
g_atype190_8,
g_atype190_9,
g_atype190_10,
g_atype190_11,
g_atype190_12,
};

static const int32 cn_attr190 [] =
{
2874,
2890,
2894,
2902,
2882,
2897,
2912,
2883,
2896,
2898,
2892,
2893,
2895,
};

extern const char *names191[];
static const uint32 types191 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags191 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype191_0 [] = {0xFF01,178,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype191_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype191_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype191_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes191 [] = {
g_atype191_0,
g_atype191_1,
g_atype191_2,
g_atype191_3,
};

static const int32 cn_attr191 [] =
{
1632,
877,
1866,
1867,
};

extern const char *names192[];
static const uint32 types192 [] =
{
SK_BOOL,
};

static const uint16 attr_flags192 [] =
{0,};

static const EIF_TYPE_INDEX g_atype192_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes192 [] = {
g_atype192_0,
};

static const int32 cn_attr192 [] =
{
877,
};

extern const char *names193[];
static const uint32 types193 [] =
{
SK_BOOL,
};

static const uint16 attr_flags193 [] =
{0,};

static const EIF_TYPE_INDEX g_atype193_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes193 [] = {
g_atype193_0,
};

static const int32 cn_attr193 [] =
{
877,
};

extern const char *names194[];
static const uint32 types194 [] =
{
SK_BOOL,
};

static const uint16 attr_flags194 [] =
{0,};

static const EIF_TYPE_INDEX g_atype194_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes194 [] = {
g_atype194_0,
};

static const int32 cn_attr194 [] =
{
877,
};

extern const char *names195[];
static const uint32 types195 [] =
{
SK_BOOL,
};

static const uint16 attr_flags195 [] =
{0,};

static const EIF_TYPE_INDEX g_atype195_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes195 [] = {
g_atype195_0,
};

static const int32 cn_attr195 [] =
{
877,
};

extern const char *names196[];
static const uint32 types196 [] =
{
SK_BOOL,
};

static const uint16 attr_flags196 [] =
{0,};

static const EIF_TYPE_INDEX g_atype196_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes196 [] = {
g_atype196_0,
};

static const int32 cn_attr196 [] =
{
877,
};

extern const char *names197[];
static const uint32 types197 [] =
{
SK_BOOL,
};

static const uint16 attr_flags197 [] =
{0,};

static const EIF_TYPE_INDEX g_atype197_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes197 [] = {
g_atype197_0,
};

static const int32 cn_attr197 [] =
{
877,
};

extern const char *names198[];
static const uint32 types198 [] =
{
SK_BOOL,
};

static const uint16 attr_flags198 [] =
{0,};

static const EIF_TYPE_INDEX g_atype198_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes198 [] = {
g_atype198_0,
};

static const int32 cn_attr198 [] =
{
877,
};

extern const char *names199[];
static const uint32 types199 [] =
{
SK_BOOL,
};

static const uint16 attr_flags199 [] =
{0,};

static const EIF_TYPE_INDEX g_atype199_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes199 [] = {
g_atype199_0,
};

static const int32 cn_attr199 [] =
{
877,
};

extern const char *names200[];
static const uint32 types200 [] =
{
SK_BOOL,
};

static const uint16 attr_flags200 [] =
{0,};

static const EIF_TYPE_INDEX g_atype200_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes200 [] = {
g_atype200_0,
};

static const int32 cn_attr200 [] =
{
877,
};

extern const char *names201[];
static const uint32 types201 [] =
{
SK_BOOL,
};

static const uint16 attr_flags201 [] =
{0,};

static const EIF_TYPE_INDEX g_atype201_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes201 [] = {
g_atype201_0,
};

static const int32 cn_attr201 [] =
{
877,
};

extern const char *names202[];
static const uint32 types202 [] =
{
SK_BOOL,
};

static const uint16 attr_flags202 [] =
{0,};

static const EIF_TYPE_INDEX g_atype202_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes202 [] = {
g_atype202_0,
};

static const int32 cn_attr202 [] =
{
877,
};

extern const char *names203[];
static const uint32 types203 [] =
{
SK_REF,
};

static const uint16 attr_flags203 [] =
{0,};

static const EIF_TYPE_INDEX g_atype203_0 [] = {0xFF01,178,0xFFF8,1,0xFFFF};

static const EIF_TYPE_INDEX *gtypes203 [] = {
g_atype203_0,
};

static const int32 cn_attr203 [] =
{
1632,
};

extern const char *names205[];
static const uint32 types205 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags205 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype205_0 [] = {0xFF01,178,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype205_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype205_2 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes205 [] = {
g_atype205_0,
g_atype205_1,
g_atype205_2,
};

static const int32 cn_attr205 [] =
{
1632,
877,
1958,
};

extern const char *names206[];
static const uint32 types206 [] =
{
SK_BOOL,
};

static const uint16 attr_flags206 [] =
{0,};

static const EIF_TYPE_INDEX g_atype206_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes206 [] = {
g_atype206_0,
};

static const int32 cn_attr206 [] =
{
877,
};

extern const char *names207[];
static const uint32 types207 [] =
{
SK_BOOL,
};

static const uint16 attr_flags207 [] =
{0,};

static const EIF_TYPE_INDEX g_atype207_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes207 [] = {
g_atype207_0,
};

static const int32 cn_attr207 [] =
{
877,
};

extern const char *names208[];
static const uint32 types208 [] =
{
SK_BOOL,
};

static const uint16 attr_flags208 [] =
{0,};

static const EIF_TYPE_INDEX g_atype208_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes208 [] = {
g_atype208_0,
};

static const int32 cn_attr208 [] =
{
877,
};

extern const char *names209[];
static const uint32 types209 [] =
{
SK_BOOL,
};

static const uint16 attr_flags209 [] =
{0,};

static const EIF_TYPE_INDEX g_atype209_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes209 [] = {
g_atype209_0,
};

static const int32 cn_attr209 [] =
{
877,
};

extern const char *names210[];
static const uint32 types210 [] =
{
SK_BOOL,
};

static const uint16 attr_flags210 [] =
{0,};

static const EIF_TYPE_INDEX g_atype210_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes210 [] = {
g_atype210_0,
};

static const int32 cn_attr210 [] =
{
877,
};

extern const char *names211[];
static const uint32 types211 [] =
{
SK_BOOL,
};

static const uint16 attr_flags211 [] =
{0,};

static const EIF_TYPE_INDEX g_atype211_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes211 [] = {
g_atype211_0,
};

static const int32 cn_attr211 [] =
{
877,
};

extern const char *names212[];
static const uint32 types212 [] =
{
SK_BOOL,
};

static const uint16 attr_flags212 [] =
{0,};

static const EIF_TYPE_INDEX g_atype212_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes212 [] = {
g_atype212_0,
};

static const int32 cn_attr212 [] =
{
877,
};

extern const char *names213[];
static const uint32 types213 [] =
{
SK_BOOL,
};

static const uint16 attr_flags213 [] =
{0,};

static const EIF_TYPE_INDEX g_atype213_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes213 [] = {
g_atype213_0,
};

static const int32 cn_attr213 [] =
{
877,
};

extern const char *names214[];
static const uint32 types214 [] =
{
SK_BOOL,
};

static const uint16 attr_flags214 [] =
{0,};

static const EIF_TYPE_INDEX g_atype214_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes214 [] = {
g_atype214_0,
};

static const int32 cn_attr214 [] =
{
877,
};

extern const char *names215[];
static const uint32 types215 [] =
{
SK_REF,
};

static const uint16 attr_flags215 [] =
{0,};

static const EIF_TYPE_INDEX g_atype215_0 [] = {0xFFF8,1,0xFFFF};

static const EIF_TYPE_INDEX *gtypes215 [] = {
g_atype215_0,
};

static const int32 cn_attr215 [] =
{
533,
};

extern const char *names217[];
static const uint32 types217 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags217 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype217_0 [] = {217,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype217_1 [] = {217,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype217_2 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype217_3 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype217_4 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype217_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes217 [] = {
g_atype217_0,
g_atype217_1,
g_atype217_2,
g_atype217_3,
g_atype217_4,
g_atype217_5,
};

static const int32 cn_attr217 [] =
{
1921,
1925,
877,
1929,
1930,
1931,
};

extern const char *names218[];
static const uint32 types218 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags218 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype218_0 [] = {0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype218_1 [] = {217,0xFFF8,1,0xFFFF};

static const EIF_TYPE_INDEX *gtypes218 [] = {
g_atype218_0,
g_atype218_1,
};

static const int32 cn_attr218 [] =
{
533,
537,
};

extern const char *names219[];
static const uint32 types219 [] =
{
SK_REF,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags219 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype219_0 [] = {217,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype219_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype219_2 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes219 [] = {
g_atype219_0,
g_atype219_1,
g_atype219_2,
};

static const int32 cn_attr219 [] =
{
1184,
1185,
1186,
};

extern const char *names220[];
static const uint32 types220 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags220 [] =
{0,0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype220_0 [] = {0xFF01,216,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype220_1 [] = {217,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype220_2 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype220_3 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype220_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype220_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype220_6 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype220_7 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes220 [] = {
g_atype220_0,
g_atype220_1,
g_atype220_2,
g_atype220_3,
g_atype220_4,
g_atype220_5,
g_atype220_6,
g_atype220_7,
};

static const int32 cn_attr220 [] =
{
1789,
1791,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names221[];
static const uint32 types221 [] =
{
SK_BOOL,
};

static const uint16 attr_flags221 [] =
{0,};

static const EIF_TYPE_INDEX g_atype221_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes221 [] = {
g_atype221_0,
};

static const int32 cn_attr221 [] =
{
877,
};

extern const char *names222[];
static const uint32 types222 [] =
{
SK_BOOL,
};

static const uint16 attr_flags222 [] =
{0,};

static const EIF_TYPE_INDEX g_atype222_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes222 [] = {
g_atype222_0,
};

static const int32 cn_attr222 [] =
{
877,
};

extern const char *names223[];
static const uint32 types223 [] =
{
SK_BOOL,
};

static const uint16 attr_flags223 [] =
{0,};

static const EIF_TYPE_INDEX g_atype223_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes223 [] = {
g_atype223_0,
};

static const int32 cn_attr223 [] =
{
877,
};

extern const char *names224[];
static const uint32 types224 [] =
{
SK_BOOL,
};

static const uint16 attr_flags224 [] =
{0,};

static const EIF_TYPE_INDEX g_atype224_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes224 [] = {
g_atype224_0,
};

static const int32 cn_attr224 [] =
{
877,
};

extern const char *names225[];
static const uint32 types225 [] =
{
SK_BOOL,
};

static const uint16 attr_flags225 [] =
{0,};

static const EIF_TYPE_INDEX g_atype225_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes225 [] = {
g_atype225_0,
};

static const int32 cn_attr225 [] =
{
877,
};

extern const char *names226[];
static const uint32 types226 [] =
{
SK_BOOL,
};

static const uint16 attr_flags226 [] =
{0,};

static const EIF_TYPE_INDEX g_atype226_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes226 [] = {
g_atype226_0,
};

static const int32 cn_attr226 [] =
{
877,
};

extern const char *names227[];
static const uint32 types227 [] =
{
SK_BOOL,
};

static const uint16 attr_flags227 [] =
{0,};

static const EIF_TYPE_INDEX g_atype227_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes227 [] = {
g_atype227_0,
};

static const int32 cn_attr227 [] =
{
877,
};

extern const char *names228[];
static const uint32 types228 [] =
{
SK_BOOL,
};

static const uint16 attr_flags228 [] =
{0,};

static const EIF_TYPE_INDEX g_atype228_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes228 [] = {
g_atype228_0,
};

static const int32 cn_attr228 [] =
{
877,
};

extern const char *names229[];
static const uint32 types229 [] =
{
SK_BOOL,
};

static const uint16 attr_flags229 [] =
{0,};

static const EIF_TYPE_INDEX g_atype229_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes229 [] = {
g_atype229_0,
};

static const int32 cn_attr229 [] =
{
877,
};

extern const char *names230[];
static const uint32 types230 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags230 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype230_0 [] = {0xFF01,230,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype230_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype230_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype230_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype230_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype230_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype230_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes230 [] = {
g_atype230_0,
g_atype230_1,
g_atype230_2,
g_atype230_3,
g_atype230_4,
g_atype230_5,
g_atype230_6,
};

static const int32 cn_attr230 [] =
{
1789,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names234[];
static const uint32 types234 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags234 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype234_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_1 [] = {0xFF01,178,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_2 [] = {0xFF01,178,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_3 [] = {0xFF01,248,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_4 [] = {0xFF01,597,158,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_5 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_6 [] = {0xFF02,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_7 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_8 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_9 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_10 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_11 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_12 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_13 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_14 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_15 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_16 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes234 [] = {
g_atype234_0,
g_atype234_1,
g_atype234_2,
g_atype234_3,
g_atype234_4,
g_atype234_5,
g_atype234_6,
g_atype234_7,
g_atype234_8,
g_atype234_9,
g_atype234_10,
g_atype234_11,
g_atype234_12,
g_atype234_13,
g_atype234_14,
g_atype234_15,
g_atype234_16,
};

static const int32 cn_attr234 [] =
{
1962,
1997,
1998,
1999,
2000,
2012,
2013,
877,
1996,
2002,
1970,
2001,
2003,
2006,
2007,
2011,
2047,
};

extern const char *names237[];
static const uint32 types237 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags237 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype237_0 [] = {0xFF02,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_1 [] = {0xFFF9,0,120,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_2 [] = {249,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_3 [] = {249,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_4 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_6 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_7 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_8 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_9 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_10 [] = {161,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_11 [] = {161,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_12 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes237 [] = {
g_atype237_0,
g_atype237_1,
g_atype237_2,
g_atype237_3,
g_atype237_4,
g_atype237_5,
g_atype237_6,
g_atype237_7,
g_atype237_8,
g_atype237_9,
g_atype237_10,
g_atype237_11,
g_atype237_12,
};

static const int32 cn_attr237 [] =
{
2874,
2890,
2894,
2902,
2882,
2897,
2912,
2883,
2896,
2898,
2892,
2893,
2895,
};

extern const char *names238[];
static const uint32 types238 [] =
{
SK_BOOL,
};

static const uint16 attr_flags238 [] =
{0,};

static const EIF_TYPE_INDEX g_atype238_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes238 [] = {
g_atype238_0,
};

static const int32 cn_attr238 [] =
{
877,
};

extern const char *names239[];
static const uint32 types239 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags239 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype239_0 [] = {0xFF01,233,0xFFF8,1,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype239_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype239_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype239_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype239_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype239_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype239_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes239 [] = {
g_atype239_0,
g_atype239_1,
g_atype239_2,
g_atype239_3,
g_atype239_4,
g_atype239_5,
g_atype239_6,
};

static const int32 cn_attr239 [] =
{
1789,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names240[];
static const uint32 types240 [] =
{
SK_REF,
};

static const uint16 attr_flags240 [] =
{0,};

static const EIF_TYPE_INDEX g_atype240_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes240 [] = {
g_atype240_0,
};

static const int32 cn_attr240 [] =
{
2139,
};

extern const char *names241[];
static const uint32 types241 [] =
{
SK_REF,
};

static const uint16 attr_flags241 [] =
{0,};

static const EIF_TYPE_INDEX g_atype241_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes241 [] = {
g_atype241_0,
};

static const int32 cn_attr241 [] =
{
2139,
};

extern const char *names242[];
static const uint32 types242 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags242 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype242_0 [] = {0xFF01,248,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_1 [] = {0xFF01,248,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_2 [] = {0xFF01,248,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_3 [] = {0xFF01,597,158,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_4 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_6 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_7 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_8 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_9 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_10 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_11 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_12 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_13 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_14 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_15 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_16 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes242 [] = {
g_atype242_0,
g_atype242_1,
g_atype242_2,
g_atype242_3,
g_atype242_4,
g_atype242_5,
g_atype242_6,
g_atype242_7,
g_atype242_8,
g_atype242_9,
g_atype242_10,
g_atype242_11,
g_atype242_12,
g_atype242_13,
g_atype242_14,
g_atype242_15,
g_atype242_16,
};

static const int32 cn_attr242 [] =
{
1997,
1998,
1999,
2000,
877,
1996,
2002,
1962,
1970,
2001,
2003,
2006,
2007,
2011,
2012,
2013,
2047,
};

extern const char *names245[];
static const uint32 types245 [] =
{
SK_REF,
};

static const uint16 attr_flags245 [] =
{0,};

static const EIF_TYPE_INDEX g_atype245_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes245 [] = {
g_atype245_0,
};

static const int32 cn_attr245 [] =
{
2139,
};

extern const char *names246[];
static const uint32 types246 [] =
{
SK_BOOL,
};

static const uint16 attr_flags246 [] =
{0,};

static const EIF_TYPE_INDEX g_atype246_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes246 [] = {
g_atype246_0,
};

static const int32 cn_attr246 [] =
{
877,
};

extern const char *names247[];
static const uint32 types247 [] =
{
SK_BOOL,
};

static const uint16 attr_flags247 [] =
{0,};

static const EIF_TYPE_INDEX g_atype247_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes247 [] = {
g_atype247_0,
};

static const int32 cn_attr247 [] =
{
877,
};

extern const char *names248[];
static const uint32 types248 [] =
{
SK_BOOL,
};

static const uint16 attr_flags248 [] =
{0,};

static const EIF_TYPE_INDEX g_atype248_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes248 [] = {
g_atype248_0,
};

static const int32 cn_attr248 [] =
{
877,
};

extern const char *names250[];
static const uint32 types250 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags250 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype250_0 [] = {0xFF01,248,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype250_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype250_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype250_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes250 [] = {
g_atype250_0,
g_atype250_1,
g_atype250_2,
g_atype250_3,
};

static const int32 cn_attr250 [] =
{
1632,
877,
1866,
1867,
};

extern const char *names251[];
static const uint32 types251 [] =
{
SK_BOOL,
};

static const uint16 attr_flags251 [] =
{0,};

static const EIF_TYPE_INDEX g_atype251_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes251 [] = {
g_atype251_0,
};

static const int32 cn_attr251 [] =
{
877,
};

extern const char *names252[];
static const uint32 types252 [] =
{
SK_BOOL,
};

static const uint16 attr_flags252 [] =
{0,};

static const EIF_TYPE_INDEX g_atype252_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes252 [] = {
g_atype252_0,
};

static const int32 cn_attr252 [] =
{
877,
};

extern const char *names253[];
static const uint32 types253 [] =
{
SK_REF,
};

static const uint16 attr_flags253 [] =
{0,};

static const EIF_TYPE_INDEX g_atype253_0 [] = {0xFF01,248,122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes253 [] = {
g_atype253_0,
};

static const int32 cn_attr253 [] =
{
1632,
};

extern const char *names255[];
static const uint32 types255 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags255 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype255_0 [] = {0xFF01,248,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype255_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype255_2 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes255 [] = {
g_atype255_0,
g_atype255_1,
g_atype255_2,
};

static const int32 cn_attr255 [] =
{
1632,
877,
1958,
};

extern const char *names256[];
static const uint32 types256 [] =
{
SK_BOOL,
};

static const uint16 attr_flags256 [] =
{0,};

static const EIF_TYPE_INDEX g_atype256_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes256 [] = {
g_atype256_0,
};

static const int32 cn_attr256 [] =
{
877,
};

extern const char *names257[];
static const uint32 types257 [] =
{
SK_BOOL,
};

static const uint16 attr_flags257 [] =
{0,};

static const EIF_TYPE_INDEX g_atype257_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes257 [] = {
g_atype257_0,
};

static const int32 cn_attr257 [] =
{
877,
};

extern const char *names258[];
static const uint32 types258 [] =
{
SK_BOOL,
};

static const uint16 attr_flags258 [] =
{0,};

static const EIF_TYPE_INDEX g_atype258_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes258 [] = {
g_atype258_0,
};

static const int32 cn_attr258 [] =
{
877,
};

extern const char *names259[];
static const uint32 types259 [] =
{
SK_BOOL,
};

static const uint16 attr_flags259 [] =
{0,};

static const EIF_TYPE_INDEX g_atype259_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes259 [] = {
g_atype259_0,
};

static const int32 cn_attr259 [] =
{
877,
};

extern const char *names260[];
static const uint32 types260 [] =
{
SK_BOOL,
};

static const uint16 attr_flags260 [] =
{0,};

static const EIF_TYPE_INDEX g_atype260_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes260 [] = {
g_atype260_0,
};

static const int32 cn_attr260 [] =
{
877,
};

extern const char *names261[];
static const uint32 types261 [] =
{
SK_BOOL,
};

static const uint16 attr_flags261 [] =
{0,};

static const EIF_TYPE_INDEX g_atype261_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes261 [] = {
g_atype261_0,
};

static const int32 cn_attr261 [] =
{
877,
};

extern const char *names262[];
static const uint32 types262 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags262 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype262_0 [] = {0xFF01,241,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype262_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype262_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype262_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype262_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype262_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype262_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes262 [] = {
g_atype262_0,
g_atype262_1,
g_atype262_2,
g_atype262_3,
g_atype262_4,
g_atype262_5,
g_atype262_6,
};

static const int32 cn_attr262 [] =
{
1789,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names263[];
static const uint32 types263 [] =
{
SK_BOOL,
};

static const uint16 attr_flags263 [] =
{0,};

static const EIF_TYPE_INDEX g_atype263_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes263 [] = {
g_atype263_0,
};

static const int32 cn_attr263 [] =
{
877,
};

extern const char *names264[];
static const uint32 types264 [] =
{
SK_BOOL,
};

static const uint16 attr_flags264 [] =
{0,};

static const EIF_TYPE_INDEX g_atype264_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes264 [] = {
g_atype264_0,
};

static const int32 cn_attr264 [] =
{
877,
};

extern const char *names265[];
static const uint32 types265 [] =
{
SK_BOOL,
};

static const uint16 attr_flags265 [] =
{0,};

static const EIF_TYPE_INDEX g_atype265_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes265 [] = {
g_atype265_0,
};

static const int32 cn_attr265 [] =
{
877,
};

extern const char *names266[];
static const uint32 types266 [] =
{
SK_BOOL,
};

static const uint16 attr_flags266 [] =
{0,};

static const EIF_TYPE_INDEX g_atype266_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes266 [] = {
g_atype266_0,
};

static const int32 cn_attr266 [] =
{
877,
};

extern const char *names267[];
static const uint32 types267 [] =
{
SK_BOOL,
};

static const uint16 attr_flags267 [] =
{0,};

static const EIF_TYPE_INDEX g_atype267_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes267 [] = {
g_atype267_0,
};

static const int32 cn_attr267 [] =
{
877,
};

extern const char *names268[];
static const uint32 types268 [] =
{
SK_BOOL,
};

static const uint16 attr_flags268 [] =
{0,};

static const EIF_TYPE_INDEX g_atype268_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes268 [] = {
g_atype268_0,
};

static const int32 cn_attr268 [] =
{
877,
};

extern const char *names269[];
static const uint32 types269 [] =
{
SK_BOOL,
};

static const uint16 attr_flags269 [] =
{0,};

static const EIF_TYPE_INDEX g_atype269_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes269 [] = {
g_atype269_0,
};

static const int32 cn_attr269 [] =
{
877,
};

extern const char *names270[];
static const uint32 types270 [] =
{
SK_REF,
};

static const uint16 attr_flags270 [] =
{0,};

static const EIF_TYPE_INDEX g_atype270_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes270 [] = {
g_atype270_0,
};

static const int32 cn_attr270 [] =
{
2139,
};

extern const char *names271[];
static const uint32 types271 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags271 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype271_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype271_1 [] = {0xFF01,178,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype271_2 [] = {0xFF01,178,0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype271_3 [] = {0xFF01,248,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype271_4 [] = {0xFF01,597,158,0xFFFF};
static const EIF_TYPE_INDEX g_atype271_5 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype271_6 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype271_7 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype271_8 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype271_9 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype271_10 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype271_11 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype271_12 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype271_13 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype271_14 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype271_15 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype271_16 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype271_17 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes271 [] = {
g_atype271_0,
g_atype271_1,
g_atype271_2,
g_atype271_3,
g_atype271_4,
g_atype271_5,
g_atype271_6,
g_atype271_7,
g_atype271_8,
g_atype271_9,
g_atype271_10,
g_atype271_11,
g_atype271_12,
g_atype271_13,
g_atype271_14,
g_atype271_15,
g_atype271_16,
g_atype271_17,
};

static const int32 cn_attr271 [] =
{
1962,
1997,
1998,
1999,
2000,
2012,
2013,
877,
1996,
2002,
2050,
1970,
2001,
2003,
2006,
2007,
2011,
2047,
};

extern const char *names272[];
static const uint32 types272 [] =
{
SK_REF,
};

static const uint16 attr_flags272 [] =
{0,};

static const EIF_TYPE_INDEX g_atype272_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes272 [] = {
g_atype272_0,
};

static const int32 cn_attr272 [] =
{
2139,
};

extern const char *names273[];
static const uint32 types273 [] =
{
SK_REF,
};

static const uint16 attr_flags273 [] =
{0,};

static const EIF_TYPE_INDEX g_atype273_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes273 [] = {
g_atype273_0,
};

static const int32 cn_attr273 [] =
{
2139,
};

extern const char *names274[];
static const uint32 types274 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags274 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype274_0 [] = {0xFF01,290,143,0xFFFF};
static const EIF_TYPE_INDEX g_atype274_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype274_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype274_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes274 [] = {
g_atype274_0,
g_atype274_1,
g_atype274_2,
g_atype274_3,
};

static const int32 cn_attr274 [] =
{
1632,
877,
1866,
1867,
};

extern const char *names275[];
static const uint32 types275 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags275 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype275_0 [] = {0xFF01,275,143,0xFFFF};
static const EIF_TYPE_INDEX g_atype275_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype275_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype275_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype275_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype275_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype275_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes275 [] = {
g_atype275_0,
g_atype275_1,
g_atype275_2,
g_atype275_3,
g_atype275_4,
g_atype275_5,
g_atype275_6,
};

static const int32 cn_attr275 [] =
{
1789,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names279[];
static const uint32 types279 [] =
{
SK_BOOL,
};

static const uint16 attr_flags279 [] =
{0,};

static const EIF_TYPE_INDEX g_atype279_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes279 [] = {
g_atype279_0,
};

static const int32 cn_attr279 [] =
{
877,
};

extern const char *names280[];
static const uint32 types280 [] =
{
SK_BOOL,
};

static const uint16 attr_flags280 [] =
{0,};

static const EIF_TYPE_INDEX g_atype280_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes280 [] = {
g_atype280_0,
};

static const int32 cn_attr280 [] =
{
877,
};

extern const char *names281[];
static const uint32 types281 [] =
{
SK_BOOL,
};

static const uint16 attr_flags281 [] =
{0,};

static const EIF_TYPE_INDEX g_atype281_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes281 [] = {
g_atype281_0,
};

static const int32 cn_attr281 [] =
{
877,
};

extern const char *names282[];
static const uint32 types282 [] =
{
SK_BOOL,
};

static const uint16 attr_flags282 [] =
{0,};

static const EIF_TYPE_INDEX g_atype282_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes282 [] = {
g_atype282_0,
};

static const int32 cn_attr282 [] =
{
877,
};

extern const char *names283[];
static const uint32 types283 [] =
{
SK_BOOL,
};

static const uint16 attr_flags283 [] =
{0,};

static const EIF_TYPE_INDEX g_atype283_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes283 [] = {
g_atype283_0,
};

static const int32 cn_attr283 [] =
{
877,
};

extern const char *names284[];
static const uint32 types284 [] =
{
SK_BOOL,
};

static const uint16 attr_flags284 [] =
{0,};

static const EIF_TYPE_INDEX g_atype284_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes284 [] = {
g_atype284_0,
};

static const int32 cn_attr284 [] =
{
877,
};

extern const char *names285[];
static const uint32 types285 [] =
{
SK_BOOL,
};

static const uint16 attr_flags285 [] =
{0,};

static const EIF_TYPE_INDEX g_atype285_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes285 [] = {
g_atype285_0,
};

static const int32 cn_attr285 [] =
{
877,
};

extern const char *names286[];
static const uint32 types286 [] =
{
SK_BOOL,
};

static const uint16 attr_flags286 [] =
{0,};

static const EIF_TYPE_INDEX g_atype286_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes286 [] = {
g_atype286_0,
};

static const int32 cn_attr286 [] =
{
877,
};

extern const char *names287[];
static const uint32 types287 [] =
{
SK_BOOL,
};

static const uint16 attr_flags287 [] =
{0,};

static const EIF_TYPE_INDEX g_atype287_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes287 [] = {
g_atype287_0,
};

static const int32 cn_attr287 [] =
{
877,
};

extern const char *names288[];
static const uint32 types288 [] =
{
SK_BOOL,
};

static const uint16 attr_flags288 [] =
{0,};

static const EIF_TYPE_INDEX g_atype288_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes288 [] = {
g_atype288_0,
};

static const int32 cn_attr288 [] =
{
877,
};

extern const char *names289[];
static const uint32 types289 [] =
{
SK_BOOL,
};

static const uint16 attr_flags289 [] =
{0,};

static const EIF_TYPE_INDEX g_atype289_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes289 [] = {
g_atype289_0,
};

static const int32 cn_attr289 [] =
{
877,
};

extern const char *names290[];
static const uint32 types290 [] =
{
SK_REF,
};

static const uint16 attr_flags290 [] =
{0,};

static const EIF_TYPE_INDEX g_atype290_0 [] = {0xFF01,290,143,0xFFFF};

static const EIF_TYPE_INDEX *gtypes290 [] = {
g_atype290_0,
};

static const int32 cn_attr290 [] =
{
1632,
};

extern const char *names292[];
static const uint32 types292 [] =
{
SK_REF,
};

static const uint16 attr_flags292 [] =
{0,};

static const EIF_TYPE_INDEX g_atype292_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes292 [] = {
g_atype292_0,
};

static const int32 cn_attr292 [] =
{
2139,
};

extern const char *names294[];
static const uint32 types294 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags294 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype294_0 [] = {0xFF01,290,143,0xFFFF};
static const EIF_TYPE_INDEX g_atype294_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype294_2 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes294 [] = {
g_atype294_0,
g_atype294_1,
g_atype294_2,
};

static const int32 cn_attr294 [] =
{
1632,
877,
1958,
};

extern const char *names295[];
static const uint32 types295 [] =
{
SK_BOOL,
};

static const uint16 attr_flags295 [] =
{0,};

static const EIF_TYPE_INDEX g_atype295_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes295 [] = {
g_atype295_0,
};

static const int32 cn_attr295 [] =
{
877,
};

extern const char *names296[];
static const uint32 types296 [] =
{
SK_BOOL,
};

static const uint16 attr_flags296 [] =
{0,};

static const EIF_TYPE_INDEX g_atype296_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes296 [] = {
g_atype296_0,
};

static const int32 cn_attr296 [] =
{
877,
};

extern const char *names297[];
static const uint32 types297 [] =
{
SK_BOOL,
};

static const uint16 attr_flags297 [] =
{0,};

static const EIF_TYPE_INDEX g_atype297_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes297 [] = {
g_atype297_0,
};

static const int32 cn_attr297 [] =
{
877,
};

extern const char *names298[];
static const uint32 types298 [] =
{
SK_BOOL,
};

static const uint16 attr_flags298 [] =
{0,};

static const EIF_TYPE_INDEX g_atype298_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes298 [] = {
g_atype298_0,
};

static const int32 cn_attr298 [] =
{
877,
};

extern const char *names299[];
static const uint32 types299 [] =
{
SK_BOOL,
};

static const uint16 attr_flags299 [] =
{0,};

static const EIF_TYPE_INDEX g_atype299_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes299 [] = {
g_atype299_0,
};

static const int32 cn_attr299 [] =
{
877,
};

extern const char *names300[];
static const uint32 types300 [] =
{
SK_BOOL,
};

static const uint16 attr_flags300 [] =
{0,};

static const EIF_TYPE_INDEX g_atype300_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes300 [] = {
g_atype300_0,
};

static const int32 cn_attr300 [] =
{
877,
};

extern const char *names301[];
static const uint32 types301 [] =
{
SK_BOOL,
};

static const uint16 attr_flags301 [] =
{0,};

static const EIF_TYPE_INDEX g_atype301_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes301 [] = {
g_atype301_0,
};

static const int32 cn_attr301 [] =
{
877,
};

extern const char *names302[];
static const uint32 types302 [] =
{
SK_BOOL,
};

static const uint16 attr_flags302 [] =
{0,};

static const EIF_TYPE_INDEX g_atype302_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes302 [] = {
g_atype302_0,
};

static const int32 cn_attr302 [] =
{
877,
};

extern const char *names303[];
static const uint32 types303 [] =
{
SK_BOOL,
};

static const uint16 attr_flags303 [] =
{0,};

static const EIF_TYPE_INDEX g_atype303_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes303 [] = {
g_atype303_0,
};

static const int32 cn_attr303 [] =
{
877,
};

extern const char *names304[];
static const uint32 types304 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags304 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype304_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype304_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype304_2 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype304_3 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype304_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype304_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes304 [] = {
g_atype304_0,
g_atype304_1,
g_atype304_2,
g_atype304_3,
g_atype304_4,
g_atype304_5,
};

static const int32 cn_attr304 [] =
{
3400,
3428,
3429,
3430,
3398,
3399,
};

extern const char *names306[];
static const uint32 types306 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags306 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype306_0 [] = {0xFF01,306,131,0xFFFF};
static const EIF_TYPE_INDEX g_atype306_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype306_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype306_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype306_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype306_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype306_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes306 [] = {
g_atype306_0,
g_atype306_1,
g_atype306_2,
g_atype306_3,
g_atype306_4,
g_atype306_5,
g_atype306_6,
};

static const int32 cn_attr306 [] =
{
1789,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names310[];
static const uint32 types310 [] =
{
SK_REF,
};

static const uint16 attr_flags310 [] =
{0,};

static const EIF_TYPE_INDEX g_atype310_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes310 [] = {
g_atype310_0,
};

static const int32 cn_attr310 [] =
{
2139,
};

extern const char *names311[];
static const uint32 types311 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags311 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype311_0 [] = {0xFF01,304,131,0xFFFF};
static const EIF_TYPE_INDEX g_atype311_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype311_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype311_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes311 [] = {
g_atype311_0,
g_atype311_1,
g_atype311_2,
g_atype311_3,
};

static const int32 cn_attr311 [] =
{
1632,
877,
1866,
1867,
};

extern const char *names312[];
static const uint32 types312 [] =
{
SK_BOOL,
};

static const uint16 attr_flags312 [] =
{0,};

static const EIF_TYPE_INDEX g_atype312_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes312 [] = {
g_atype312_0,
};

static const int32 cn_attr312 [] =
{
877,
};

extern const char *names313[];
static const uint32 types313 [] =
{
SK_BOOL,
};

static const uint16 attr_flags313 [] =
{0,};

static const EIF_TYPE_INDEX g_atype313_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes313 [] = {
g_atype313_0,
};

static const int32 cn_attr313 [] =
{
877,
};

extern const char *names314[];
static const uint32 types314 [] =
{
SK_BOOL,
};

static const uint16 attr_flags314 [] =
{0,};

static const EIF_TYPE_INDEX g_atype314_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes314 [] = {
g_atype314_0,
};

static const int32 cn_attr314 [] =
{
877,
};

extern const char *names315[];
static const uint32 types315 [] =
{
SK_BOOL,
};

static const uint16 attr_flags315 [] =
{0,};

static const EIF_TYPE_INDEX g_atype315_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes315 [] = {
g_atype315_0,
};

static const int32 cn_attr315 [] =
{
877,
};

extern const char *names316[];
static const uint32 types316 [] =
{
SK_BOOL,
};

static const uint16 attr_flags316 [] =
{0,};

static const EIF_TYPE_INDEX g_atype316_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes316 [] = {
g_atype316_0,
};

static const int32 cn_attr316 [] =
{
877,
};

extern const char *names317[];
static const uint32 types317 [] =
{
SK_BOOL,
};

static const uint16 attr_flags317 [] =
{0,};

static const EIF_TYPE_INDEX g_atype317_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes317 [] = {
g_atype317_0,
};

static const int32 cn_attr317 [] =
{
877,
};

extern const char *names318[];
static const uint32 types318 [] =
{
SK_BOOL,
};

static const uint16 attr_flags318 [] =
{0,};

static const EIF_TYPE_INDEX g_atype318_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes318 [] = {
g_atype318_0,
};

static const int32 cn_attr318 [] =
{
877,
};

extern const char *names319[];
static const uint32 types319 [] =
{
SK_BOOL,
};

static const uint16 attr_flags319 [] =
{0,};

static const EIF_TYPE_INDEX g_atype319_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes319 [] = {
g_atype319_0,
};

static const int32 cn_attr319 [] =
{
877,
};

extern const char *names320[];
static const uint32 types320 [] =
{
SK_BOOL,
};

static const uint16 attr_flags320 [] =
{0,};

static const EIF_TYPE_INDEX g_atype320_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes320 [] = {
g_atype320_0,
};

static const int32 cn_attr320 [] =
{
877,
};

extern const char *names321[];
static const uint32 types321 [] =
{
SK_BOOL,
};

static const uint16 attr_flags321 [] =
{0,};

static const EIF_TYPE_INDEX g_atype321_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes321 [] = {
g_atype321_0,
};

static const int32 cn_attr321 [] =
{
877,
};

extern const char *names322[];
static const uint32 types322 [] =
{
SK_BOOL,
};

static const uint16 attr_flags322 [] =
{0,};

static const EIF_TYPE_INDEX g_atype322_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes322 [] = {
g_atype322_0,
};

static const int32 cn_attr322 [] =
{
877,
};

extern const char *names323[];
static const uint32 types323 [] =
{
SK_REF,
};

static const uint16 attr_flags323 [] =
{0,};

static const EIF_TYPE_INDEX g_atype323_0 [] = {0xFF01,304,131,0xFFFF};

static const EIF_TYPE_INDEX *gtypes323 [] = {
g_atype323_0,
};

static const int32 cn_attr323 [] =
{
1632,
};

extern const char *names325[];
static const uint32 types325 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags325 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype325_0 [] = {0xFF01,304,131,0xFFFF};
static const EIF_TYPE_INDEX g_atype325_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype325_2 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes325 [] = {
g_atype325_0,
g_atype325_1,
g_atype325_2,
};

static const int32 cn_attr325 [] =
{
1632,
877,
1958,
};

extern const char *names326[];
static const uint32 types326 [] =
{
SK_BOOL,
};

static const uint16 attr_flags326 [] =
{0,};

static const EIF_TYPE_INDEX g_atype326_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes326 [] = {
g_atype326_0,
};

static const int32 cn_attr326 [] =
{
877,
};

extern const char *names327[];
static const uint32 types327 [] =
{
SK_BOOL,
};

static const uint16 attr_flags327 [] =
{0,};

static const EIF_TYPE_INDEX g_atype327_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes327 [] = {
g_atype327_0,
};

static const int32 cn_attr327 [] =
{
877,
};

extern const char *names328[];
static const uint32 types328 [] =
{
SK_BOOL,
};

static const uint16 attr_flags328 [] =
{0,};

static const EIF_TYPE_INDEX g_atype328_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes328 [] = {
g_atype328_0,
};

static const int32 cn_attr328 [] =
{
877,
};

extern const char *names329[];
static const uint32 types329 [] =
{
SK_BOOL,
};

static const uint16 attr_flags329 [] =
{0,};

static const EIF_TYPE_INDEX g_atype329_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes329 [] = {
g_atype329_0,
};

static const int32 cn_attr329 [] =
{
877,
};

extern const char *names330[];
static const uint32 types330 [] =
{
SK_BOOL,
};

static const uint16 attr_flags330 [] =
{0,};

static const EIF_TYPE_INDEX g_atype330_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes330 [] = {
g_atype330_0,
};

static const int32 cn_attr330 [] =
{
877,
};

extern const char *names331[];
static const uint32 types331 [] =
{
SK_BOOL,
};

static const uint16 attr_flags331 [] =
{0,};

static const EIF_TYPE_INDEX g_atype331_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes331 [] = {
g_atype331_0,
};

static const int32 cn_attr331 [] =
{
877,
};

extern const char *names332[];
static const uint32 types332 [] =
{
SK_BOOL,
};

static const uint16 attr_flags332 [] =
{0,};

static const EIF_TYPE_INDEX g_atype332_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes332 [] = {
g_atype332_0,
};

static const int32 cn_attr332 [] =
{
877,
};

extern const char *names333[];
static const uint32 types333 [] =
{
SK_BOOL,
};

static const uint16 attr_flags333 [] =
{0,};

static const EIF_TYPE_INDEX g_atype333_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes333 [] = {
g_atype333_0,
};

static const int32 cn_attr333 [] =
{
877,
};

extern const char *names334[];
static const uint32 types334 [] =
{
SK_BOOL,
};

static const uint16 attr_flags334 [] =
{0,};

static const EIF_TYPE_INDEX g_atype334_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes334 [] = {
g_atype334_0,
};

static const int32 cn_attr334 [] =
{
877,
};

extern const char *names335[];
static const uint32 types335 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags335 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype335_0 [] = {0xFF01,178,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype335_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype335_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype335_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes335 [] = {
g_atype335_0,
g_atype335_1,
g_atype335_2,
g_atype335_3,
};

static const int32 cn_attr335 [] =
{
1937,
877,
1938,
1943,
};

extern const char *names336[];
static const uint32 types336 [] =
{
SK_BOOL,
};

static const uint16 attr_flags336 [] =
{0,};

static const EIF_TYPE_INDEX g_atype336_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes336 [] = {
g_atype336_0,
};

static const int32 cn_attr336 [] =
{
877,
};

extern const char *names337[];
static const uint32 types337 [] =
{
SK_BOOL,
};

static const uint16 attr_flags337 [] =
{0,};

static const EIF_TYPE_INDEX g_atype337_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes337 [] = {
g_atype337_0,
};

static const int32 cn_attr337 [] =
{
877,
};

extern const char *names338[];
static const uint32 types338 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags338 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype338_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype338_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype338_2 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype338_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype338_4 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes338 [] = {
g_atype338_0,
g_atype338_1,
g_atype338_2,
g_atype338_3,
g_atype338_4,
};

static const int32 cn_attr338 [] =
{
3400,
3421,
3422,
3398,
3399,
};

extern const char *names339[];
static const uint32 types339 [] =
{
SK_REF,
};

static const uint16 attr_flags339 [] =
{0,};

static const EIF_TYPE_INDEX g_atype339_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes339 [] = {
g_atype339_0,
};

static const int32 cn_attr339 [] =
{
2139,
};

extern const char *names340[];
static const uint32 types340 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags340 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype340_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype340_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype340_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype340_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype340_4 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes340 [] = {
g_atype340_0,
g_atype340_1,
g_atype340_2,
g_atype340_3,
g_atype340_4,
};

static const int32 cn_attr340 [] =
{
3400,
3421,
3422,
3398,
3399,
};

extern const char *names341[];
static const uint32 types341 [] =
{
SK_POINTER,
};

static const uint16 attr_flags341 [] =
{0,};

static const EIF_TYPE_INDEX g_atype341_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes341 [] = {
g_atype341_0,
};

static const int32 cn_attr341 [] =
{
2849,
};

extern const char *names342[];
static const uint32 types342 [] =
{
SK_POINTER,
};

static const uint16 attr_flags342 [] =
{0,};

static const EIF_TYPE_INDEX g_atype342_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes342 [] = {
g_atype342_0,
};

static const int32 cn_attr342 [] =
{
2849,
};

extern const char *names343[];
static const uint32 types343 [] =
{
SK_REF,
};

static const uint16 attr_flags343 [] =
{0,};

static const EIF_TYPE_INDEX g_atype343_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes343 [] = {
g_atype343_0,
};

static const int32 cn_attr343 [] =
{
2139,
};

extern const char *names344[];
static const uint32 types344 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags344 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype344_0 [] = {0xFF01,344,149,0xFFFF};
static const EIF_TYPE_INDEX g_atype344_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype344_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype344_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype344_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype344_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype344_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes344 [] = {
g_atype344_0,
g_atype344_1,
g_atype344_2,
g_atype344_3,
g_atype344_4,
g_atype344_5,
g_atype344_6,
};

static const int32 cn_attr344 [] =
{
1789,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names348[];
static const uint32 types348 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags348 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype348_0 [] = {0xFF01,348,155,0xFFFF};
static const EIF_TYPE_INDEX g_atype348_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype348_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype348_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype348_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype348_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype348_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes348 [] = {
g_atype348_0,
g_atype348_1,
g_atype348_2,
g_atype348_3,
g_atype348_4,
g_atype348_5,
g_atype348_6,
};

static const int32 cn_attr348 [] =
{
1789,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names351[];
static const uint32 types351 [] =
{
SK_BOOL,
};

static const uint16 attr_flags351 [] =
{0,};

static const EIF_TYPE_INDEX g_atype351_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes351 [] = {
g_atype351_0,
};

static const int32 cn_attr351 [] =
{
877,
};

extern const char *names352[];
static const uint32 types352 [] =
{
SK_BOOL,
};

static const uint16 attr_flags352 [] =
{0,};

static const EIF_TYPE_INDEX g_atype352_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes352 [] = {
g_atype352_0,
};

static const int32 cn_attr352 [] =
{
877,
};

extern const char *names353[];
static const uint32 types353 [] =
{
SK_BOOL,
};

static const uint16 attr_flags353 [] =
{0,};

static const EIF_TYPE_INDEX g_atype353_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes353 [] = {
g_atype353_0,
};

static const int32 cn_attr353 [] =
{
877,
};

extern const char *names354[];
static const uint32 types354 [] =
{
SK_BOOL,
};

static const uint16 attr_flags354 [] =
{0,};

static const EIF_TYPE_INDEX g_atype354_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes354 [] = {
g_atype354_0,
};

static const int32 cn_attr354 [] =
{
877,
};

extern const char *names355[];
static const uint32 types355 [] =
{
SK_BOOL,
};

static const uint16 attr_flags355 [] =
{0,};

static const EIF_TYPE_INDEX g_atype355_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes355 [] = {
g_atype355_0,
};

static const int32 cn_attr355 [] =
{
877,
};

extern const char *names356[];
static const uint32 types356 [] =
{
SK_BOOL,
};

static const uint16 attr_flags356 [] =
{0,};

static const EIF_TYPE_INDEX g_atype356_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes356 [] = {
g_atype356_0,
};

static const int32 cn_attr356 [] =
{
877,
};

extern const char *names357[];
static const uint32 types357 [] =
{
SK_BOOL,
};

static const uint16 attr_flags357 [] =
{0,};

static const EIF_TYPE_INDEX g_atype357_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes357 [] = {
g_atype357_0,
};

static const int32 cn_attr357 [] =
{
877,
};

extern const char *names358[];
static const uint32 types358 [] =
{
SK_BOOL,
};

static const uint16 attr_flags358 [] =
{0,};

static const EIF_TYPE_INDEX g_atype358_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes358 [] = {
g_atype358_0,
};

static const int32 cn_attr358 [] =
{
877,
};

extern const char *names359[];
static const uint32 types359 [] =
{
SK_BOOL,
};

static const uint16 attr_flags359 [] =
{0,};

static const EIF_TYPE_INDEX g_atype359_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes359 [] = {
g_atype359_0,
};

static const int32 cn_attr359 [] =
{
877,
};

extern const char *names360[];
static const uint32 types360 [] =
{
SK_BOOL,
};

static const uint16 attr_flags360 [] =
{0,};

static const EIF_TYPE_INDEX g_atype360_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes360 [] = {
g_atype360_0,
};

static const int32 cn_attr360 [] =
{
877,
};

extern const char *names361[];
static const uint32 types361 [] =
{
SK_BOOL,
};

static const uint16 attr_flags361 [] =
{0,};

static const EIF_TYPE_INDEX g_atype361_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes361 [] = {
g_atype361_0,
};

static const int32 cn_attr361 [] =
{
877,
};

extern const char *names362[];
static const uint32 types362 [] =
{
SK_REF,
};

static const uint16 attr_flags362 [] =
{0,};

static const EIF_TYPE_INDEX g_atype362_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes362 [] = {
g_atype362_0,
};

static const int32 cn_attr362 [] =
{
2139,
};

extern const char *names363[];
static const uint32 types363 [] =
{
SK_REF,
};

static const uint16 attr_flags363 [] =
{0,};

static const EIF_TYPE_INDEX g_atype363_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes363 [] = {
g_atype363_0,
};

static const int32 cn_attr363 [] =
{
2139,
};

extern const char *names364[];
static const uint32 types364 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags364 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype364_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype364_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype364_2 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype364_3 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype364_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype364_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes364 [] = {
g_atype364_0,
g_atype364_1,
g_atype364_2,
g_atype364_3,
g_atype364_4,
g_atype364_5,
};

static const int32 cn_attr364 [] =
{
3400,
3428,
3429,
3430,
3398,
3399,
};

extern const char *names365[];
static const uint32 types365 [] =
{
SK_POINTER,
};

static const uint16 attr_flags365 [] =
{0,};

static const EIF_TYPE_INDEX g_atype365_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes365 [] = {
g_atype365_0,
};

static const int32 cn_attr365 [] =
{
2849,
};

extern const char *names366[];
static const uint32 types366 [] =
{
SK_POINTER,
};

static const uint16 attr_flags366 [] =
{0,};

static const EIF_TYPE_INDEX g_atype366_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes366 [] = {
g_atype366_0,
};

static const int32 cn_attr366 [] =
{
2849,
};

extern const char *names367[];
static const uint32 types367 [] =
{
SK_REF,
};

static const uint16 attr_flags367 [] =
{0,};

static const EIF_TYPE_INDEX g_atype367_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes367 [] = {
g_atype367_0,
};

static const int32 cn_attr367 [] =
{
2139,
};

extern const char *names369[];
static const uint32 types369 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags369 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype369_0 [] = {0xFF01,369,134,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes369 [] = {
g_atype369_0,
g_atype369_1,
g_atype369_2,
g_atype369_3,
g_atype369_4,
g_atype369_5,
g_atype369_6,
};

static const int32 cn_attr369 [] =
{
1789,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names373[];
static const uint32 types373 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags373 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype373_0 [] = {0xFF01,367,134,0xFFFF};
static const EIF_TYPE_INDEX g_atype373_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype373_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype373_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes373 [] = {
g_atype373_0,
g_atype373_1,
g_atype373_2,
g_atype373_3,
};

static const int32 cn_attr373 [] =
{
1632,
877,
1866,
1867,
};

extern const char *names374[];
static const uint32 types374 [] =
{
SK_BOOL,
};

static const uint16 attr_flags374 [] =
{0,};

static const EIF_TYPE_INDEX g_atype374_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes374 [] = {
g_atype374_0,
};

static const int32 cn_attr374 [] =
{
877,
};

extern const char *names375[];
static const uint32 types375 [] =
{
SK_BOOL,
};

static const uint16 attr_flags375 [] =
{0,};

static const EIF_TYPE_INDEX g_atype375_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes375 [] = {
g_atype375_0,
};

static const int32 cn_attr375 [] =
{
877,
};

extern const char *names376[];
static const uint32 types376 [] =
{
SK_BOOL,
};

static const uint16 attr_flags376 [] =
{0,};

static const EIF_TYPE_INDEX g_atype376_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes376 [] = {
g_atype376_0,
};

static const int32 cn_attr376 [] =
{
877,
};

extern const char *names377[];
static const uint32 types377 [] =
{
SK_BOOL,
};

static const uint16 attr_flags377 [] =
{0,};

static const EIF_TYPE_INDEX g_atype377_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes377 [] = {
g_atype377_0,
};

static const int32 cn_attr377 [] =
{
877,
};

extern const char *names378[];
static const uint32 types378 [] =
{
SK_BOOL,
};

static const uint16 attr_flags378 [] =
{0,};

static const EIF_TYPE_INDEX g_atype378_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes378 [] = {
g_atype378_0,
};

static const int32 cn_attr378 [] =
{
877,
};

extern const char *names379[];
static const uint32 types379 [] =
{
SK_BOOL,
};

static const uint16 attr_flags379 [] =
{0,};

static const EIF_TYPE_INDEX g_atype379_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes379 [] = {
g_atype379_0,
};

static const int32 cn_attr379 [] =
{
877,
};

extern const char *names380[];
static const uint32 types380 [] =
{
SK_BOOL,
};

static const uint16 attr_flags380 [] =
{0,};

static const EIF_TYPE_INDEX g_atype380_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes380 [] = {
g_atype380_0,
};

static const int32 cn_attr380 [] =
{
877,
};

extern const char *names381[];
static const uint32 types381 [] =
{
SK_BOOL,
};

static const uint16 attr_flags381 [] =
{0,};

static const EIF_TYPE_INDEX g_atype381_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes381 [] = {
g_atype381_0,
};

static const int32 cn_attr381 [] =
{
877,
};

extern const char *names382[];
static const uint32 types382 [] =
{
SK_BOOL,
};

static const uint16 attr_flags382 [] =
{0,};

static const EIF_TYPE_INDEX g_atype382_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes382 [] = {
g_atype382_0,
};

static const int32 cn_attr382 [] =
{
877,
};

extern const char *names383[];
static const uint32 types383 [] =
{
SK_BOOL,
};

static const uint16 attr_flags383 [] =
{0,};

static const EIF_TYPE_INDEX g_atype383_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes383 [] = {
g_atype383_0,
};

static const int32 cn_attr383 [] =
{
877,
};

extern const char *names384[];
static const uint32 types384 [] =
{
SK_BOOL,
};

static const uint16 attr_flags384 [] =
{0,};

static const EIF_TYPE_INDEX g_atype384_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes384 [] = {
g_atype384_0,
};

static const int32 cn_attr384 [] =
{
877,
};

extern const char *names385[];
static const uint32 types385 [] =
{
SK_REF,
};

static const uint16 attr_flags385 [] =
{0,};

static const EIF_TYPE_INDEX g_atype385_0 [] = {0xFF01,367,134,0xFFFF};

static const EIF_TYPE_INDEX *gtypes385 [] = {
g_atype385_0,
};

static const int32 cn_attr385 [] =
{
1632,
};

extern const char *names387[];
static const uint32 types387 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags387 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype387_0 [] = {0xFF01,367,134,0xFFFF};
static const EIF_TYPE_INDEX g_atype387_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype387_2 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes387 [] = {
g_atype387_0,
g_atype387_1,
g_atype387_2,
};

static const int32 cn_attr387 [] =
{
1632,
877,
1958,
};

extern const char *names388[];
static const uint32 types388 [] =
{
SK_BOOL,
};

static const uint16 attr_flags388 [] =
{0,};

static const EIF_TYPE_INDEX g_atype388_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes388 [] = {
g_atype388_0,
};

static const int32 cn_attr388 [] =
{
877,
};

extern const char *names389[];
static const uint32 types389 [] =
{
SK_BOOL,
};

static const uint16 attr_flags389 [] =
{0,};

static const EIF_TYPE_INDEX g_atype389_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes389 [] = {
g_atype389_0,
};

static const int32 cn_attr389 [] =
{
877,
};

extern const char *names390[];
static const uint32 types390 [] =
{
SK_BOOL,
};

static const uint16 attr_flags390 [] =
{0,};

static const EIF_TYPE_INDEX g_atype390_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes390 [] = {
g_atype390_0,
};

static const int32 cn_attr390 [] =
{
877,
};

extern const char *names391[];
static const uint32 types391 [] =
{
SK_BOOL,
};

static const uint16 attr_flags391 [] =
{0,};

static const EIF_TYPE_INDEX g_atype391_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes391 [] = {
g_atype391_0,
};

static const int32 cn_attr391 [] =
{
877,
};

extern const char *names392[];
static const uint32 types392 [] =
{
SK_BOOL,
};

static const uint16 attr_flags392 [] =
{0,};

static const EIF_TYPE_INDEX g_atype392_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes392 [] = {
g_atype392_0,
};

static const int32 cn_attr392 [] =
{
877,
};

extern const char *names393[];
static const uint32 types393 [] =
{
SK_BOOL,
};

static const uint16 attr_flags393 [] =
{0,};

static const EIF_TYPE_INDEX g_atype393_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes393 [] = {
g_atype393_0,
};

static const int32 cn_attr393 [] =
{
877,
};

extern const char *names394[];
static const uint32 types394 [] =
{
SK_BOOL,
};

static const uint16 attr_flags394 [] =
{0,};

static const EIF_TYPE_INDEX g_atype394_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes394 [] = {
g_atype394_0,
};

static const int32 cn_attr394 [] =
{
877,
};

extern const char *names395[];
static const uint32 types395 [] =
{
SK_BOOL,
};

static const uint16 attr_flags395 [] =
{0,};

static const EIF_TYPE_INDEX g_atype395_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes395 [] = {
g_atype395_0,
};

static const int32 cn_attr395 [] =
{
877,
};

extern const char *names396[];
static const uint32 types396 [] =
{
SK_BOOL,
};

static const uint16 attr_flags396 [] =
{0,};

static const EIF_TYPE_INDEX g_atype396_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes396 [] = {
g_atype396_0,
};

static const int32 cn_attr396 [] =
{
877,
};

extern const char *names397[];
static const uint32 types397 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags397 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype397_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype397_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype397_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype397_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype397_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype397_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes397 [] = {
g_atype397_0,
g_atype397_1,
g_atype397_2,
g_atype397_3,
g_atype397_4,
g_atype397_5,
};

static const int32 cn_attr397 [] =
{
3400,
3413,
3415,
3398,
3399,
3414,
};

extern const char *names398[];
static const uint32 types398 [] =
{
SK_UINT64,
};

static const uint16 attr_flags398 [] =
{0,};

static const EIF_TYPE_INDEX g_atype398_0 [] = {131,0xFFFF};

static const EIF_TYPE_INDEX *gtypes398 [] = {
g_atype398_0,
};

static const int32 cn_attr398 [] =
{
533,
};

extern const char *names399[];
static const uint32 types399 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_REAL32,
};

static const uint16 attr_flags399 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype399_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype399_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype399_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype399_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype399_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype399_5 [] = {143,0xFFFF};

static const EIF_TYPE_INDEX *gtypes399 [] = {
g_atype399_0,
g_atype399_1,
g_atype399_2,
g_atype399_3,
g_atype399_4,
g_atype399_5,
};

static const int32 cn_attr399 [] =
{
3400,
3428,
3430,
3398,
3399,
3429,
};

extern const char *names400[];
static const uint32 types400 [] =
{
SK_POINTER,
};

static const uint16 attr_flags400 [] =
{0,};

static const EIF_TYPE_INDEX g_atype400_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes400 [] = {
g_atype400_0,
};

static const int32 cn_attr400 [] =
{
2849,
};

extern const char *names401[];
static const uint32 types401 [] =
{
SK_POINTER,
};

static const uint16 attr_flags401 [] =
{0,};

static const EIF_TYPE_INDEX g_atype401_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes401 [] = {
g_atype401_0,
};

static const int32 cn_attr401 [] =
{
2849,
};

extern const char *names402[];
static const uint32 types402 [] =
{
SK_REF,
};

static const uint16 attr_flags402 [] =
{0,};

static const EIF_TYPE_INDEX g_atype402_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes402 [] = {
g_atype402_0,
};

static const int32 cn_attr402 [] =
{
2139,
};

extern const char *names403[];
static const uint32 types403 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_REAL32,
};

static const uint16 attr_flags403 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype403_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype403_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype403_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype403_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype403_4 [] = {143,0xFFFF};

static const EIF_TYPE_INDEX *gtypes403 [] = {
g_atype403_0,
g_atype403_1,
g_atype403_2,
g_atype403_3,
g_atype403_4,
};

static const int32 cn_attr403 [] =
{
3400,
3421,
3398,
3399,
3422,
};

extern const char *names404[];
static const uint32 types404 [] =
{
SK_REF,
};

static const uint16 attr_flags404 [] =
{0,};

static const EIF_TYPE_INDEX g_atype404_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes404 [] = {
g_atype404_0,
};

static const int32 cn_attr404 [] =
{
2139,
};

extern const char *names405[];
static const uint32 types405 [] =
{
SK_REF,
};

static const uint16 attr_flags405 [] =
{0,};

static const EIF_TYPE_INDEX g_atype405_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes405 [] = {
g_atype405_0,
};

static const int32 cn_attr405 [] =
{
2139,
};

extern const char *names406[];
static const uint32 types406 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_REAL64,
};

static const uint16 attr_flags406 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype406_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype406_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype406_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype406_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype406_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype406_5 [] = {146,0xFFFF};

static const EIF_TYPE_INDEX *gtypes406 [] = {
g_atype406_0,
g_atype406_1,
g_atype406_2,
g_atype406_3,
g_atype406_4,
g_atype406_5,
};

static const int32 cn_attr406 [] =
{
3400,
3428,
3430,
3398,
3399,
3429,
};

extern const char *names407[];
static const uint32 types407 [] =
{
SK_POINTER,
};

static const uint16 attr_flags407 [] =
{0,};

static const EIF_TYPE_INDEX g_atype407_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes407 [] = {
g_atype407_0,
};

static const int32 cn_attr407 [] =
{
2849,
};

extern const char *names408[];
static const uint32 types408 [] =
{
SK_POINTER,
};

static const uint16 attr_flags408 [] =
{0,};

static const EIF_TYPE_INDEX g_atype408_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes408 [] = {
g_atype408_0,
};

static const int32 cn_attr408 [] =
{
2849,
};

extern const char *names409[];
static const uint32 types409 [] =
{
SK_REF,
};

static const uint16 attr_flags409 [] =
{0,};

static const EIF_TYPE_INDEX g_atype409_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes409 [] = {
g_atype409_0,
};

static const int32 cn_attr409 [] =
{
2139,
};

extern const char *names411[];
static const uint32 types411 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags411 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype411_0 [] = {0xFF01,411,137,0xFFFF};
static const EIF_TYPE_INDEX g_atype411_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype411_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype411_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype411_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype411_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype411_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes411 [] = {
g_atype411_0,
g_atype411_1,
g_atype411_2,
g_atype411_3,
g_atype411_4,
g_atype411_5,
g_atype411_6,
};

static const int32 cn_attr411 [] =
{
1789,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names415[];
static const uint32 types415 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags415 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype415_0 [] = {0xFF01,409,137,0xFFFF};
static const EIF_TYPE_INDEX g_atype415_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype415_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype415_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes415 [] = {
g_atype415_0,
g_atype415_1,
g_atype415_2,
g_atype415_3,
};

static const int32 cn_attr415 [] =
{
1632,
877,
1866,
1867,
};

extern const char *names416[];
static const uint32 types416 [] =
{
SK_BOOL,
};

static const uint16 attr_flags416 [] =
{0,};

static const EIF_TYPE_INDEX g_atype416_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes416 [] = {
g_atype416_0,
};

static const int32 cn_attr416 [] =
{
877,
};

extern const char *names417[];
static const uint32 types417 [] =
{
SK_BOOL,
};

static const uint16 attr_flags417 [] =
{0,};

static const EIF_TYPE_INDEX g_atype417_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes417 [] = {
g_atype417_0,
};

static const int32 cn_attr417 [] =
{
877,
};

extern const char *names418[];
static const uint32 types418 [] =
{
SK_BOOL,
};

static const uint16 attr_flags418 [] =
{0,};

static const EIF_TYPE_INDEX g_atype418_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes418 [] = {
g_atype418_0,
};

static const int32 cn_attr418 [] =
{
877,
};

extern const char *names419[];
static const uint32 types419 [] =
{
SK_BOOL,
};

static const uint16 attr_flags419 [] =
{0,};

static const EIF_TYPE_INDEX g_atype419_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes419 [] = {
g_atype419_0,
};

static const int32 cn_attr419 [] =
{
877,
};

extern const char *names420[];
static const uint32 types420 [] =
{
SK_BOOL,
};

static const uint16 attr_flags420 [] =
{0,};

static const EIF_TYPE_INDEX g_atype420_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes420 [] = {
g_atype420_0,
};

static const int32 cn_attr420 [] =
{
877,
};

extern const char *names421[];
static const uint32 types421 [] =
{
SK_BOOL,
};

static const uint16 attr_flags421 [] =
{0,};

static const EIF_TYPE_INDEX g_atype421_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes421 [] = {
g_atype421_0,
};

static const int32 cn_attr421 [] =
{
877,
};

extern const char *names422[];
static const uint32 types422 [] =
{
SK_BOOL,
};

static const uint16 attr_flags422 [] =
{0,};

static const EIF_TYPE_INDEX g_atype422_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes422 [] = {
g_atype422_0,
};

static const int32 cn_attr422 [] =
{
877,
};

extern const char *names423[];
static const uint32 types423 [] =
{
SK_BOOL,
};

static const uint16 attr_flags423 [] =
{0,};

static const EIF_TYPE_INDEX g_atype423_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes423 [] = {
g_atype423_0,
};

static const int32 cn_attr423 [] =
{
877,
};

extern const char *names424[];
static const uint32 types424 [] =
{
SK_BOOL,
};

static const uint16 attr_flags424 [] =
{0,};

static const EIF_TYPE_INDEX g_atype424_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes424 [] = {
g_atype424_0,
};

static const int32 cn_attr424 [] =
{
877,
};

extern const char *names425[];
static const uint32 types425 [] =
{
SK_BOOL,
};

static const uint16 attr_flags425 [] =
{0,};

static const EIF_TYPE_INDEX g_atype425_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes425 [] = {
g_atype425_0,
};

static const int32 cn_attr425 [] =
{
877,
};

extern const char *names426[];
static const uint32 types426 [] =
{
SK_BOOL,
};

static const uint16 attr_flags426 [] =
{0,};

static const EIF_TYPE_INDEX g_atype426_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes426 [] = {
g_atype426_0,
};

static const int32 cn_attr426 [] =
{
877,
};

extern const char *names427[];
static const uint32 types427 [] =
{
SK_REF,
};

static const uint16 attr_flags427 [] =
{0,};

static const EIF_TYPE_INDEX g_atype427_0 [] = {0xFF01,409,137,0xFFFF};

static const EIF_TYPE_INDEX *gtypes427 [] = {
g_atype427_0,
};

static const int32 cn_attr427 [] =
{
1632,
};

extern const char *names429[];
static const uint32 types429 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags429 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype429_0 [] = {0xFF01,409,137,0xFFFF};
static const EIF_TYPE_INDEX g_atype429_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype429_2 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes429 [] = {
g_atype429_0,
g_atype429_1,
g_atype429_2,
};

static const int32 cn_attr429 [] =
{
1632,
877,
1958,
};

extern const char *names430[];
static const uint32 types430 [] =
{
SK_BOOL,
};

static const uint16 attr_flags430 [] =
{0,};

static const EIF_TYPE_INDEX g_atype430_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes430 [] = {
g_atype430_0,
};

static const int32 cn_attr430 [] =
{
877,
};

extern const char *names431[];
static const uint32 types431 [] =
{
SK_BOOL,
};

static const uint16 attr_flags431 [] =
{0,};

static const EIF_TYPE_INDEX g_atype431_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes431 [] = {
g_atype431_0,
};

static const int32 cn_attr431 [] =
{
877,
};

extern const char *names432[];
static const uint32 types432 [] =
{
SK_BOOL,
};

static const uint16 attr_flags432 [] =
{0,};

static const EIF_TYPE_INDEX g_atype432_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes432 [] = {
g_atype432_0,
};

static const int32 cn_attr432 [] =
{
877,
};

extern const char *names433[];
static const uint32 types433 [] =
{
SK_BOOL,
};

static const uint16 attr_flags433 [] =
{0,};

static const EIF_TYPE_INDEX g_atype433_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes433 [] = {
g_atype433_0,
};

static const int32 cn_attr433 [] =
{
877,
};

extern const char *names434[];
static const uint32 types434 [] =
{
SK_BOOL,
};

static const uint16 attr_flags434 [] =
{0,};

static const EIF_TYPE_INDEX g_atype434_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes434 [] = {
g_atype434_0,
};

static const int32 cn_attr434 [] =
{
877,
};

extern const char *names435[];
static const uint32 types435 [] =
{
SK_BOOL,
};

static const uint16 attr_flags435 [] =
{0,};

static const EIF_TYPE_INDEX g_atype435_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes435 [] = {
g_atype435_0,
};

static const int32 cn_attr435 [] =
{
877,
};

extern const char *names436[];
static const uint32 types436 [] =
{
SK_BOOL,
};

static const uint16 attr_flags436 [] =
{0,};

static const EIF_TYPE_INDEX g_atype436_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes436 [] = {
g_atype436_0,
};

static const int32 cn_attr436 [] =
{
877,
};

extern const char *names437[];
static const uint32 types437 [] =
{
SK_BOOL,
};

static const uint16 attr_flags437 [] =
{0,};

static const EIF_TYPE_INDEX g_atype437_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes437 [] = {
g_atype437_0,
};

static const int32 cn_attr437 [] =
{
877,
};

extern const char *names438[];
static const uint32 types438 [] =
{
SK_BOOL,
};

static const uint16 attr_flags438 [] =
{0,};

static const EIF_TYPE_INDEX g_atype438_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes438 [] = {
g_atype438_0,
};

static const int32 cn_attr438 [] =
{
877,
};

extern const char *names439[];
static const uint32 types439 [] =
{
SK_BOOL,
};

static const uint16 attr_flags439 [] =
{0,};

static const EIF_TYPE_INDEX g_atype439_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes439 [] = {
g_atype439_0,
};

static const int32 cn_attr439 [] =
{
877,
};

extern const char *names440[];
static const uint32 types440 [] =
{
SK_POINTER,
};

static const uint16 attr_flags440 [] =
{0,};

static const EIF_TYPE_INDEX g_atype440_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes440 [] = {
g_atype440_0,
};

static const int32 cn_attr440 [] =
{
2849,
};

extern const char *names441[];
static const uint32 types441 [] =
{
SK_POINTER,
};

static const uint16 attr_flags441 [] =
{0,};

static const EIF_TYPE_INDEX g_atype441_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes441 [] = {
g_atype441_0,
};

static const int32 cn_attr441 [] =
{
2849,
};

extern const char *names442[];
static const uint32 types442 [] =
{
SK_REF,
};

static const uint16 attr_flags442 [] =
{0,};

static const EIF_TYPE_INDEX g_atype442_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes442 [] = {
g_atype442_0,
};

static const int32 cn_attr442 [] =
{
2139,
};

extern const char *names443[];
static const uint32 types443 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags443 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype443_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype443_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype443_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype443_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype443_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype443_5 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes443 [] = {
g_atype443_0,
g_atype443_1,
g_atype443_2,
g_atype443_3,
g_atype443_4,
g_atype443_5,
};

static const int32 cn_attr443 [] =
{
3400,
3428,
3430,
3398,
3399,
3429,
};

extern const char *names444[];
static const uint32 types444 [] =
{
SK_POINTER,
};

static const uint16 attr_flags444 [] =
{0,};

static const EIF_TYPE_INDEX g_atype444_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes444 [] = {
g_atype444_0,
};

static const int32 cn_attr444 [] =
{
2849,
};

extern const char *names445[];
static const uint32 types445 [] =
{
SK_POINTER,
};

static const uint16 attr_flags445 [] =
{0,};

static const EIF_TYPE_INDEX g_atype445_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes445 [] = {
g_atype445_0,
};

static const int32 cn_attr445 [] =
{
2849,
};

extern const char *names446[];
static const uint32 types446 [] =
{
SK_REF,
};

static const uint16 attr_flags446 [] =
{0,};

static const EIF_TYPE_INDEX g_atype446_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes446 [] = {
g_atype446_0,
};

static const int32 cn_attr446 [] =
{
2139,
};

extern const char *names447[];
static const uint32 types447 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags447 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype447_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype447_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype447_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype447_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype447_4 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes447 [] = {
g_atype447_0,
g_atype447_1,
g_atype447_2,
g_atype447_3,
g_atype447_4,
};

static const int32 cn_attr447 [] =
{
3400,
3421,
3398,
3399,
3422,
};

extern const char *names448[];
static const uint32 types448 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL64,
};

static const uint16 attr_flags448 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype448_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype448_1 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype448_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype448_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype448_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype448_5 [] = {146,0xFFFF};

static const EIF_TYPE_INDEX *gtypes448 [] = {
g_atype448_0,
g_atype448_1,
g_atype448_2,
g_atype448_3,
g_atype448_4,
g_atype448_5,
};

static const int32 cn_attr448 [] =
{
3400,
3415,
3398,
3399,
3414,
3413,
};

extern const char *names449[];
static const uint32 types449 [] =
{
SK_REF,
SK_UINT32,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags449 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype449_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype449_1 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype449_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype449_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype449_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype449_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes449 [] = {
g_atype449_0,
g_atype449_1,
g_atype449_2,
g_atype449_3,
g_atype449_4,
g_atype449_5,
};

static const int32 cn_attr449 [] =
{
3400,
3413,
3415,
3398,
3399,
3414,
};

extern const char *names450[];
static const uint32 types450 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_REAL64,
};

static const uint16 attr_flags450 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype450_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype450_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype450_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype450_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype450_4 [] = {146,0xFFFF};

static const EIF_TYPE_INDEX *gtypes450 [] = {
g_atype450_0,
g_atype450_1,
g_atype450_2,
g_atype450_3,
g_atype450_4,
};

static const int32 cn_attr450 [] =
{
3400,
3421,
3398,
3399,
3422,
};

extern const char *names451[];
static const uint32 types451 [] =
{
SK_REF,
SK_REF,
SK_INT8,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags451 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype451_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype451_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype451_2 [] = {128,0xFFFF};
static const EIF_TYPE_INDEX g_atype451_3 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype451_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype451_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes451 [] = {
g_atype451_0,
g_atype451_1,
g_atype451_2,
g_atype451_3,
g_atype451_4,
g_atype451_5,
};

static const int32 cn_attr451 [] =
{
3400,
3428,
3429,
3430,
3398,
3399,
};

extern const char *names452[];
static const uint32 types452 [] =
{
SK_POINTER,
};

static const uint16 attr_flags452 [] =
{0,};

static const EIF_TYPE_INDEX g_atype452_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes452 [] = {
g_atype452_0,
};

static const int32 cn_attr452 [] =
{
2849,
};

extern const char *names453[];
static const uint32 types453 [] =
{
SK_POINTER,
};

static const uint16 attr_flags453 [] =
{0,};

static const EIF_TYPE_INDEX g_atype453_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes453 [] = {
g_atype453_0,
};

static const int32 cn_attr453 [] =
{
2849,
};

extern const char *names454[];
static const uint32 types454 [] =
{
SK_REF,
};

static const uint16 attr_flags454 [] =
{0,};

static const EIF_TYPE_INDEX g_atype454_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes454 [] = {
g_atype454_0,
};

static const int32 cn_attr454 [] =
{
2139,
};

extern const char *names455[];
static const uint32 types455 [] =
{
SK_REF,
};

static const uint16 attr_flags455 [] =
{0,};

static const EIF_TYPE_INDEX g_atype455_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes455 [] = {
g_atype455_0,
};

static const int32 cn_attr455 [] =
{
2139,
};

extern const char *names456[];
static const uint32 types456 [] =
{
SK_REF,
};

static const uint16 attr_flags456 [] =
{0,};

static const EIF_TYPE_INDEX g_atype456_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes456 [] = {
g_atype456_0,
};

static const int32 cn_attr456 [] =
{
2139,
};

extern const char *names458[];
static const uint32 types458 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags458 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype458_0 [] = {0xFF01,458,140,0xFFFF};
static const EIF_TYPE_INDEX g_atype458_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype458_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype458_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype458_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype458_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype458_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes458 [] = {
g_atype458_0,
g_atype458_1,
g_atype458_2,
g_atype458_3,
g_atype458_4,
g_atype458_5,
g_atype458_6,
};

static const int32 cn_attr458 [] =
{
1789,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names462[];
static const uint32 types462 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags462 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype462_0 [] = {0xFF01,456,140,0xFFFF};
static const EIF_TYPE_INDEX g_atype462_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype462_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype462_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes462 [] = {
g_atype462_0,
g_atype462_1,
g_atype462_2,
g_atype462_3,
};

static const int32 cn_attr462 [] =
{
1632,
877,
1866,
1867,
};

extern const char *names463[];
static const uint32 types463 [] =
{
SK_BOOL,
};

static const uint16 attr_flags463 [] =
{0,};

static const EIF_TYPE_INDEX g_atype463_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes463 [] = {
g_atype463_0,
};

static const int32 cn_attr463 [] =
{
877,
};

extern const char *names464[];
static const uint32 types464 [] =
{
SK_BOOL,
};

static const uint16 attr_flags464 [] =
{0,};

static const EIF_TYPE_INDEX g_atype464_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes464 [] = {
g_atype464_0,
};

static const int32 cn_attr464 [] =
{
877,
};

extern const char *names465[];
static const uint32 types465 [] =
{
SK_BOOL,
};

static const uint16 attr_flags465 [] =
{0,};

static const EIF_TYPE_INDEX g_atype465_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes465 [] = {
g_atype465_0,
};

static const int32 cn_attr465 [] =
{
877,
};

extern const char *names466[];
static const uint32 types466 [] =
{
SK_BOOL,
};

static const uint16 attr_flags466 [] =
{0,};

static const EIF_TYPE_INDEX g_atype466_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes466 [] = {
g_atype466_0,
};

static const int32 cn_attr466 [] =
{
877,
};

extern const char *names467[];
static const uint32 types467 [] =
{
SK_BOOL,
};

static const uint16 attr_flags467 [] =
{0,};

static const EIF_TYPE_INDEX g_atype467_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes467 [] = {
g_atype467_0,
};

static const int32 cn_attr467 [] =
{
877,
};

extern const char *names468[];
static const uint32 types468 [] =
{
SK_BOOL,
};

static const uint16 attr_flags468 [] =
{0,};

static const EIF_TYPE_INDEX g_atype468_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes468 [] = {
g_atype468_0,
};

static const int32 cn_attr468 [] =
{
877,
};

extern const char *names469[];
static const uint32 types469 [] =
{
SK_BOOL,
};

static const uint16 attr_flags469 [] =
{0,};

static const EIF_TYPE_INDEX g_atype469_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes469 [] = {
g_atype469_0,
};

static const int32 cn_attr469 [] =
{
877,
};

extern const char *names470[];
static const uint32 types470 [] =
{
SK_BOOL,
};

static const uint16 attr_flags470 [] =
{0,};

static const EIF_TYPE_INDEX g_atype470_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes470 [] = {
g_atype470_0,
};

static const int32 cn_attr470 [] =
{
877,
};

extern const char *names471[];
static const uint32 types471 [] =
{
SK_BOOL,
};

static const uint16 attr_flags471 [] =
{0,};

static const EIF_TYPE_INDEX g_atype471_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes471 [] = {
g_atype471_0,
};

static const int32 cn_attr471 [] =
{
877,
};

extern const char *names472[];
static const uint32 types472 [] =
{
SK_BOOL,
};

static const uint16 attr_flags472 [] =
{0,};

static const EIF_TYPE_INDEX g_atype472_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes472 [] = {
g_atype472_0,
};

static const int32 cn_attr472 [] =
{
877,
};

extern const char *names473[];
static const uint32 types473 [] =
{
SK_BOOL,
};

static const uint16 attr_flags473 [] =
{0,};

static const EIF_TYPE_INDEX g_atype473_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes473 [] = {
g_atype473_0,
};

static const int32 cn_attr473 [] =
{
877,
};

extern const char *names474[];
static const uint32 types474 [] =
{
SK_REF,
};

static const uint16 attr_flags474 [] =
{0,};

static const EIF_TYPE_INDEX g_atype474_0 [] = {0xFF01,456,140,0xFFFF};

static const EIF_TYPE_INDEX *gtypes474 [] = {
g_atype474_0,
};

static const int32 cn_attr474 [] =
{
1632,
};

extern const char *names476[];
static const uint32 types476 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags476 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype476_0 [] = {0xFF01,456,140,0xFFFF};
static const EIF_TYPE_INDEX g_atype476_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype476_2 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes476 [] = {
g_atype476_0,
g_atype476_1,
g_atype476_2,
};

static const int32 cn_attr476 [] =
{
1632,
877,
1958,
};

extern const char *names477[];
static const uint32 types477 [] =
{
SK_BOOL,
};

static const uint16 attr_flags477 [] =
{0,};

static const EIF_TYPE_INDEX g_atype477_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes477 [] = {
g_atype477_0,
};

static const int32 cn_attr477 [] =
{
877,
};

extern const char *names478[];
static const uint32 types478 [] =
{
SK_BOOL,
};

static const uint16 attr_flags478 [] =
{0,};

static const EIF_TYPE_INDEX g_atype478_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes478 [] = {
g_atype478_0,
};

static const int32 cn_attr478 [] =
{
877,
};

extern const char *names479[];
static const uint32 types479 [] =
{
SK_BOOL,
};

static const uint16 attr_flags479 [] =
{0,};

static const EIF_TYPE_INDEX g_atype479_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes479 [] = {
g_atype479_0,
};

static const int32 cn_attr479 [] =
{
877,
};

extern const char *names480[];
static const uint32 types480 [] =
{
SK_BOOL,
};

static const uint16 attr_flags480 [] =
{0,};

static const EIF_TYPE_INDEX g_atype480_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes480 [] = {
g_atype480_0,
};

static const int32 cn_attr480 [] =
{
877,
};

extern const char *names481[];
static const uint32 types481 [] =
{
SK_BOOL,
};

static const uint16 attr_flags481 [] =
{0,};

static const EIF_TYPE_INDEX g_atype481_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes481 [] = {
g_atype481_0,
};

static const int32 cn_attr481 [] =
{
877,
};

extern const char *names482[];
static const uint32 types482 [] =
{
SK_BOOL,
};

static const uint16 attr_flags482 [] =
{0,};

static const EIF_TYPE_INDEX g_atype482_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes482 [] = {
g_atype482_0,
};

static const int32 cn_attr482 [] =
{
877,
};

extern const char *names483[];
static const uint32 types483 [] =
{
SK_BOOL,
};

static const uint16 attr_flags483 [] =
{0,};

static const EIF_TYPE_INDEX g_atype483_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes483 [] = {
g_atype483_0,
};

static const int32 cn_attr483 [] =
{
877,
};

extern const char *names484[];
static const uint32 types484 [] =
{
SK_BOOL,
};

static const uint16 attr_flags484 [] =
{0,};

static const EIF_TYPE_INDEX g_atype484_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes484 [] = {
g_atype484_0,
};

static const int32 cn_attr484 [] =
{
877,
};

extern const char *names485[];
static const uint32 types485 [] =
{
SK_BOOL,
};

static const uint16 attr_flags485 [] =
{0,};

static const EIF_TYPE_INDEX g_atype485_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes485 [] = {
g_atype485_0,
};

static const int32 cn_attr485 [] =
{
877,
};

extern const char *names486[];
static const uint32 types486 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
};

static const uint16 attr_flags486 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype486_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype486_1 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype486_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype486_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype486_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype486_5 [] = {143,0xFFFF};

static const EIF_TYPE_INDEX *gtypes486 [] = {
g_atype486_0,
g_atype486_1,
g_atype486_2,
g_atype486_3,
g_atype486_4,
g_atype486_5,
};

static const int32 cn_attr486 [] =
{
3400,
3415,
3398,
3399,
3414,
3413,
};

extern const char *names487[];
static const uint32 types487 [] =
{
SK_REF,
SK_INT8,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags487 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype487_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype487_1 [] = {128,0xFFFF};
static const EIF_TYPE_INDEX g_atype487_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype487_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype487_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype487_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes487 [] = {
g_atype487_0,
g_atype487_1,
g_atype487_2,
g_atype487_3,
g_atype487_4,
g_atype487_5,
};

static const int32 cn_attr487 [] =
{
3400,
3413,
3415,
3398,
3399,
3414,
};

extern const char *names488[];
static const uint32 types488 [] =
{
SK_POINTER,
};

static const uint16 attr_flags488 [] =
{0,};

static const EIF_TYPE_INDEX g_atype488_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes488 [] = {
g_atype488_0,
};

static const int32 cn_attr488 [] =
{
2849,
};

extern const char *names489[];
static const uint32 types489 [] =
{
SK_POINTER,
};

static const uint16 attr_flags489 [] =
{0,};

static const EIF_TYPE_INDEX g_atype489_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes489 [] = {
g_atype489_0,
};

static const int32 cn_attr489 [] =
{
2849,
};

extern const char *names490[];
static const uint32 types490 [] =
{
SK_REF,
};

static const uint16 attr_flags490 [] =
{0,};

static const EIF_TYPE_INDEX g_atype490_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes490 [] = {
g_atype490_0,
};

static const int32 cn_attr490 [] =
{
2139,
};

extern const char *names491[];
static const uint32 types491 [] =
{
SK_REF,
SK_REF,
SK_CHAR32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags491 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype491_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype491_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype491_2 [] = {149,0xFFFF};
static const EIF_TYPE_INDEX g_atype491_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype491_4 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes491 [] = {
g_atype491_0,
g_atype491_1,
g_atype491_2,
g_atype491_3,
g_atype491_4,
};

static const int32 cn_attr491 [] =
{
3400,
3421,
3422,
3398,
3399,
};

extern const char *names492[];
static const uint32 types492 [] =
{
SK_POINTER,
};

static const uint16 attr_flags492 [] =
{0,};

static const EIF_TYPE_INDEX g_atype492_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes492 [] = {
g_atype492_0,
};

static const int32 cn_attr492 [] =
{
2849,
};

extern const char *names493[];
static const uint32 types493 [] =
{
SK_POINTER,
};

static const uint16 attr_flags493 [] =
{0,};

static const EIF_TYPE_INDEX g_atype493_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes493 [] = {
g_atype493_0,
};

static const int32 cn_attr493 [] =
{
2849,
};

extern const char *names494[];
static const uint32 types494 [] =
{
SK_REF,
};

static const uint16 attr_flags494 [] =
{0,};

static const EIF_TYPE_INDEX g_atype494_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes494 [] = {
g_atype494_0,
};

static const int32 cn_attr494 [] =
{
2139,
};

extern const char *names495[];
static const uint32 types495 [] =
{
SK_REF,
SK_CHAR32,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags495 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype495_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype495_1 [] = {149,0xFFFF};
static const EIF_TYPE_INDEX g_atype495_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype495_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype495_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype495_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes495 [] = {
g_atype495_0,
g_atype495_1,
g_atype495_2,
g_atype495_3,
g_atype495_4,
g_atype495_5,
};

static const int32 cn_attr495 [] =
{
3400,
3413,
3415,
3398,
3399,
3414,
};

extern const char *names496[];
static const uint32 types496 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags496 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype496_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype496_1 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype496_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype496_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype496_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype496_5 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes496 [] = {
g_atype496_0,
g_atype496_1,
g_atype496_2,
g_atype496_3,
g_atype496_4,
g_atype496_5,
};

static const int32 cn_attr496 [] =
{
3400,
3415,
3398,
3399,
3414,
3413,
};

extern const char *names497[];
static const uint32 types497 [] =
{
SK_REF,
};

static const uint16 attr_flags497 [] =
{0,};

static const EIF_TYPE_INDEX g_atype497_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes497 [] = {
g_atype497_0,
};

static const int32 cn_attr497 [] =
{
2139,
};

extern const char *names498[];
static const uint32 types498 [] =
{
SK_REF,
};

static const uint16 attr_flags498 [] =
{0,};

static const EIF_TYPE_INDEX g_atype498_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes498 [] = {
g_atype498_0,
};

static const int32 cn_attr498 [] =
{
2139,
};

extern const char *names499[];
static const uint32 types499 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_UINT64,
};

static const uint16 attr_flags499 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype499_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype499_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype499_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype499_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype499_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype499_5 [] = {131,0xFFFF};

static const EIF_TYPE_INDEX *gtypes499 [] = {
g_atype499_0,
g_atype499_1,
g_atype499_2,
g_atype499_3,
g_atype499_4,
g_atype499_5,
};

static const int32 cn_attr499 [] =
{
3400,
3428,
3430,
3398,
3399,
3429,
};

extern const char *names500[];
static const uint32 types500 [] =
{
SK_POINTER,
};

static const uint16 attr_flags500 [] =
{0,};

static const EIF_TYPE_INDEX g_atype500_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes500 [] = {
g_atype500_0,
};

static const int32 cn_attr500 [] =
{
2849,
};

extern const char *names501[];
static const uint32 types501 [] =
{
SK_POINTER,
};

static const uint16 attr_flags501 [] =
{0,};

static const EIF_TYPE_INDEX g_atype501_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes501 [] = {
g_atype501_0,
};

static const int32 cn_attr501 [] =
{
2849,
};

extern const char *names502[];
static const uint32 types502 [] =
{
SK_REF,
};

static const uint16 attr_flags502 [] =
{0,};

static const EIF_TYPE_INDEX g_atype502_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes502 [] = {
g_atype502_0,
};

static const int32 cn_attr502 [] =
{
2139,
};

extern const char *names503[];
static const uint32 types503 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_UINT64,
};

static const uint16 attr_flags503 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype503_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype503_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype503_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype503_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype503_4 [] = {131,0xFFFF};

static const EIF_TYPE_INDEX *gtypes503 [] = {
g_atype503_0,
g_atype503_1,
g_atype503_2,
g_atype503_3,
g_atype503_4,
};

static const int32 cn_attr503 [] =
{
3400,
3421,
3398,
3399,
3422,
};

extern const char *names504[];
static const uint32 types504 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_UINT64,
};

static const uint16 attr_flags504 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype504_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype504_1 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype504_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype504_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype504_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype504_5 [] = {131,0xFFFF};

static const EIF_TYPE_INDEX *gtypes504 [] = {
g_atype504_0,
g_atype504_1,
g_atype504_2,
g_atype504_3,
g_atype504_4,
g_atype504_5,
};

static const int32 cn_attr504 [] =
{
3400,
3415,
3398,
3399,
3414,
3413,
};

extern const char *names505[];
static const uint32 types505 [] =
{
SK_REF,
SK_REF,
SK_INT16,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags505 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype505_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype505_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype505_2 [] = {125,0xFFFF};
static const EIF_TYPE_INDEX g_atype505_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype505_4 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes505 [] = {
g_atype505_0,
g_atype505_1,
g_atype505_2,
g_atype505_3,
g_atype505_4,
};

static const int32 cn_attr505 [] =
{
3400,
3421,
3422,
3398,
3399,
};

extern const char *names506[];
static const uint32 types506 [] =
{
SK_POINTER,
};

static const uint16 attr_flags506 [] =
{0,};

static const EIF_TYPE_INDEX g_atype506_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes506 [] = {
g_atype506_0,
};

static const int32 cn_attr506 [] =
{
2849,
};

extern const char *names507[];
static const uint32 types507 [] =
{
SK_POINTER,
};

static const uint16 attr_flags507 [] =
{0,};

static const EIF_TYPE_INDEX g_atype507_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes507 [] = {
g_atype507_0,
};

static const int32 cn_attr507 [] =
{
2849,
};

extern const char *names508[];
static const uint32 types508 [] =
{
SK_REF,
};

static const uint16 attr_flags508 [] =
{0,};

static const EIF_TYPE_INDEX g_atype508_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes508 [] = {
g_atype508_0,
};

static const int32 cn_attr508 [] =
{
2139,
};

extern const char *names509[];
static const uint32 types509 [] =
{
SK_INT32,
};

static const uint16 attr_flags509 [] =
{0,};

static const EIF_TYPE_INDEX g_atype509_0 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes509 [] = {
g_atype509_0,
};

static const int32 cn_attr509 [] =
{
533,
};

extern const char *names510[];
static const uint32 types510 [] =
{
SK_BOOL,
};

static const uint16 attr_flags510 [] =
{0,};

static const EIF_TYPE_INDEX g_atype510_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes510 [] = {
g_atype510_0,
};

static const int32 cn_attr510 [] =
{
877,
};

extern const char *names511[];
static const uint32 types511 [] =
{
SK_BOOL,
};

static const uint16 attr_flags511 [] =
{0,};

static const EIF_TYPE_INDEX g_atype511_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes511 [] = {
g_atype511_0,
};

static const int32 cn_attr511 [] =
{
877,
};

extern const char *names512[];
static const uint32 types512 [] =
{
SK_REF,
};

static const uint16 attr_flags512 [] =
{0,};

static const EIF_TYPE_INDEX g_atype512_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes512 [] = {
g_atype512_0,
};

static const int32 cn_attr512 [] =
{
2139,
};

extern const char *names513[];
static const uint32 types513 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags513 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype513_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype513_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype513_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype513_3 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype513_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype513_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes513 [] = {
g_atype513_0,
g_atype513_1,
g_atype513_2,
g_atype513_3,
g_atype513_4,
g_atype513_5,
};

static const int32 cn_attr513 [] =
{
3400,
3428,
3429,
3430,
3398,
3399,
};

extern const char *names514[];
static const uint32 types514 [] =
{
SK_REF,
};

static const uint16 attr_flags514 [] =
{0,};

static const EIF_TYPE_INDEX g_atype514_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes514 [] = {
g_atype514_0,
};

static const int32 cn_attr514 [] =
{
2139,
};

extern const char *names515[];
static const uint32 types515 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags515 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype515_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_1 [] = {0xFF01,178,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_2 [] = {0xFF01,248,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_3 [] = {0xFF01,248,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_4 [] = {0xFF01,597,158,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_5 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_6 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_7 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_8 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_9 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_10 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_11 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_12 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_13 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_14 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_15 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_16 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes515 [] = {
g_atype515_0,
g_atype515_1,
g_atype515_2,
g_atype515_3,
g_atype515_4,
g_atype515_5,
g_atype515_6,
g_atype515_7,
g_atype515_8,
g_atype515_9,
g_atype515_10,
g_atype515_11,
g_atype515_12,
g_atype515_13,
g_atype515_14,
g_atype515_15,
g_atype515_16,
};

static const int32 cn_attr515 [] =
{
1962,
1997,
1998,
1999,
2000,
2012,
877,
1996,
2002,
1970,
2001,
2003,
2006,
2007,
2011,
2013,
2047,
};

extern const char *names518[];
static const uint32 types518 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags518 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype518_0 [] = {0xFF01,514,0xFFF8,1,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype518_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype518_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype518_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype518_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype518_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype518_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes518 [] = {
g_atype518_0,
g_atype518_1,
g_atype518_2,
g_atype518_3,
g_atype518_4,
g_atype518_5,
g_atype518_6,
};

static const int32 cn_attr518 [] =
{
1789,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names519[];
static const uint32 types519 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags519 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype519_0 [] = {519,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype519_1 [] = {519,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype519_2 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype519_3 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype519_4 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype519_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes519 [] = {
g_atype519_0,
g_atype519_1,
g_atype519_2,
g_atype519_3,
g_atype519_4,
g_atype519_5,
};

static const int32 cn_attr519 [] =
{
1921,
1925,
877,
1929,
1930,
1931,
};

extern const char *names520[];
static const uint32 types520 [] =
{
SK_REF,
SK_INT32,
};

static const uint16 attr_flags520 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype520_0 [] = {519,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype520_1 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes520 [] = {
g_atype520_0,
g_atype520_1,
};

static const int32 cn_attr520 [] =
{
537,
533,
};

extern const char *names521[];
static const uint32 types521 [] =
{
SK_REF,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags521 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype521_0 [] = {519,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype521_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype521_2 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes521 [] = {
g_atype521_0,
g_atype521_1,
g_atype521_2,
};

static const int32 cn_attr521 [] =
{
1184,
1185,
1186,
};

extern const char *names522[];
static const uint32 types522 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags522 [] =
{0,0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype522_0 [] = {0xFF01,518,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype522_1 [] = {519,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype522_2 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype522_3 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype522_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype522_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype522_6 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype522_7 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes522 [] = {
g_atype522_0,
g_atype522_1,
g_atype522_2,
g_atype522_3,
g_atype522_4,
g_atype522_5,
g_atype522_6,
g_atype522_7,
};

static const int32 cn_attr522 [] =
{
1789,
1791,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names523[];
static const uint32 types523 [] =
{
SK_REF,
};

static const uint16 attr_flags523 [] =
{0,};

static const EIF_TYPE_INDEX g_atype523_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes523 [] = {
g_atype523_0,
};

static const int32 cn_attr523 [] =
{
2139,
};

extern const char *names524[];
static const uint32 types524 [] =
{
SK_REF,
};

static const uint16 attr_flags524 [] =
{0,};

static const EIF_TYPE_INDEX g_atype524_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes524 [] = {
g_atype524_0,
};

static const int32 cn_attr524 [] =
{
2139,
};

extern const char *names525[];
static const uint32 types525 [] =
{
SK_REF,
SK_REF,
SK_UINT16,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags525 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype525_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype525_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype525_2 [] = {137,0xFFFF};
static const EIF_TYPE_INDEX g_atype525_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype525_4 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes525 [] = {
g_atype525_0,
g_atype525_1,
g_atype525_2,
g_atype525_3,
g_atype525_4,
};

static const int32 cn_attr525 [] =
{
3400,
3421,
3422,
3398,
3399,
};

extern const char *names526[];
static const uint32 types526 [] =
{
SK_REF,
SK_UINT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags526 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype526_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_1 [] = {137,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes526 [] = {
g_atype526_0,
g_atype526_1,
g_atype526_2,
g_atype526_3,
g_atype526_4,
g_atype526_5,
};

static const int32 cn_attr526 [] =
{
3400,
3413,
3415,
3398,
3399,
3414,
};

extern const char *names527[];
static const uint32 types527 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags527 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype527_0 [] = {0xFF02,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_1 [] = {0xFFF9,0,120,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_2 [] = {249,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_3 [] = {249,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_4 [] = {0xFF02,0xFFF8,3,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_6 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_7 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_8 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_9 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_10 [] = {161,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_11 [] = {161,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_12 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes527 [] = {
g_atype527_0,
g_atype527_1,
g_atype527_2,
g_atype527_3,
g_atype527_4,
g_atype527_5,
g_atype527_6,
g_atype527_7,
g_atype527_8,
g_atype527_9,
g_atype527_10,
g_atype527_11,
g_atype527_12,
};

static const int32 cn_attr527 [] =
{
2874,
2890,
2894,
2902,
2912,
2882,
2897,
2883,
2896,
2898,
2892,
2893,
2895,
};

extern const char *names528[];
static const uint32 types528 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags528 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype528_0 [] = {0xFF01,531,155,0xFFFF};
static const EIF_TYPE_INDEX g_atype528_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype528_2 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes528 [] = {
g_atype528_0,
g_atype528_1,
g_atype528_2,
};

static const int32 cn_attr528 [] =
{
1632,
877,
1958,
};

extern const char *names529[];
static const uint32 types529 [] =
{
SK_BOOL,
};

static const uint16 attr_flags529 [] =
{0,};

static const EIF_TYPE_INDEX g_atype529_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes529 [] = {
g_atype529_0,
};

static const int32 cn_attr529 [] =
{
877,
};

extern const char *names530[];
static const uint32 types530 [] =
{
SK_BOOL,
};

static const uint16 attr_flags530 [] =
{0,};

static const EIF_TYPE_INDEX g_atype530_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes530 [] = {
g_atype530_0,
};

static const int32 cn_attr530 [] =
{
877,
};

extern const char *names531[];
static const uint32 types531 [] =
{
SK_REF,
};

static const uint16 attr_flags531 [] =
{0,};

static const EIF_TYPE_INDEX g_atype531_0 [] = {0xFF01,531,155,0xFFFF};

static const EIF_TYPE_INDEX *gtypes531 [] = {
g_atype531_0,
};

static const int32 cn_attr531 [] =
{
1632,
};

extern const char *names533[];
static const uint32 types533 [] =
{
SK_REF,
};

static const uint16 attr_flags533 [] =
{0,};

static const EIF_TYPE_INDEX g_atype533_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes533 [] = {
g_atype533_0,
};

static const int32 cn_attr533 [] =
{
2139,
};

extern const char *names534[];
static const uint32 types534 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags534 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype534_0 [] = {0xFF01,531,155,0xFFFF};
static const EIF_TYPE_INDEX g_atype534_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype534_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype534_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes534 [] = {
g_atype534_0,
g_atype534_1,
g_atype534_2,
g_atype534_3,
};

static const int32 cn_attr534 [] =
{
1632,
877,
1866,
1867,
};

extern const char *names536[];
static const uint32 types536 [] =
{
SK_BOOL,
};

static const uint16 attr_flags536 [] =
{0,};

static const EIF_TYPE_INDEX g_atype536_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes536 [] = {
g_atype536_0,
};

static const int32 cn_attr536 [] =
{
877,
};

extern const char *names537[];
static const uint32 types537 [] =
{
SK_BOOL,
};

static const uint16 attr_flags537 [] =
{0,};

static const EIF_TYPE_INDEX g_atype537_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes537 [] = {
g_atype537_0,
};

static const int32 cn_attr537 [] =
{
877,
};

extern const char *names538[];
static const uint32 types538 [] =
{
SK_BOOL,
};

static const uint16 attr_flags538 [] =
{0,};

static const EIF_TYPE_INDEX g_atype538_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes538 [] = {
g_atype538_0,
};

static const int32 cn_attr538 [] =
{
877,
};

extern const char *names539[];
static const uint32 types539 [] =
{
SK_BOOL,
};

static const uint16 attr_flags539 [] =
{0,};

static const EIF_TYPE_INDEX g_atype539_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes539 [] = {
g_atype539_0,
};

static const int32 cn_attr539 [] =
{
877,
};

extern const char *names540[];
static const uint32 types540 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags540 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype540_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype540_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype540_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype540_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype540_4 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes540 [] = {
g_atype540_0,
g_atype540_1,
g_atype540_2,
g_atype540_3,
g_atype540_4,
};

static const int32 cn_attr540 [] =
{
3400,
3421,
3398,
3399,
3422,
};

extern const char *names541[];
static const uint32 types541 [] =
{
SK_POINTER,
};

static const uint16 attr_flags541 [] =
{0,};

static const EIF_TYPE_INDEX g_atype541_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes541 [] = {
g_atype541_0,
};

static const int32 cn_attr541 [] =
{
2849,
};

extern const char *names542[];
static const uint32 types542 [] =
{
SK_POINTER,
};

static const uint16 attr_flags542 [] =
{0,};

static const EIF_TYPE_INDEX g_atype542_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes542 [] = {
g_atype542_0,
};

static const int32 cn_attr542 [] =
{
2849,
};

extern const char *names543[];
static const uint32 types543 [] =
{
SK_REF,
};

static const uint16 attr_flags543 [] =
{0,};

static const EIF_TYPE_INDEX g_atype543_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes543 [] = {
g_atype543_0,
};

static const int32 cn_attr543 [] =
{
2139,
};

extern const char *names544[];
static const uint32 types544 [] =
{
SK_REF,
};

static const uint16 attr_flags544 [] =
{0,};

static const EIF_TYPE_INDEX g_atype544_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes544 [] = {
g_atype544_0,
};

static const int32 cn_attr544 [] =
{
2139,
};

extern const char *names545[];
static const uint32 types545 [] =
{
SK_REF,
};

static const uint16 attr_flags545 [] =
{0,};

static const EIF_TYPE_INDEX g_atype545_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes545 [] = {
g_atype545_0,
};

static const int32 cn_attr545 [] =
{
2139,
};

extern const char *names546[];
static const uint32 types546 [] =
{
SK_REF,
SK_REF,
SK_UINT8,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags546 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype546_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype546_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype546_2 [] = {140,0xFFFF};
static const EIF_TYPE_INDEX g_atype546_3 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype546_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype546_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes546 [] = {
g_atype546_0,
g_atype546_1,
g_atype546_2,
g_atype546_3,
g_atype546_4,
g_atype546_5,
};

static const int32 cn_attr546 [] =
{
3400,
3428,
3429,
3430,
3398,
3399,
};

extern const char *names547[];
static const uint32 types547 [] =
{
SK_REF,
SK_REF,
SK_UINT8,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags547 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype547_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype547_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype547_2 [] = {140,0xFFFF};
static const EIF_TYPE_INDEX g_atype547_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype547_4 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes547 [] = {
g_atype547_0,
g_atype547_1,
g_atype547_2,
g_atype547_3,
g_atype547_4,
};

static const int32 cn_attr547 [] =
{
3400,
3421,
3422,
3398,
3399,
};

extern const char *names548[];
static const uint32 types548 [] =
{
SK_REF,
SK_UINT8,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags548 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype548_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype548_1 [] = {140,0xFFFF};
static const EIF_TYPE_INDEX g_atype548_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype548_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype548_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype548_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes548 [] = {
g_atype548_0,
g_atype548_1,
g_atype548_2,
g_atype548_3,
g_atype548_4,
g_atype548_5,
};

static const int32 cn_attr548 [] =
{
3400,
3413,
3415,
3398,
3399,
3414,
};

extern const char *names549[];
static const uint32 types549 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags549 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype549_0 [] = {0xFF01,565,161,0xFFFF};
static const EIF_TYPE_INDEX g_atype549_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype549_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype549_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes549 [] = {
g_atype549_0,
g_atype549_1,
g_atype549_2,
g_atype549_3,
};

static const int32 cn_attr549 [] =
{
1632,
877,
1866,
1867,
};

extern const char *names550[];
static const uint32 types550 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags550 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype550_0 [] = {0xFF01,550,161,0xFFFF};
static const EIF_TYPE_INDEX g_atype550_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype550_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype550_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype550_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype550_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype550_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes550 [] = {
g_atype550_0,
g_atype550_1,
g_atype550_2,
g_atype550_3,
g_atype550_4,
g_atype550_5,
g_atype550_6,
};

static const int32 cn_attr550 [] =
{
1789,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names554[];
static const uint32 types554 [] =
{
SK_BOOL,
};

static const uint16 attr_flags554 [] =
{0,};

static const EIF_TYPE_INDEX g_atype554_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes554 [] = {
g_atype554_0,
};

static const int32 cn_attr554 [] =
{
877,
};

extern const char *names555[];
static const uint32 types555 [] =
{
SK_BOOL,
};

static const uint16 attr_flags555 [] =
{0,};

static const EIF_TYPE_INDEX g_atype555_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes555 [] = {
g_atype555_0,
};

static const int32 cn_attr555 [] =
{
877,
};

extern const char *names556[];
static const uint32 types556 [] =
{
SK_BOOL,
};

static const uint16 attr_flags556 [] =
{0,};

static const EIF_TYPE_INDEX g_atype556_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes556 [] = {
g_atype556_0,
};

static const int32 cn_attr556 [] =
{
877,
};

extern const char *names557[];
static const uint32 types557 [] =
{
SK_BOOL,
};

static const uint16 attr_flags557 [] =
{0,};

static const EIF_TYPE_INDEX g_atype557_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes557 [] = {
g_atype557_0,
};

static const int32 cn_attr557 [] =
{
877,
};

extern const char *names558[];
static const uint32 types558 [] =
{
SK_BOOL,
};

static const uint16 attr_flags558 [] =
{0,};

static const EIF_TYPE_INDEX g_atype558_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes558 [] = {
g_atype558_0,
};

static const int32 cn_attr558 [] =
{
877,
};

extern const char *names559[];
static const uint32 types559 [] =
{
SK_BOOL,
};

static const uint16 attr_flags559 [] =
{0,};

static const EIF_TYPE_INDEX g_atype559_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes559 [] = {
g_atype559_0,
};

static const int32 cn_attr559 [] =
{
877,
};

extern const char *names560[];
static const uint32 types560 [] =
{
SK_BOOL,
};

static const uint16 attr_flags560 [] =
{0,};

static const EIF_TYPE_INDEX g_atype560_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes560 [] = {
g_atype560_0,
};

static const int32 cn_attr560 [] =
{
877,
};

extern const char *names561[];
static const uint32 types561 [] =
{
SK_BOOL,
};

static const uint16 attr_flags561 [] =
{0,};

static const EIF_TYPE_INDEX g_atype561_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes561 [] = {
g_atype561_0,
};

static const int32 cn_attr561 [] =
{
877,
};

extern const char *names562[];
static const uint32 types562 [] =
{
SK_BOOL,
};

static const uint16 attr_flags562 [] =
{0,};

static const EIF_TYPE_INDEX g_atype562_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes562 [] = {
g_atype562_0,
};

static const int32 cn_attr562 [] =
{
877,
};

extern const char *names563[];
static const uint32 types563 [] =
{
SK_BOOL,
};

static const uint16 attr_flags563 [] =
{0,};

static const EIF_TYPE_INDEX g_atype563_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes563 [] = {
g_atype563_0,
};

static const int32 cn_attr563 [] =
{
877,
};

extern const char *names564[];
static const uint32 types564 [] =
{
SK_BOOL,
};

static const uint16 attr_flags564 [] =
{0,};

static const EIF_TYPE_INDEX g_atype564_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes564 [] = {
g_atype564_0,
};

static const int32 cn_attr564 [] =
{
877,
};

extern const char *names565[];
static const uint32 types565 [] =
{
SK_REF,
};

static const uint16 attr_flags565 [] =
{0,};

static const EIF_TYPE_INDEX g_atype565_0 [] = {0xFF01,565,161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes565 [] = {
g_atype565_0,
};

static const int32 cn_attr565 [] =
{
1632,
};

extern const char *names568[];
static const uint32 types568 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags568 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype568_0 [] = {0xFF01,565,161,0xFFFF};
static const EIF_TYPE_INDEX g_atype568_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype568_2 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes568 [] = {
g_atype568_0,
g_atype568_1,
g_atype568_2,
};

static const int32 cn_attr568 [] =
{
1632,
877,
1958,
};

extern const char *names569[];
static const uint32 types569 [] =
{
SK_BOOL,
};

static const uint16 attr_flags569 [] =
{0,};

static const EIF_TYPE_INDEX g_atype569_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes569 [] = {
g_atype569_0,
};

static const int32 cn_attr569 [] =
{
877,
};

extern const char *names570[];
static const uint32 types570 [] =
{
SK_BOOL,
};

static const uint16 attr_flags570 [] =
{0,};

static const EIF_TYPE_INDEX g_atype570_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes570 [] = {
g_atype570_0,
};

static const int32 cn_attr570 [] =
{
877,
};

extern const char *names571[];
static const uint32 types571 [] =
{
SK_BOOL,
};

static const uint16 attr_flags571 [] =
{0,};

static const EIF_TYPE_INDEX g_atype571_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes571 [] = {
g_atype571_0,
};

static const int32 cn_attr571 [] =
{
877,
};

extern const char *names572[];
static const uint32 types572 [] =
{
SK_BOOL,
};

static const uint16 attr_flags572 [] =
{0,};

static const EIF_TYPE_INDEX g_atype572_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes572 [] = {
g_atype572_0,
};

static const int32 cn_attr572 [] =
{
877,
};

extern const char *names573[];
static const uint32 types573 [] =
{
SK_BOOL,
};

static const uint16 attr_flags573 [] =
{0,};

static const EIF_TYPE_INDEX g_atype573_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes573 [] = {
g_atype573_0,
};

static const int32 cn_attr573 [] =
{
877,
};

extern const char *names574[];
static const uint32 types574 [] =
{
SK_BOOL,
};

static const uint16 attr_flags574 [] =
{0,};

static const EIF_TYPE_INDEX g_atype574_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes574 [] = {
g_atype574_0,
};

static const int32 cn_attr574 [] =
{
877,
};

extern const char *names575[];
static const uint32 types575 [] =
{
SK_BOOL,
};

static const uint16 attr_flags575 [] =
{0,};

static const EIF_TYPE_INDEX g_atype575_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes575 [] = {
g_atype575_0,
};

static const int32 cn_attr575 [] =
{
877,
};

extern const char *names576[];
static const uint32 types576 [] =
{
SK_BOOL,
};

static const uint16 attr_flags576 [] =
{0,};

static const EIF_TYPE_INDEX g_atype576_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes576 [] = {
g_atype576_0,
};

static const int32 cn_attr576 [] =
{
877,
};

extern const char *names577[];
static const uint32 types577 [] =
{
SK_BOOL,
};

static const uint16 attr_flags577 [] =
{0,};

static const EIF_TYPE_INDEX g_atype577_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes577 [] = {
g_atype577_0,
};

static const int32 cn_attr577 [] =
{
877,
};

extern const char *names578[];
static const uint32 types578 [] =
{
SK_BOOL,
};

static const uint16 attr_flags578 [] =
{0,};

static const EIF_TYPE_INDEX g_atype578_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes578 [] = {
g_atype578_0,
};

static const int32 cn_attr578 [] =
{
877,
};

extern const char *names579[];
static const uint32 types579 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags579 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype579_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype579_1 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype579_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype579_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype579_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype579_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes579 [] = {
g_atype579_0,
g_atype579_1,
g_atype579_2,
g_atype579_3,
g_atype579_4,
g_atype579_5,
};

static const int32 cn_attr579 [] =
{
3400,
3413,
3415,
3398,
3399,
3414,
};

extern const char *names580[];
static const uint32 types580 [] =
{
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags580 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype580_0 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype580_1 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes580 [] = {
g_atype580_0,
g_atype580_1,
};

static const int32 cn_attr580 [] =
{
877,
938,
};

extern const char *names581[];
static const uint32 types581 [] =
{
SK_BOOL,
};

static const uint16 attr_flags581 [] =
{0,};

static const EIF_TYPE_INDEX g_atype581_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes581 [] = {
g_atype581_0,
};

static const int32 cn_attr581 [] =
{
877,
};

extern const char *names582[];
static const uint32 types582 [] =
{
SK_BOOL,
};

static const uint16 attr_flags582 [] =
{0,};

static const EIF_TYPE_INDEX g_atype582_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes582 [] = {
g_atype582_0,
};

static const int32 cn_attr582 [] =
{
877,
};

extern const char *names583[];
static const uint32 types583 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags583 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype583_0 [] = {0xFF01,248,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype583_1 [] = {0xFF01,178,0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype583_2 [] = {0xFF01,248,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype583_3 [] = {0xFF01,597,158,0xFFFF};
static const EIF_TYPE_INDEX g_atype583_4 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype583_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype583_6 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype583_7 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype583_8 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype583_9 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype583_10 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype583_11 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype583_12 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype583_13 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype583_14 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype583_15 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype583_16 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype583_17 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes583 [] = {
g_atype583_0,
g_atype583_1,
g_atype583_2,
g_atype583_3,
g_atype583_4,
g_atype583_5,
g_atype583_6,
g_atype583_7,
g_atype583_8,
g_atype583_9,
g_atype583_10,
g_atype583_11,
g_atype583_12,
g_atype583_13,
g_atype583_14,
g_atype583_15,
g_atype583_16,
g_atype583_17,
};

static const int32 cn_attr583 [] =
{
1997,
1998,
1999,
2000,
2013,
877,
1996,
2002,
2050,
1962,
1970,
2001,
2003,
2006,
2007,
2011,
2012,
2047,
};

extern const char *names584[];
static const uint32 types584 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags584 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype584_0 [] = {0xFF01,585,122,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype584_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype584_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype584_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype584_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype584_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype584_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes584 [] = {
g_atype584_0,
g_atype584_1,
g_atype584_2,
g_atype584_3,
g_atype584_4,
g_atype584_5,
g_atype584_6,
};

static const int32 cn_attr584 [] =
{
1789,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names586[];
static const uint32 types586 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags586 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype586_0 [] = {0xFF01,248,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype586_1 [] = {0xFF01,178,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype586_2 [] = {0xFF01,248,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype586_3 [] = {0xFF01,597,158,0xFFFF};
static const EIF_TYPE_INDEX g_atype586_4 [] = {0xFF02,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype586_5 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype586_6 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype586_7 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype586_8 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype586_9 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype586_10 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype586_11 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype586_12 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype586_13 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype586_14 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype586_15 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype586_16 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes586 [] = {
g_atype586_0,
g_atype586_1,
g_atype586_2,
g_atype586_3,
g_atype586_4,
g_atype586_5,
g_atype586_6,
g_atype586_7,
g_atype586_8,
g_atype586_9,
g_atype586_10,
g_atype586_11,
g_atype586_12,
g_atype586_13,
g_atype586_14,
g_atype586_15,
g_atype586_16,
};

static const int32 cn_attr586 [] =
{
1997,
1998,
1999,
2000,
2013,
877,
1996,
2002,
1962,
1970,
2001,
2003,
2006,
2007,
2011,
2012,
2047,
};

extern const char *names588[];
static const uint32 types588 [] =
{
SK_BOOL,
};

static const uint16 attr_flags588 [] =
{0,};

static const EIF_TYPE_INDEX g_atype588_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes588 [] = {
g_atype588_0,
};

static const int32 cn_attr588 [] =
{
877,
};

extern const char *names589[];
static const uint32 types589 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags589 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype589_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype589_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype589_2 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype589_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype589_4 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes589 [] = {
g_atype589_0,
g_atype589_1,
g_atype589_2,
g_atype589_3,
g_atype589_4,
};

static const int32 cn_attr589 [] =
{
3400,
3421,
3422,
3398,
3399,
};

extern const char *names590[];
static const uint32 types590 [] =
{
SK_BOOL,
};

static const uint16 attr_flags590 [] =
{0,};

static const EIF_TYPE_INDEX g_atype590_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes590 [] = {
g_atype590_0,
};

static const int32 cn_attr590 [] =
{
877,
};

extern const char *names591[];
static const uint32 types591 [] =
{
SK_BOOL,
};

static const uint16 attr_flags591 [] =
{0,};

static const EIF_TYPE_INDEX g_atype591_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes591 [] = {
g_atype591_0,
};

static const int32 cn_attr591 [] =
{
877,
};

extern const char *names592[];
static const uint32 types592 [] =
{
SK_REF,
};

static const uint16 attr_flags592 [] =
{0,};

static const EIF_TYPE_INDEX g_atype592_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes592 [] = {
g_atype592_0,
};

static const int32 cn_attr592 [] =
{
2139,
};

extern const char *names593[];
static const uint32 types593 [] =
{
SK_REF,
};

static const uint16 attr_flags593 [] =
{0,};

static const EIF_TYPE_INDEX g_atype593_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes593 [] = {
g_atype593_0,
};

static const int32 cn_attr593 [] =
{
2139,
};

extern const char *names594[];
static const uint32 types594 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT64,
};

static const uint16 attr_flags594 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype594_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype594_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype594_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype594_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype594_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype594_5 [] = {152,0xFFFF};

static const EIF_TYPE_INDEX *gtypes594 [] = {
g_atype594_0,
g_atype594_1,
g_atype594_2,
g_atype594_3,
g_atype594_4,
g_atype594_5,
};

static const int32 cn_attr594 [] =
{
3400,
3428,
3430,
3398,
3399,
3429,
};

extern const char *names595[];
static const uint32 types595 [] =
{
SK_POINTER,
};

static const uint16 attr_flags595 [] =
{0,};

static const EIF_TYPE_INDEX g_atype595_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes595 [] = {
g_atype595_0,
};

static const int32 cn_attr595 [] =
{
2849,
};

extern const char *names596[];
static const uint32 types596 [] =
{
SK_POINTER,
};

static const uint16 attr_flags596 [] =
{0,};

static const EIF_TYPE_INDEX g_atype596_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes596 [] = {
g_atype596_0,
};

static const int32 cn_attr596 [] =
{
2849,
};

extern const char *names597[];
static const uint32 types597 [] =
{
SK_REF,
};

static const uint16 attr_flags597 [] =
{0,};

static const EIF_TYPE_INDEX g_atype597_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes597 [] = {
g_atype597_0,
};

static const int32 cn_attr597 [] =
{
2139,
};

extern const char *names599[];
static const uint32 types599 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags599 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype599_0 [] = {0xFF01,599,158,0xFFFF};
static const EIF_TYPE_INDEX g_atype599_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype599_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype599_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype599_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype599_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype599_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes599 [] = {
g_atype599_0,
g_atype599_1,
g_atype599_2,
g_atype599_3,
g_atype599_4,
g_atype599_5,
g_atype599_6,
};

static const int32 cn_attr599 [] =
{
1789,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names603[];
static const uint32 types603 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags603 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype603_0 [] = {0xFF01,597,158,0xFFFF};
static const EIF_TYPE_INDEX g_atype603_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype603_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype603_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes603 [] = {
g_atype603_0,
g_atype603_1,
g_atype603_2,
g_atype603_3,
};

static const int32 cn_attr603 [] =
{
1632,
877,
1866,
1867,
};

extern const char *names604[];
static const uint32 types604 [] =
{
SK_BOOL,
};

static const uint16 attr_flags604 [] =
{0,};

static const EIF_TYPE_INDEX g_atype604_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes604 [] = {
g_atype604_0,
};

static const int32 cn_attr604 [] =
{
877,
};

extern const char *names605[];
static const uint32 types605 [] =
{
SK_BOOL,
};

static const uint16 attr_flags605 [] =
{0,};

static const EIF_TYPE_INDEX g_atype605_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes605 [] = {
g_atype605_0,
};

static const int32 cn_attr605 [] =
{
877,
};

extern const char *names606[];
static const uint32 types606 [] =
{
SK_BOOL,
};

static const uint16 attr_flags606 [] =
{0,};

static const EIF_TYPE_INDEX g_atype606_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes606 [] = {
g_atype606_0,
};

static const int32 cn_attr606 [] =
{
877,
};

extern const char *names607[];
static const uint32 types607 [] =
{
SK_BOOL,
};

static const uint16 attr_flags607 [] =
{0,};

static const EIF_TYPE_INDEX g_atype607_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes607 [] = {
g_atype607_0,
};

static const int32 cn_attr607 [] =
{
877,
};

extern const char *names608[];
static const uint32 types608 [] =
{
SK_BOOL,
};

static const uint16 attr_flags608 [] =
{0,};

static const EIF_TYPE_INDEX g_atype608_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes608 [] = {
g_atype608_0,
};

static const int32 cn_attr608 [] =
{
877,
};

extern const char *names609[];
static const uint32 types609 [] =
{
SK_BOOL,
};

static const uint16 attr_flags609 [] =
{0,};

static const EIF_TYPE_INDEX g_atype609_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes609 [] = {
g_atype609_0,
};

static const int32 cn_attr609 [] =
{
877,
};

extern const char *names610[];
static const uint32 types610 [] =
{
SK_BOOL,
};

static const uint16 attr_flags610 [] =
{0,};

static const EIF_TYPE_INDEX g_atype610_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes610 [] = {
g_atype610_0,
};

static const int32 cn_attr610 [] =
{
877,
};

extern const char *names611[];
static const uint32 types611 [] =
{
SK_BOOL,
};

static const uint16 attr_flags611 [] =
{0,};

static const EIF_TYPE_INDEX g_atype611_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes611 [] = {
g_atype611_0,
};

static const int32 cn_attr611 [] =
{
877,
};

extern const char *names612[];
static const uint32 types612 [] =
{
SK_BOOL,
};

static const uint16 attr_flags612 [] =
{0,};

static const EIF_TYPE_INDEX g_atype612_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes612 [] = {
g_atype612_0,
};

static const int32 cn_attr612 [] =
{
877,
};

extern const char *names613[];
static const uint32 types613 [] =
{
SK_BOOL,
};

static const uint16 attr_flags613 [] =
{0,};

static const EIF_TYPE_INDEX g_atype613_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes613 [] = {
g_atype613_0,
};

static const int32 cn_attr613 [] =
{
877,
};

extern const char *names614[];
static const uint32 types614 [] =
{
SK_BOOL,
};

static const uint16 attr_flags614 [] =
{0,};

static const EIF_TYPE_INDEX g_atype614_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes614 [] = {
g_atype614_0,
};

static const int32 cn_attr614 [] =
{
877,
};

extern const char *names615[];
static const uint32 types615 [] =
{
SK_REF,
};

static const uint16 attr_flags615 [] =
{0,};

static const EIF_TYPE_INDEX g_atype615_0 [] = {0xFF01,597,158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes615 [] = {
g_atype615_0,
};

static const int32 cn_attr615 [] =
{
1632,
};

extern const char *names617[];
static const uint32 types617 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags617 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype617_0 [] = {0xFF01,597,158,0xFFFF};
static const EIF_TYPE_INDEX g_atype617_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype617_2 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes617 [] = {
g_atype617_0,
g_atype617_1,
g_atype617_2,
};

static const int32 cn_attr617 [] =
{
1632,
877,
1958,
};

extern const char *names618[];
static const uint32 types618 [] =
{
SK_BOOL,
};

static const uint16 attr_flags618 [] =
{0,};

static const EIF_TYPE_INDEX g_atype618_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes618 [] = {
g_atype618_0,
};

static const int32 cn_attr618 [] =
{
877,
};

extern const char *names619[];
static const uint32 types619 [] =
{
SK_BOOL,
};

static const uint16 attr_flags619 [] =
{0,};

static const EIF_TYPE_INDEX g_atype619_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes619 [] = {
g_atype619_0,
};

static const int32 cn_attr619 [] =
{
877,
};

extern const char *names620[];
static const uint32 types620 [] =
{
SK_BOOL,
};

static const uint16 attr_flags620 [] =
{0,};

static const EIF_TYPE_INDEX g_atype620_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes620 [] = {
g_atype620_0,
};

static const int32 cn_attr620 [] =
{
877,
};

extern const char *names621[];
static const uint32 types621 [] =
{
SK_BOOL,
};

static const uint16 attr_flags621 [] =
{0,};

static const EIF_TYPE_INDEX g_atype621_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes621 [] = {
g_atype621_0,
};

static const int32 cn_attr621 [] =
{
877,
};

extern const char *names622[];
static const uint32 types622 [] =
{
SK_BOOL,
};

static const uint16 attr_flags622 [] =
{0,};

static const EIF_TYPE_INDEX g_atype622_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes622 [] = {
g_atype622_0,
};

static const int32 cn_attr622 [] =
{
877,
};

extern const char *names623[];
static const uint32 types623 [] =
{
SK_BOOL,
};

static const uint16 attr_flags623 [] =
{0,};

static const EIF_TYPE_INDEX g_atype623_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes623 [] = {
g_atype623_0,
};

static const int32 cn_attr623 [] =
{
877,
};

extern const char *names624[];
static const uint32 types624 [] =
{
SK_BOOL,
};

static const uint16 attr_flags624 [] =
{0,};

static const EIF_TYPE_INDEX g_atype624_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes624 [] = {
g_atype624_0,
};

static const int32 cn_attr624 [] =
{
877,
};

extern const char *names625[];
static const uint32 types625 [] =
{
SK_BOOL,
};

static const uint16 attr_flags625 [] =
{0,};

static const EIF_TYPE_INDEX g_atype625_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes625 [] = {
g_atype625_0,
};

static const int32 cn_attr625 [] =
{
877,
};

extern const char *names626[];
static const uint32 types626 [] =
{
SK_BOOL,
};

static const uint16 attr_flags626 [] =
{0,};

static const EIF_TYPE_INDEX g_atype626_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes626 [] = {
g_atype626_0,
};

static const int32 cn_attr626 [] =
{
877,
};

extern const char *names627[];
static const uint32 types627 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT64,
};

static const uint16 attr_flags627 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype627_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype627_1 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype627_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype627_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype627_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype627_5 [] = {152,0xFFFF};

static const EIF_TYPE_INDEX *gtypes627 [] = {
g_atype627_0,
g_atype627_1,
g_atype627_2,
g_atype627_3,
g_atype627_4,
g_atype627_5,
};

static const int32 cn_attr627 [] =
{
3400,
3415,
3398,
3399,
3414,
3413,
};

extern const char *names628[];
static const uint32 types628 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT64,
};

static const uint16 attr_flags628 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype628_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype628_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype628_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype628_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype628_4 [] = {152,0xFFFF};

static const EIF_TYPE_INDEX *gtypes628 [] = {
g_atype628_0,
g_atype628_1,
g_atype628_2,
g_atype628_3,
g_atype628_4,
};

static const int32 cn_attr628 [] =
{
3400,
3421,
3398,
3399,
3422,
};

extern const char *names629[];
static const uint32 types629 [] =
{
SK_REF,
SK_REF,
SK_UINT16,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags629 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype629_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype629_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype629_2 [] = {137,0xFFFF};
static const EIF_TYPE_INDEX g_atype629_3 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype629_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype629_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes629 [] = {
g_atype629_0,
g_atype629_1,
g_atype629_2,
g_atype629_3,
g_atype629_4,
g_atype629_5,
};

static const int32 cn_attr629 [] =
{
3400,
3428,
3429,
3430,
3398,
3399,
};

extern const char *names630[];
static const uint32 types630 [] =
{
SK_REF,
SK_REF,
SK_CHAR8,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags630 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype630_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype630_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype630_2 [] = {155,0xFFFF};
static const EIF_TYPE_INDEX g_atype630_3 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype630_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype630_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes630 [] = {
g_atype630_0,
g_atype630_1,
g_atype630_2,
g_atype630_3,
g_atype630_4,
g_atype630_5,
};

static const int32 cn_attr630 [] =
{
3400,
3428,
3429,
3430,
3398,
3399,
};

extern const char *names631[];
static const uint32 types631 [] =
{
SK_POINTER,
};

static const uint16 attr_flags631 [] =
{0,};

static const EIF_TYPE_INDEX g_atype631_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes631 [] = {
g_atype631_0,
};

static const int32 cn_attr631 [] =
{
2849,
};

extern const char *names632[];
static const uint32 types632 [] =
{
SK_POINTER,
};

static const uint16 attr_flags632 [] =
{0,};

static const EIF_TYPE_INDEX g_atype632_0 [] = {161,0xFFFF};

static const EIF_TYPE_INDEX *gtypes632 [] = {
g_atype632_0,
};

static const int32 cn_attr632 [] =
{
2849,
};

extern const char *names633[];
static const uint32 types633 [] =
{
SK_REF,
};

static const uint16 attr_flags633 [] =
{0,};

static const EIF_TYPE_INDEX g_atype633_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes633 [] = {
g_atype633_0,
};

static const int32 cn_attr633 [] =
{
2139,
};

extern const char *names634[];
static const uint32 types634 [] =
{
SK_REF,
SK_REF,
SK_CHAR8,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags634 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype634_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype634_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype634_2 [] = {155,0xFFFF};
static const EIF_TYPE_INDEX g_atype634_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype634_4 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes634 [] = {
g_atype634_0,
g_atype634_1,
g_atype634_2,
g_atype634_3,
g_atype634_4,
};

static const int32 cn_attr634 [] =
{
3400,
3421,
3422,
3398,
3399,
};

extern const char *names635[];
static const uint32 types635 [] =
{
SK_REF,
SK_CHAR8,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags635 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype635_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype635_1 [] = {155,0xFFFF};
static const EIF_TYPE_INDEX g_atype635_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype635_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype635_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype635_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes635 [] = {
g_atype635_0,
g_atype635_1,
g_atype635_2,
g_atype635_3,
g_atype635_4,
g_atype635_5,
};

static const int32 cn_attr635 [] =
{
3400,
3413,
3415,
3398,
3399,
3414,
};

extern const char *names636[];
static const uint32 types636 [] =
{
SK_REF,
};

static const uint16 attr_flags636 [] =
{0,};

static const EIF_TYPE_INDEX g_atype636_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes636 [] = {
g_atype636_0,
};

static const int32 cn_attr636 [] =
{
2139,
};

extern const char *names637[];
static const uint32 types637 [] =
{
SK_REF,
};

static const uint16 attr_flags637 [] =
{0,};

static const EIF_TYPE_INDEX g_atype637_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes637 [] = {
g_atype637_0,
};

static const int32 cn_attr637 [] =
{
2139,
};

extern const char *names638[];
static const uint32 types638 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags638 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype638_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype638_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype638_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype638_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype638_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype638_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes638 [] = {
g_atype638_0,
g_atype638_1,
g_atype638_2,
g_atype638_3,
g_atype638_4,
g_atype638_5,
};

static const int32 cn_attr638 [] =
{
3400,
3428,
3430,
3398,
3399,
3429,
};

extern const char *names640[];
static const uint32 types640 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags640 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype640_0 [] = {0xFF01,638,149,0xFFFF};
static const EIF_TYPE_INDEX g_atype640_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype640_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype640_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes640 [] = {
g_atype640_0,
g_atype640_1,
g_atype640_2,
g_atype640_3,
};

static const int32 cn_attr640 [] =
{
1632,
877,
1866,
1867,
};

extern const char *names641[];
static const uint32 types641 [] =
{
SK_BOOL,
};

static const uint16 attr_flags641 [] =
{0,};

static const EIF_TYPE_INDEX g_atype641_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes641 [] = {
g_atype641_0,
};

static const int32 cn_attr641 [] =
{
877,
};

extern const char *names642[];
static const uint32 types642 [] =
{
SK_BOOL,
};

static const uint16 attr_flags642 [] =
{0,};

static const EIF_TYPE_INDEX g_atype642_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes642 [] = {
g_atype642_0,
};

static const int32 cn_attr642 [] =
{
877,
};

extern const char *names643[];
static const uint32 types643 [] =
{
SK_BOOL,
};

static const uint16 attr_flags643 [] =
{0,};

static const EIF_TYPE_INDEX g_atype643_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes643 [] = {
g_atype643_0,
};

static const int32 cn_attr643 [] =
{
877,
};

extern const char *names644[];
static const uint32 types644 [] =
{
SK_BOOL,
};

static const uint16 attr_flags644 [] =
{0,};

static const EIF_TYPE_INDEX g_atype644_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes644 [] = {
g_atype644_0,
};

static const int32 cn_attr644 [] =
{
877,
};

extern const char *names645[];
static const uint32 types645 [] =
{
SK_REF,
};

static const uint16 attr_flags645 [] =
{0,};

static const EIF_TYPE_INDEX g_atype645_0 [] = {0xFF01,638,149,0xFFFF};

static const EIF_TYPE_INDEX *gtypes645 [] = {
g_atype645_0,
};

static const int32 cn_attr645 [] =
{
1632,
};

extern const char *names647[];
static const uint32 types647 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags647 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype647_0 [] = {0xFF01,638,149,0xFFFF};
static const EIF_TYPE_INDEX g_atype647_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype647_2 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes647 [] = {
g_atype647_0,
g_atype647_1,
g_atype647_2,
};

static const int32 cn_attr647 [] =
{
1632,
877,
1958,
};

extern const char *names648[];
static const uint32 types648 [] =
{
SK_BOOL,
};

static const uint16 attr_flags648 [] =
{0,};

static const EIF_TYPE_INDEX g_atype648_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes648 [] = {
g_atype648_0,
};

static const int32 cn_attr648 [] =
{
877,
};

extern const char *names649[];
static const uint32 types649 [] =
{
SK_BOOL,
};

static const uint16 attr_flags649 [] =
{0,};

static const EIF_TYPE_INDEX g_atype649_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes649 [] = {
g_atype649_0,
};

static const int32 cn_attr649 [] =
{
877,
};

extern const char *names650[];
static const uint32 types650 [] =
{
SK_BOOL,
};

static const uint16 attr_flags650 [] =
{0,};

static const EIF_TYPE_INDEX g_atype650_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes650 [] = {
g_atype650_0,
};

static const int32 cn_attr650 [] =
{
877,
};

extern const char *names651[];
static const uint32 types651 [] =
{
SK_BOOL,
};

static const uint16 attr_flags651 [] =
{0,};

static const EIF_TYPE_INDEX g_atype651_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes651 [] = {
g_atype651_0,
};

static const int32 cn_attr651 [] =
{
877,
};

extern const char *names652[];
static const uint32 types652 [] =
{
SK_BOOL,
};

static const uint16 attr_flags652 [] =
{0,};

static const EIF_TYPE_INDEX g_atype652_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes652 [] = {
g_atype652_0,
};

static const int32 cn_attr652 [] =
{
877,
};

extern const char *names653[];
static const uint32 types653 [] =
{
SK_BOOL,
};

static const uint16 attr_flags653 [] =
{0,};

static const EIF_TYPE_INDEX g_atype653_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes653 [] = {
g_atype653_0,
};

static const int32 cn_attr653 [] =
{
877,
};

extern const char *names654[];
static const uint32 types654 [] =
{
SK_BOOL,
};

static const uint16 attr_flags654 [] =
{0,};

static const EIF_TYPE_INDEX g_atype654_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes654 [] = {
g_atype654_0,
};

static const int32 cn_attr654 [] =
{
877,
};

extern const char *names655[];
static const uint32 types655 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags655 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype655_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype655_1 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype655_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype655_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype655_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype655_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes655 [] = {
g_atype655_0,
g_atype655_1,
g_atype655_2,
g_atype655_3,
g_atype655_4,
g_atype655_5,
};

static const int32 cn_attr655 [] =
{
3400,
3415,
3398,
3399,
3413,
3414,
};

extern const char *names656[];
static const uint32 types656 [] =
{
SK_REF,
SK_REF,
SK_CHAR32,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags656 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype656_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype656_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype656_2 [] = {149,0xFFFF};
static const EIF_TYPE_INDEX g_atype656_3 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype656_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype656_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes656 [] = {
g_atype656_0,
g_atype656_1,
g_atype656_2,
g_atype656_3,
g_atype656_4,
g_atype656_5,
};

static const int32 cn_attr656 [] =
{
3400,
3428,
3429,
3430,
3398,
3399,
};

extern const char *names657[];
static const uint32 types657 [] =
{
SK_BOOL,
};

static const uint16 attr_flags657 [] =
{0,};

static const EIF_TYPE_INDEX g_atype657_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes657 [] = {
g_atype657_0,
};

static const int32 cn_attr657 [] =
{
533,
};

extern const char *names658[];
static const uint32 types658 [] =
{
SK_REF,
};

static const uint16 attr_flags658 [] =
{0,};

static const EIF_TYPE_INDEX g_atype658_0 [] = {170,0xFFFF};

static const EIF_TYPE_INDEX *gtypes658 [] = {
g_atype658_0,
};

static const int32 cn_attr658 [] =
{
2139,
};

extern const char *names659[];
static const uint32 types659 [] =
{
SK_REF,
SK_REF,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags659 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype659_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype659_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype659_2 [] = {125,0xFFFF};
static const EIF_TYPE_INDEX g_atype659_3 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype659_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype659_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes659 [] = {
g_atype659_0,
g_atype659_1,
g_atype659_2,
g_atype659_3,
g_atype659_4,
g_atype659_5,
};

static const int32 cn_attr659 [] =
{
3400,
3428,
3429,
3430,
3398,
3399,
};

extern const char *names660[];
static const uint32 types660 [] =
{
SK_REF,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags660 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype660_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype660_1 [] = {125,0xFFFF};
static const EIF_TYPE_INDEX g_atype660_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype660_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype660_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype660_5 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes660 [] = {
g_atype660_0,
g_atype660_1,
g_atype660_2,
g_atype660_3,
g_atype660_4,
g_atype660_5,
};

static const int32 cn_attr660 [] =
{
3400,
3413,
3415,
3398,
3399,
3414,
};

extern const char *names661[];
static const uint32 types661 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags661 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype661_0 [] = {0xFF01,677,146,0xFFFF};
static const EIF_TYPE_INDEX g_atype661_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype661_2 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype661_3 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes661 [] = {
g_atype661_0,
g_atype661_1,
g_atype661_2,
g_atype661_3,
};

static const int32 cn_attr661 [] =
{
1632,
877,
1866,
1867,
};

extern const char *names662[];
static const uint32 types662 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags662 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype662_0 [] = {0xFF01,662,146,0xFFFF};
static const EIF_TYPE_INDEX g_atype662_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype662_2 [] = {134,0xFFFF};
static const EIF_TYPE_INDEX g_atype662_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype662_4 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype662_5 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype662_6 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes662 [] = {
g_atype662_0,
g_atype662_1,
g_atype662_2,
g_atype662_3,
g_atype662_4,
g_atype662_5,
g_atype662_6,
};

static const int32 cn_attr662 [] =
{
1789,
1782,
1781,
1773,
1774,
1775,
1776,
};

extern const char *names666[];
static const uint32 types666 [] =
{
SK_BOOL,
};

static const uint16 attr_flags666 [] =
{0,};

static const EIF_TYPE_INDEX g_atype666_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes666 [] = {
g_atype666_0,
};

static const int32 cn_attr666 [] =
{
877,
};

extern const char *names667[];
static const uint32 types667 [] =
{
SK_BOOL,
};

static const uint16 attr_flags667 [] =
{0,};

static const EIF_TYPE_INDEX g_atype667_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes667 [] = {
g_atype667_0,
};

static const int32 cn_attr667 [] =
{
877,
};

extern const char *names668[];
static const uint32 types668 [] =
{
SK_BOOL,
};

static const uint16 attr_flags668 [] =
{0,};

static const EIF_TYPE_INDEX g_atype668_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes668 [] = {
g_atype668_0,
};

static const int32 cn_attr668 [] =
{
877,
};

extern const char *names669[];
static const uint32 types669 [] =
{
SK_BOOL,
};

static const uint16 attr_flags669 [] =
{0,};

static const EIF_TYPE_INDEX g_atype669_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes669 [] = {
g_atype669_0,
};

static const int32 cn_attr669 [] =
{
877,
};

extern const char *names670[];
static const uint32 types670 [] =
{
SK_BOOL,
};

static const uint16 attr_flags670 [] =
{0,};

static const EIF_TYPE_INDEX g_atype670_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes670 [] = {
g_atype670_0,
};

static const int32 cn_attr670 [] =
{
877,
};

extern const char *names671[];
static const uint32 types671 [] =
{
SK_BOOL,
};

static const uint16 attr_flags671 [] =
{0,};

static const EIF_TYPE_INDEX g_atype671_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes671 [] = {
g_atype671_0,
};

static const int32 cn_attr671 [] =
{
877,
};

extern const char *names672[];
static const uint32 types672 [] =
{
SK_BOOL,
};

static const uint16 attr_flags672 [] =
{0,};

static const EIF_TYPE_INDEX g_atype672_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes672 [] = {
g_atype672_0,
};

static const int32 cn_attr672 [] =
{
877,
};

extern const char *names673[];
static const uint32 types673 [] =
{
SK_BOOL,
};

static const uint16 attr_flags673 [] =
{0,};

static const EIF_TYPE_INDEX g_atype673_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes673 [] = {
g_atype673_0,
};

static const int32 cn_attr673 [] =
{
877,
};

extern const char *names674[];
static const uint32 types674 [] =
{
SK_BOOL,
};

static const uint16 attr_flags674 [] =
{0,};

static const EIF_TYPE_INDEX g_atype674_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes674 [] = {
g_atype674_0,
};

static const int32 cn_attr674 [] =
{
877,
};

extern const char *names675[];
static const uint32 types675 [] =
{
SK_BOOL,
};

static const uint16 attr_flags675 [] =
{0,};

static const EIF_TYPE_INDEX g_atype675_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes675 [] = {
g_atype675_0,
};

static const int32 cn_attr675 [] =
{
877,
};

extern const char *names676[];
static const uint32 types676 [] =
{
SK_BOOL,
};

static const uint16 attr_flags676 [] =
{0,};

static const EIF_TYPE_INDEX g_atype676_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes676 [] = {
g_atype676_0,
};

static const int32 cn_attr676 [] =
{
877,
};

extern const char *names677[];
static const uint32 types677 [] =
{
SK_REF,
};

static const uint16 attr_flags677 [] =
{0,};

static const EIF_TYPE_INDEX g_atype677_0 [] = {0xFF01,677,146,0xFFFF};

static const EIF_TYPE_INDEX *gtypes677 [] = {
g_atype677_0,
};

static const int32 cn_attr677 [] =
{
1632,
};

extern const char *names680[];
static const uint32 types680 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags680 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype680_0 [] = {0xFF01,677,146,0xFFFF};
static const EIF_TYPE_INDEX g_atype680_1 [] = {158,0xFFFF};
static const EIF_TYPE_INDEX g_atype680_2 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes680 [] = {
g_atype680_0,
g_atype680_1,
g_atype680_2,
};

static const int32 cn_attr680 [] =
{
1632,
877,
1958,
};

extern const char *names681[];
static const uint32 types681 [] =
{
SK_BOOL,
};

static const uint16 attr_flags681 [] =
{0,};

static const EIF_TYPE_INDEX g_atype681_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes681 [] = {
g_atype681_0,
};

static const int32 cn_attr681 [] =
{
877,
};

extern const char *names682[];
static const uint32 types682 [] =
{
SK_BOOL,
};

static const uint16 attr_flags682 [] =
{0,};

static const EIF_TYPE_INDEX g_atype682_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes682 [] = {
g_atype682_0,
};

static const int32 cn_attr682 [] =
{
877,
};

extern const char *names683[];
static const uint32 types683 [] =
{
SK_BOOL,
};

static const uint16 attr_flags683 [] =
{0,};

static const EIF_TYPE_INDEX g_atype683_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes683 [] = {
g_atype683_0,
};

static const int32 cn_attr683 [] =
{
877,
};

extern const char *names684[];
static const uint32 types684 [] =
{
SK_BOOL,
};

static const uint16 attr_flags684 [] =
{0,};

static const EIF_TYPE_INDEX g_atype684_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes684 [] = {
g_atype684_0,
};

static const int32 cn_attr684 [] =
{
877,
};

extern const char *names685[];
static const uint32 types685 [] =
{
SK_BOOL,
};

static const uint16 attr_flags685 [] =
{0,};

static const EIF_TYPE_INDEX g_atype685_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes685 [] = {
g_atype685_0,
};

static const int32 cn_attr685 [] =
{
877,
};

extern const char *names686[];
static const uint32 types686 [] =
{
SK_BOOL,
};

static const uint16 attr_flags686 [] =
{0,};

static const EIF_TYPE_INDEX g_atype686_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes686 [] = {
g_atype686_0,
};

static const int32 cn_attr686 [] =
{
877,
};

extern const char *names687[];
static const uint32 types687 [] =
{
SK_BOOL,
};

static const uint16 attr_flags687 [] =
{0,};

static const EIF_TYPE_INDEX g_atype687_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes687 [] = {
g_atype687_0,
};

static const int32 cn_attr687 [] =
{
877,
};

extern const char *names688[];
static const uint32 types688 [] =
{
SK_BOOL,
};

static const uint16 attr_flags688 [] =
{0,};

static const EIF_TYPE_INDEX g_atype688_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes688 [] = {
g_atype688_0,
};

static const int32 cn_attr688 [] =
{
877,
};

extern const char *names689[];
static const uint32 types689 [] =
{
SK_BOOL,
};

static const uint16 attr_flags689 [] =
{0,};

static const EIF_TYPE_INDEX g_atype689_0 [] = {158,0xFFFF};

static const EIF_TYPE_INDEX *gtypes689 [] = {
g_atype689_0,
};

static const int32 cn_attr689 [] =
{
877,
};

extern const char *names690[];
static const uint32 types690 [] =
{
SK_CHAR32,
};

static const uint16 attr_flags690 [] =
{0,};

static const EIF_TYPE_INDEX g_atype690_0 [] = {149,0xFFFF};

static const EIF_TYPE_INDEX *gtypes690 [] = {
g_atype690_0,
};

static const int32 cn_attr690 [] =
{
533,
};

extern const char *names691[];
static const uint32 types691 [] =
{
SK_REF,
SK_REF,
SK_INT8,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags691 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype691_0 [] = {0xFFF9,2,120,122,122,0xFFFF};
static const EIF_TYPE_INDEX g_atype691_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype691_2 [] = {128,0xFFFF};
static const EIF_TYPE_INDEX g_atype691_3 [] = {122,0xFFFF};
static const EIF_TYPE_INDEX g_atype691_4 [] = {122,0xFFFF};

static const EIF_TYPE_INDEX *gtypes691 [] = {
g_atype691_0,
g_atype691_1,
g_atype691_2,
g_atype691_3,
g_atype691_4,
};

static const int32 cn_attr691 [] =
{
3400,
3421,
3422,
3398,
3399,
};

const struct cnode egc_fsystem_init[] = {
{
	(long) 0,
	(long) 0,
	"ANY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 0,
	"VERSIONABLE",
	names2,
	types2,
	attr_flags2,
	gtypes2,
	(uint16) 4096,
	cn_attr2,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"SYSTEM_STRING_FACTORY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"CHARACTER_PROPERTY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"UTF_CONVERTER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 768,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"UTF_CONVERTER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 256,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"SYSTEM_STRING",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"RT_DBG_EXECUTION_PARAMETERS",
	names8,
	types8,
	attr_flags8,
	gtypes8,
	(uint16) 0,
	cn_attr8,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ISE_RUNTIME",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"STD_FILES",
	names10,
	types10,
	attr_flags10,
	gtypes10,
	(uint16) 0,
	cn_attr10,
	4,
	1L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"OPERATING_ENVIRONMENT",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHOPSTICK",
	names12,
	types12,
	attr_flags12,
	gtypes12,
	(uint16) 0,
	cn_attr12,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 12,
	(long) 12,
	"OBJECT_GRAPH_TRAVERSABLE",
	names13,
	types13,
	attr_flags13,
	gtypes13,
	(uint16) 4096,
	cn_attr13,
	32,
	6L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 12,
	(long) 12,
	"OBJECT_GRAPH_BREADTH_FIRST_TRAVERSABLE",
	names14,
	types14,
	attr_flags14,
	gtypes14,
	(uint16) 0,
	cn_attr14,
	32,
	6L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"MEMORY_STRUCTURE",
	names15,
	types15,
	attr_flags15,
	gtypes15,
	(uint16) 4096,
	cn_attr15,
	12,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"OBJECT_GRAPH_MARKER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"MATH_CONST",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"DOUBLE_MATH",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"REFLECTOR_HELPER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"REFACTORING_HELPER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"EXCEPTION_MANAGER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ISE_EXCEPTION_MANAGER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"EXCEP_CONST",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"PLATFORM",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"THREAD_ENVIRONMENT",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"THREAD_CONTROL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"THREAD",
	names27,
	types27,
	attr_flags27,
	gtypes27,
	(uint16) 4096,
	cn_attr27,
	12,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 10,
	(long) 10,
	"PHILOSOPHER",
	names28,
	types28,
	attr_flags28,
	gtypes28,
	(uint16) 0,
	cn_attr28,
	36,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"RT_EXTENSION_COMMON",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"RT_EXTENSION_GENERAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"RT_EXTENSION",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"RT_DBG_COMMON",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 2,
	(long) 2,
	"NUMERIC_INFORMATION",
	names33,
	types33,
	attr_flags33,
	gtypes33,
	(uint16) 0,
	cn_attr33,
	16,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"INTEGER_OVERFLOW_CHECKER",
	names34,
	types34,
	attr_flags34,
	gtypes34,
	(uint16) 0,
	cn_attr34,
	32,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 9,
	(long) 9,
	"STRING_TO_NUMERIC_CONVERTOR",
	names35,
	types35,
	attr_flags35,
	gtypes35,
	(uint16) 4096,
	cn_attr35,
	40,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 13,
	(long) 13,
	"HEXADECIMAL_STRING_TO_INTEGER_CONVERTER",
	names36,
	types36,
	attr_flags36,
	gtypes36,
	(uint16) 8192,
	cn_attr36,
	56,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 12,
	(long) 12,
	"STRING_TO_INTEGER_CONVERTOR",
	names37,
	types37,
	attr_flags37,
	gtypes37,
	(uint16) 8192,
	cn_attr37,
	56,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 17,
	(long) 17,
	"STRING_TO_REAL_CONVERTOR",
	names38,
	types38,
	attr_flags38,
	gtypes38,
	(uint16) 8192,
	cn_attr38,
	72,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 2,
	(long) 2,
	"STRING_SEARCHER",
	names39,
	types39,
	attr_flags39,
	gtypes39,
	(uint16) 4096,
	cn_attr39,
	8,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 2,
	(long) 2,
	"STRING_32_SEARCHER",
	names40,
	types40,
	attr_flags40,
	gtypes40,
	(uint16) 8192,
	cn_attr40,
	8,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 2,
	(long) 2,
	"STRING_8_SEARCHER",
	names41,
	types41,
	attr_flags41,
	gtypes41,
	(uint16) 8192,
	cn_attr41,
	8,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"EXCEPTION_MANAGER_FACTORY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"EXCEPTIONS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"EXCEPTION",
	names44,
	types44,
	attr_flags44,
	gtypes44,
	(uint16) 0,
	cn_attr44,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"DEVELOPER_EXCEPTION",
	names45,
	types45,
	attr_flags45,
	gtypes45,
	(uint16) 0,
	cn_attr45,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"MACHINE_EXCEPTION",
	names46,
	types46,
	attr_flags46,
	gtypes46,
	(uint16) 4096,
	cn_attr46,
	28,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"HARDWARE_EXCEPTION",
	names47,
	types47,
	attr_flags47,
	gtypes47,
	(uint16) 4096,
	cn_attr47,
	28,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"FLOATING_POINT_FAILURE",
	names48,
	types48,
	attr_flags48,
	gtypes48,
	(uint16) 0,
	cn_attr48,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"OPERATING_SYSTEM_EXCEPTION",
	names49,
	types49,
	attr_flags49,
	gtypes49,
	(uint16) 4096,
	cn_attr49,
	28,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 10,
	(long) 10,
	"COM_FAILURE",
	names50,
	types50,
	attr_flags50,
	gtypes50,
	(uint16) 0,
	cn_attr50,
	40,
	6L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 8,
	(long) 8,
	"OPERATING_SYSTEM_FAILURE",
	names51,
	types51,
	attr_flags51,
	gtypes51,
	(uint16) 0,
	cn_attr51,
	32,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 8,
	(long) 8,
	"OPERATING_SYSTEM_SIGNAL_FAILURE",
	names52,
	types52,
	attr_flags52,
	gtypes52,
	(uint16) 0,
	cn_attr52,
	32,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"OBSOLETE_EXCEPTION",
	names53,
	types53,
	attr_flags53,
	gtypes53,
	(uint16) 4096,
	cn_attr53,
	28,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"EXCEPTION_IN_SIGNAL_HANDLER_FAILURE",
	names54,
	types54,
	attr_flags54,
	gtypes54,
	(uint16) 0,
	cn_attr54,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"RESUMPTION_FAILURE",
	names55,
	types55,
	attr_flags55,
	gtypes55,
	(uint16) 0,
	cn_attr55,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"RESCUE_FAILURE",
	names56,
	types56,
	attr_flags56,
	gtypes56,
	(uint16) 0,
	cn_attr56,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"SYS_EXCEPTION",
	names57,
	types57,
	attr_flags57,
	gtypes57,
	(uint16) 4096,
	cn_attr57,
	28,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"OLD_VIOLATION",
	names58,
	types58,
	attr_flags58,
	gtypes58,
	(uint16) 0,
	cn_attr58,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 8,
	(long) 8,
	"EIFFEL_RUNTIME_PANIC",
	names59,
	types59,
	attr_flags59,
	gtypes59,
	(uint16) 0,
	cn_attr59,
	32,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"EIF_EXCEPTION",
	names60,
	types60,
	attr_flags60,
	gtypes60,
	(uint16) 4096,
	cn_attr60,
	28,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"EIFFEL_RUNTIME_EXCEPTION",
	names61,
	types61,
	attr_flags61,
	gtypes61,
	(uint16) 4096,
	cn_attr61,
	28,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"EXTERNAL_FAILURE",
	names62,
	types62,
	attr_flags62,
	gtypes62,
	(uint16) 0,
	cn_attr62,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 8,
	(long) 8,
	"NO_MORE_MEMORY",
	names63,
	types63,
	attr_flags63,
	gtypes63,
	(uint16) 0,
	cn_attr63,
	32,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"DATA_EXCEPTION",
	names64,
	types64,
	attr_flags64,
	gtypes64,
	(uint16) 4096,
	cn_attr64,
	28,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"MISMATCH_FAILURE",
	names65,
	types65,
	attr_flags65,
	gtypes65,
	(uint16) 0,
	cn_attr65,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 9,
	(long) 9,
	"IO_FAILURE",
	names66,
	types66,
	attr_flags66,
	gtypes66,
	(uint16) 0,
	cn_attr66,
	36,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"SERIALIZATION_FAILURE",
	names67,
	types67,
	attr_flags67,
	gtypes67,
	(uint16) 0,
	cn_attr67,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"LANGUAGE_EXCEPTION",
	names68,
	types68,
	attr_flags68,
	gtypes68,
	(uint16) 4096,
	cn_attr68,
	28,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"VOID_ASSIGNED_TO_EXPANDED",
	names69,
	types69,
	attr_flags69,
	gtypes69,
	(uint16) 0,
	cn_attr69,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"BAD_INSPECT_VALUE",
	names70,
	types70,
	attr_flags70,
	gtypes70,
	(uint16) 0,
	cn_attr70,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 9,
	(long) 9,
	"ROUTINE_FAILURE",
	names71,
	types71,
	attr_flags71,
	gtypes71,
	(uint16) 0,
	cn_attr71,
	36,
	7L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"VOID_TARGET",
	names72,
	types72,
	attr_flags72,
	gtypes72,
	(uint16) 0,
	cn_attr72,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"EIFFELSTUDIO_SPECIFIC_LANGUAGE_EXCEPTION",
	names73,
	types73,
	attr_flags73,
	gtypes73,
	(uint16) 4096,
	cn_attr73,
	28,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"ADDRESS_APPLIED_TO_MELTED_FEATURE",
	names74,
	types74,
	attr_flags74,
	gtypes74,
	(uint16) 0,
	cn_attr74,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"CREATE_ON_DEFERRED",
	names75,
	types75,
	attr_flags75,
	gtypes75,
	(uint16) 0,
	cn_attr75,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"ASSERTION_VIOLATION",
	names76,
	types76,
	attr_flags76,
	gtypes76,
	(uint16) 4096,
	cn_attr76,
	28,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"LOOP_INVARIANT_VIOLATION",
	names77,
	types77,
	attr_flags77,
	gtypes77,
	(uint16) 0,
	cn_attr77,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"VARIANT_VIOLATION",
	names78,
	types78,
	attr_flags78,
	gtypes78,
	(uint16) 0,
	cn_attr78,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"CHECK_VIOLATION",
	names79,
	types79,
	attr_flags79,
	gtypes79,
	(uint16) 0,
	cn_attr79,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 8,
	(long) 8,
	"INVARIANT_VIOLATION",
	names80,
	types80,
	attr_flags80,
	gtypes80,
	(uint16) 0,
	cn_attr80,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"POSTCONDITION_VIOLATION",
	names81,
	types81,
	attr_flags81,
	gtypes81,
	(uint16) 0,
	cn_attr81,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 7,
	"PRECONDITION_VIOLATION",
	names82,
	types82,
	attr_flags82,
	gtypes82,
	(uint16) 0,
	cn_attr82,
	28,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"PART_COMPARABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"COMPARABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 2,
	(long) 2,
	"PRIMES",
	names85,
	types85,
	attr_flags85,
	gtypes85,
	(uint16) 0,
	cn_attr85,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"DISPOSABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 2,
	"MANAGED_POINTER",
	names87,
	types87,
	attr_flags87,
	gtypes87,
	(uint16) 1024,
	cn_attr87,
	20,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 2,
	(long) 2,
	"MUTEX",
	names88,
	types88,
	attr_flags88,
	gtypes88,
	(uint16) 1024,
	cn_attr88,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"STRING_HANDLER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 2,
	(long) 2,
	"C_STRING",
	names90,
	types90,
	attr_flags90,
	gtypes90,
	(uint16) 0,
	cn_attr90,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 13,
	(long) 13,
	"IO_MEDIUM",
	names91,
	types91,
	attr_flags91,
	gtypes91,
	(uint16) 5120,
	cn_attr91,
	52,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"HASH_TABLE_CURSOR",
	names93,
	types93,
	attr_flags93,
	gtypes93,
	(uint16) 0,
	cn_attr93,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"ARRAYED_LIST_CURSOR",
	names94,
	types94,
	attr_flags94,
	gtypes94,
	(uint16) 0,
	cn_attr94,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_STRING_HANDLER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 2,
	(long) 2,
	"NATIVE_STRING",
	names96,
	types96,
	attr_flags96,
	gtypes96,
	(uint16) 0,
	cn_attr96,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"EXECUTION_ENVIRONMENT",
	names97,
	types97,
	attr_flags97,
	gtypes97,
	(uint16) 0,
	cn_attr97,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 20,
	(long) 19,
	"FILE",
	names98,
	types98,
	attr_flags98,
	gtypes98,
	(uint16) 5120,
	cn_attr98,
	72,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 21,
	(long) 20,
	"RAW_FILE",
	names99,
	types99,
	attr_flags99,
	gtypes99,
	(uint16) 1024,
	cn_attr99,
	76,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 21,
	(long) 20,
	"PLAIN_TEXT_FILE",
	names100,
	types100,
	attr_flags100,
	gtypes100,
	(uint16) 1024,
	cn_attr100,
	72,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"REFLECTOR_CONSTANTS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"REFLECTED_OBJECT",
	names102,
	types102,
	attr_flags102,
	gtypes102,
	(uint16) 4096,
	cn_attr102,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"REFLECTOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"INTERNAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"REFLECTED_COPY_SEMANTICS_OBJECT",
	names105,
	types105,
	attr_flags105,
	gtypes105,
	(uint16) 0,
	cn_attr105,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"REFLECTED_REFERENCE_OBJECT",
	names106,
	types106,
	attr_flags106,
	gtypes106,
	(uint16) 0,
	cn_attr106,
	12,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"FILE_INFO",
	names107,
	types107,
	attr_flags107,
	gtypes107,
	(uint16) 0,
	cn_attr107,
	16,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"UNIX_FILE_INFO",
	names108,
	types108,
	attr_flags108,
	gtypes108,
	(uint16) 0,
	cn_attr108,
	16,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"DEBUG_OUTPUT",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ABSTRACT_SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"NUMERIC",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"STRING_ITERATION_CURSOR",
	names112,
	types112,
	attr_flags112,
	gtypes112,
	(uint16) 0,
	cn_attr112,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ARGUMENTS_32",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ARGUMENTS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"APPLICATION",
	names115,
	types115,
	attr_flags115,
	gtypes115,
	(uint16) 0,
	cn_attr115,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"INTEGER_INTERVAL",
	names116,
	types116,
	attr_flags116,
	gtypes116,
	(uint16) 0,
	cn_attr116,
	12,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"MISMATCH_CORRECTOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 19,
	(long) 19,
	"MISMATCH_INFORMATION",
	names118,
	types118,
	attr_flags118,
	gtypes118,
	(uint16) 0,
	cn_attr118,
	68,
	9L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"HASHABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"PATH",
	names120,
	types120,
	attr_flags120,
	gtypes120,
	(uint16) 0,
	cn_attr120,
	12,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"TUPLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_32_REF",
	names122,
	types122,
	attr_flags122,
	gtypes122,
	(uint16) 0,
	cn_attr122,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_32",
	names123,
	types123,
	attr_flags123,
	gtypes123,
	(uint16) 8968,
	cn_attr123,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_32",
	names124,
	types124,
	attr_flags124,
	gtypes124,
	(uint16) 8448,
	cn_attr124,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_16_REF",
	names125,
	types125,
	attr_flags125,
	gtypes125,
	(uint16) 0,
	cn_attr125,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_16",
	names126,
	types126,
	attr_flags126,
	gtypes126,
	(uint16) 8967,
	cn_attr126,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_16",
	names127,
	types127,
	attr_flags127,
	gtypes127,
	(uint16) 8448,
	cn_attr127,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_8_REF",
	names128,
	types128,
	attr_flags128,
	gtypes128,
	(uint16) 0,
	cn_attr128,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_8",
	names129,
	types129,
	attr_flags129,
	gtypes129,
	(uint16) 8966,
	cn_attr129,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_8",
	names130,
	types130,
	attr_flags130,
	gtypes130,
	(uint16) 8448,
	cn_attr130,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_64_REF",
	names131,
	types131,
	attr_flags131,
	gtypes131,
	(uint16) 0,
	cn_attr131,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_64",
	names132,
	types132,
	attr_flags132,
	gtypes132,
	(uint16) 8973,
	cn_attr132,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_64",
	names133,
	types133,
	attr_flags133,
	gtypes133,
	(uint16) 8448,
	cn_attr133,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_32_REF",
	names134,
	types134,
	attr_flags134,
	gtypes134,
	(uint16) 0,
	cn_attr134,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_32",
	names135,
	types135,
	attr_flags135,
	gtypes135,
	(uint16) 8972,
	cn_attr135,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_32",
	names136,
	types136,
	attr_flags136,
	gtypes136,
	(uint16) 8448,
	cn_attr136,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_16_REF",
	names137,
	types137,
	attr_flags137,
	gtypes137,
	(uint16) 0,
	cn_attr137,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_16",
	names138,
	types138,
	attr_flags138,
	gtypes138,
	(uint16) 8971,
	cn_attr138,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_16",
	names139,
	types139,
	attr_flags139,
	gtypes139,
	(uint16) 8448,
	cn_attr139,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_8_REF",
	names140,
	types140,
	attr_flags140,
	gtypes140,
	(uint16) 0,
	cn_attr140,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_8",
	names141,
	types141,
	attr_flags141,
	gtypes141,
	(uint16) 8970,
	cn_attr141,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_8",
	names142,
	types142,
	attr_flags142,
	gtypes142,
	(uint16) 8448,
	cn_attr142,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_32_REF",
	names143,
	types143,
	attr_flags143,
	gtypes143,
	(uint16) 0,
	cn_attr143,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_32",
	names144,
	types144,
	attr_flags144,
	gtypes144,
	(uint16) 8964,
	cn_attr144,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_32",
	names145,
	types145,
	attr_flags145,
	gtypes145,
	(uint16) 8448,
	cn_attr145,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_64_REF",
	names146,
	types146,
	attr_flags146,
	gtypes146,
	(uint16) 0,
	cn_attr146,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_64",
	names147,
	types147,
	attr_flags147,
	gtypes147,
	(uint16) 8963,
	cn_attr147,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_64",
	names148,
	types148,
	attr_flags148,
	gtypes148,
	(uint16) 8448,
	cn_attr148,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_32_REF",
	names149,
	types149,
	attr_flags149,
	gtypes149,
	(uint16) 0,
	cn_attr149,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_32",
	names150,
	types150,
	attr_flags150,
	gtypes150,
	(uint16) 8974,
	cn_attr150,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_32",
	names151,
	types151,
	attr_flags151,
	gtypes151,
	(uint16) 8448,
	cn_attr151,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_64_REF",
	names152,
	types152,
	attr_flags152,
	gtypes152,
	(uint16) 0,
	cn_attr152,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_64",
	names153,
	types153,
	attr_flags153,
	gtypes153,
	(uint16) 8969,
	cn_attr153,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_64",
	names154,
	types154,
	attr_flags154,
	gtypes154,
	(uint16) 8448,
	cn_attr154,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_8_REF",
	names155,
	types155,
	attr_flags155,
	gtypes155,
	(uint16) 0,
	cn_attr155,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_8",
	names156,
	types156,
	attr_flags156,
	gtypes156,
	(uint16) 8962,
	cn_attr156,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_8",
	names157,
	types157,
	attr_flags157,
	gtypes157,
	(uint16) 8448,
	cn_attr157,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOOLEAN_REF",
	names158,
	types158,
	attr_flags158,
	gtypes158,
	(uint16) 0,
	cn_attr158,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOOLEAN",
	names159,
	types159,
	attr_flags159,
	gtypes159,
	(uint16) 8961,
	cn_attr159,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOOLEAN",
	names160,
	types160,
	attr_flags160,
	gtypes160,
	(uint16) 8448,
	cn_attr160,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"POINTER_REF",
	names161,
	types161,
	attr_flags161,
	gtypes161,
	(uint16) 0,
	cn_attr161,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"POINTER",
	names162,
	types162,
	attr_flags162,
	gtypes162,
	(uint16) 8965,
	cn_attr162,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"POINTER",
	names163,
	types163,
	attr_flags163,
	gtypes163,
	(uint16) 8448,
	cn_attr163,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 2,
	(long) 2,
	"READABLE_STRING_GENERAL",
	names164,
	types164,
	attr_flags164,
	gtypes164,
	(uint16) 4096,
	cn_attr164,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 2,
	(long) 2,
	"IMMUTABLE_STRING_GENERAL",
	names165,
	types165,
	attr_flags165,
	gtypes165,
	(uint16) 4096,
	cn_attr165,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 2,
	(long) 2,
	"STRING_GENERAL",
	names166,
	types166,
	attr_flags166,
	gtypes166,
	(uint16) 4096,
	cn_attr166,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"READABLE_STRING_32",
	names167,
	types167,
	attr_flags167,
	gtypes167,
	(uint16) 4096,
	cn_attr167,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"IMMUTABLE_STRING_32",
	names168,
	types168,
	attr_flags168,
	gtypes168,
	(uint16) 8192,
	cn_attr168,
	20,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"STRING_32",
	names169,
	types169,
	attr_flags169,
	gtypes169,
	(uint16) 0,
	cn_attr169,
	20,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"READABLE_STRING_8",
	names170,
	types170,
	attr_flags170,
	gtypes170,
	(uint16) 4096,
	cn_attr170,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"IMMUTABLE_STRING_8",
	names171,
	types171,
	attr_flags171,
	gtypes171,
	(uint16) 8192,
	cn_attr171,
	20,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"STRING_8",
	names172,
	types172,
	attr_flags172,
	gtypes172,
	(uint16) 0,
	cn_attr172,
	20,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"THREAD_ATTRIBUTES",
	names173,
	types173,
	attr_flags173,
	gtypes173,
	(uint16) 0,
	cn_attr173,
	12,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 21,
	(long) 20,
	"CONSOLE",
	names174,
	types174,
	attr_flags174,
	gtypes174,
	(uint16) 1024,
	cn_attr174,
	72,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"RT_DBG_INTERNAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 15,
	(long) 15,
	"RT_DBG_CALL_RECORD",
	names176,
	types176,
	attr_flags176,
	gtypes176,
	(uint16) 0,
	cn_attr176,
	48,
	8L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 11,
	(long) 11,
	"RT_DBG_EXECUTION_RECORDER",
	names177,
	types177,
	attr_flags177,
	gtypes177,
	(uint16) 0,
	cn_attr177,
	32,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"RT_DBG_VALUE_RECORD",
	names178,
	types178,
	attr_flags178,
	gtypes178,
	(uint16) 4096,
	cn_attr178,
	12,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 6,
	"INDEXABLE_ITERATION_CURSOR",
	names180,
	types180,
	attr_flags180,
	gtypes180,
	(uint16) 0,
	cn_attr180,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names184,
	types184,
	attr_flags184,
	gtypes184,
	(uint16) 0,
	cn_attr184,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names185,
	types185,
	attr_flags185,
	gtypes185,
	(uint16) 8965,
	cn_attr185,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names186,
	types186,
	attr_flags186,
	gtypes186,
	(uint16) 8448,
	cn_attr186,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names187,
	types187,
	attr_flags187,
	gtypes187,
	(uint16) 0,
	cn_attr187,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 12,
	(long) 12,
	"PROCEDURE",
	names188,
	types188,
	attr_flags188,
	gtypes188,
	(uint16) 0,
	cn_attr188,
	44,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 12,
	(long) 12,
	"ROUTINE",
	names189,
	types189,
	attr_flags189,
	gtypes189,
	(uint16) 4096,
	cn_attr189,
	44,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 13,
	(long) 13,
	"FUNCTION",
	names190,
	types190,
	attr_flags190,
	gtypes190,
	(uint16) 0,
	cn_attr190,
	44,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names191,
	types191,
	attr_flags191,
	gtypes191,
	(uint16) 0,
	cn_attr191,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names192,
	types192,
	attr_flags192,
	gtypes192,
	(uint16) 4096,
	cn_attr192,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names193,
	types193,
	attr_flags193,
	gtypes193,
	(uint16) 4096,
	cn_attr193,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names194,
	types194,
	attr_flags194,
	gtypes194,
	(uint16) 4096,
	cn_attr194,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names195,
	types195,
	attr_flags195,
	gtypes195,
	(uint16) 4096,
	cn_attr195,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names196,
	types196,
	attr_flags196,
	gtypes196,
	(uint16) 4096,
	cn_attr196,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names197,
	types197,
	attr_flags197,
	gtypes197,
	(uint16) 4096,
	cn_attr197,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names198,
	types198,
	attr_flags198,
	gtypes198,
	(uint16) 4096,
	cn_attr198,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names199,
	types199,
	attr_flags199,
	gtypes199,
	(uint16) 4096,
	cn_attr199,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names200,
	types200,
	attr_flags200,
	gtypes200,
	(uint16) 4096,
	cn_attr200,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names201,
	types201,
	attr_flags201,
	gtypes201,
	(uint16) 4096,
	cn_attr201,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names202,
	types202,
	attr_flags202,
	gtypes202,
	(uint16) 4096,
	cn_attr202,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names203,
	types203,
	attr_flags203,
	gtypes203,
	(uint16) 0,
	cn_attr203,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names205,
	types205,
	attr_flags205,
	gtypes205,
	(uint16) 0,
	cn_attr205,
	12,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names206,
	types206,
	attr_flags206,
	gtypes206,
	(uint16) 4096,
	cn_attr206,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names207,
	types207,
	attr_flags207,
	gtypes207,
	(uint16) 4096,
	cn_attr207,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names208,
	types208,
	attr_flags208,
	gtypes208,
	(uint16) 4096,
	cn_attr208,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names209,
	types209,
	attr_flags209,
	gtypes209,
	(uint16) 4096,
	cn_attr209,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names210,
	types210,
	attr_flags210,
	gtypes210,
	(uint16) 4096,
	cn_attr210,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names211,
	types211,
	attr_flags211,
	gtypes211,
	(uint16) 4096,
	cn_attr211,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names212,
	types212,
	attr_flags212,
	gtypes212,
	(uint16) 4096,
	cn_attr212,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names213,
	types213,
	attr_flags213,
	gtypes213,
	(uint16) 4096,
	cn_attr213,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names214,
	types214,
	attr_flags214,
	gtypes214,
	(uint16) 4096,
	cn_attr214,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CELL",
	names215,
	types215,
	attr_flags215,
	gtypes215,
	(uint16) 0,
	cn_attr215,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"LINKED_LIST",
	names217,
	types217,
	attr_flags217,
	gtypes217,
	(uint16) 0,
	cn_attr217,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 2,
	(long) 2,
	"LINKABLE",
	names218,
	types218,
	attr_flags218,
	gtypes218,
	(uint16) 0,
	cn_attr218,
	8,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"LINKED_LIST_CURSOR",
	names219,
	types219,
	attr_flags219,
	gtypes219,
	(uint16) 0,
	cn_attr219,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 8,
	(long) 7,
	"LINKED_LIST_ITERATION_CURSOR",
	names220,
	types220,
	attr_flags220,
	gtypes220,
	(uint16) 0,
	cn_attr220,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names221,
	types221,
	attr_flags221,
	gtypes221,
	(uint16) 4096,
	cn_attr221,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names222,
	types222,
	attr_flags222,
	gtypes222,
	(uint16) 4096,
	cn_attr222,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names223,
	types223,
	attr_flags223,
	gtypes223,
	(uint16) 4096,
	cn_attr223,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names224,
	types224,
	attr_flags224,
	gtypes224,
	(uint16) 4096,
	cn_attr224,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names225,
	types225,
	attr_flags225,
	gtypes225,
	(uint16) 4096,
	cn_attr225,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names226,
	types226,
	attr_flags226,
	gtypes226,
	(uint16) 4096,
	cn_attr226,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names227,
	types227,
	attr_flags227,
	gtypes227,
	(uint16) 4096,
	cn_attr227,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names228,
	types228,
	attr_flags228,
	gtypes228,
	(uint16) 4096,
	cn_attr228,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names229,
	types229,
	attr_flags229,
	gtypes229,
	(uint16) 4096,
	cn_attr229,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 6,
	"INDEXABLE_ITERATION_CURSOR",
	names230,
	types230,
	attr_flags230,
	gtypes230,
	(uint16) 0,
	cn_attr230,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 17,
	(long) 17,
	"HASH_TABLE",
	names234,
	types234,
	attr_flags234,
	gtypes234,
	(uint16) 0,
	cn_attr234,
	60,
	7L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 13,
	(long) 13,
	"PREDICATE",
	names237,
	types237,
	attr_flags237,
	gtypes237,
	(uint16) 0,
	cn_attr237,
	44,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names238,
	types238,
	attr_flags238,
	gtypes238,
	(uint16) 4096,
	cn_attr238,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 6,
	"HASH_TABLE_ITERATION_CURSOR",
	names239,
	types239,
	attr_flags239,
	gtypes239,
	(uint16) 0,
	cn_attr239,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names240,
	types240,
	attr_flags240,
	gtypes240,
	(uint16) 0,
	cn_attr240,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names241,
	types241,
	attr_flags241,
	gtypes241,
	(uint16) 0,
	cn_attr241,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 17,
	(long) 17,
	"HASH_TABLE",
	names242,
	types242,
	attr_flags242,
	gtypes242,
	(uint16) 0,
	cn_attr242,
	60,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names245,
	types245,
	attr_flags245,
	gtypes245,
	(uint16) 0,
	cn_attr245,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names246,
	types246,
	attr_flags246,
	gtypes246,
	(uint16) 4096,
	cn_attr246,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names247,
	types247,
	attr_flags247,
	gtypes247,
	(uint16) 4096,
	cn_attr247,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names248,
	types248,
	attr_flags248,
	gtypes248,
	(uint16) 4096,
	cn_attr248,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names250,
	types250,
	attr_flags250,
	gtypes250,
	(uint16) 0,
	cn_attr250,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names251,
	types251,
	attr_flags251,
	gtypes251,
	(uint16) 4096,
	cn_attr251,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names252,
	types252,
	attr_flags252,
	gtypes252,
	(uint16) 4096,
	cn_attr252,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names253,
	types253,
	attr_flags253,
	gtypes253,
	(uint16) 0,
	cn_attr253,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names255,
	types255,
	attr_flags255,
	gtypes255,
	(uint16) 0,
	cn_attr255,
	12,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names256,
	types256,
	attr_flags256,
	gtypes256,
	(uint16) 4096,
	cn_attr256,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names257,
	types257,
	attr_flags257,
	gtypes257,
	(uint16) 4096,
	cn_attr257,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names258,
	types258,
	attr_flags258,
	gtypes258,
	(uint16) 4096,
	cn_attr258,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names259,
	types259,
	attr_flags259,
	gtypes259,
	(uint16) 4096,
	cn_attr259,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names260,
	types260,
	attr_flags260,
	gtypes260,
	(uint16) 4096,
	cn_attr260,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names261,
	types261,
	attr_flags261,
	gtypes261,
	(uint16) 4096,
	cn_attr261,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 6,
	"HASH_TABLE_ITERATION_CURSOR",
	names262,
	types262,
	attr_flags262,
	gtypes262,
	(uint16) 0,
	cn_attr262,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names263,
	types263,
	attr_flags263,
	gtypes263,
	(uint16) 4096,
	cn_attr263,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names264,
	types264,
	attr_flags264,
	gtypes264,
	(uint16) 4096,
	cn_attr264,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names265,
	types265,
	attr_flags265,
	gtypes265,
	(uint16) 4096,
	cn_attr265,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names266,
	types266,
	attr_flags266,
	gtypes266,
	(uint16) 4096,
	cn_attr266,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names267,
	types267,
	attr_flags267,
	gtypes267,
	(uint16) 4096,
	cn_attr267,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names268,
	types268,
	attr_flags268,
	gtypes268,
	(uint16) 4096,
	cn_attr268,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names269,
	types269,
	attr_flags269,
	gtypes269,
	(uint16) 4096,
	cn_attr269,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names270,
	types270,
	attr_flags270,
	gtypes270,
	(uint16) 0,
	cn_attr270,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 18,
	(long) 18,
	"STRING_TABLE",
	names271,
	types271,
	attr_flags271,
	gtypes271,
	(uint16) 0,
	cn_attr271,
	60,
	7L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names272,
	types272,
	attr_flags272,
	gtypes272,
	(uint16) 0,
	cn_attr272,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names273,
	types273,
	attr_flags273,
	gtypes273,
	(uint16) 0,
	cn_attr273,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names274,
	types274,
	attr_flags274,
	gtypes274,
	(uint16) 0,
	cn_attr274,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 6,
	"INDEXABLE_ITERATION_CURSOR",
	names275,
	types275,
	attr_flags275,
	gtypes275,
	(uint16) 0,
	cn_attr275,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names279,
	types279,
	attr_flags279,
	gtypes279,
	(uint16) 4096,
	cn_attr279,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names280,
	types280,
	attr_flags280,
	gtypes280,
	(uint16) 4096,
	cn_attr280,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names281,
	types281,
	attr_flags281,
	gtypes281,
	(uint16) 4096,
	cn_attr281,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names282,
	types282,
	attr_flags282,
	gtypes282,
	(uint16) 4096,
	cn_attr282,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names283,
	types283,
	attr_flags283,
	gtypes283,
	(uint16) 4096,
	cn_attr283,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names284,
	types284,
	attr_flags284,
	gtypes284,
	(uint16) 4096,
	cn_attr284,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names285,
	types285,
	attr_flags285,
	gtypes285,
	(uint16) 4096,
	cn_attr285,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names286,
	types286,
	attr_flags286,
	gtypes286,
	(uint16) 4096,
	cn_attr286,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names287,
	types287,
	attr_flags287,
	gtypes287,
	(uint16) 4096,
	cn_attr287,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names288,
	types288,
	attr_flags288,
	gtypes288,
	(uint16) 4096,
	cn_attr288,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names289,
	types289,
	attr_flags289,
	gtypes289,
	(uint16) 4096,
	cn_attr289,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names290,
	types290,
	attr_flags290,
	gtypes290,
	(uint16) 0,
	cn_attr290,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names292,
	types292,
	attr_flags292,
	gtypes292,
	(uint16) 0,
	cn_attr292,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names294,
	types294,
	attr_flags294,
	gtypes294,
	(uint16) 0,
	cn_attr294,
	12,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names295,
	types295,
	attr_flags295,
	gtypes295,
	(uint16) 4096,
	cn_attr295,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names296,
	types296,
	attr_flags296,
	gtypes296,
	(uint16) 4096,
	cn_attr296,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names297,
	types297,
	attr_flags297,
	gtypes297,
	(uint16) 4096,
	cn_attr297,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names298,
	types298,
	attr_flags298,
	gtypes298,
	(uint16) 4096,
	cn_attr298,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names299,
	types299,
	attr_flags299,
	gtypes299,
	(uint16) 4096,
	cn_attr299,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names300,
	types300,
	attr_flags300,
	gtypes300,
	(uint16) 4096,
	cn_attr300,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names301,
	types301,
	attr_flags301,
	gtypes301,
	(uint16) 4096,
	cn_attr301,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names302,
	types302,
	attr_flags302,
	gtypes302,
	(uint16) 4096,
	cn_attr302,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names303,
	types303,
	attr_flags303,
	gtypes303,
	(uint16) 4096,
	cn_attr303,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names304,
	types304,
	attr_flags304,
	gtypes304,
	(uint16) 0,
	cn_attr304,
	24,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 6,
	"INDEXABLE_ITERATION_CURSOR",
	names306,
	types306,
	attr_flags306,
	gtypes306,
	(uint16) 0,
	cn_attr306,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names310,
	types310,
	attr_flags310,
	gtypes310,
	(uint16) 0,
	cn_attr310,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names311,
	types311,
	attr_flags311,
	gtypes311,
	(uint16) 0,
	cn_attr311,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names312,
	types312,
	attr_flags312,
	gtypes312,
	(uint16) 4096,
	cn_attr312,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names313,
	types313,
	attr_flags313,
	gtypes313,
	(uint16) 4096,
	cn_attr313,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names314,
	types314,
	attr_flags314,
	gtypes314,
	(uint16) 4096,
	cn_attr314,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names315,
	types315,
	attr_flags315,
	gtypes315,
	(uint16) 4096,
	cn_attr315,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names316,
	types316,
	attr_flags316,
	gtypes316,
	(uint16) 4096,
	cn_attr316,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names317,
	types317,
	attr_flags317,
	gtypes317,
	(uint16) 4096,
	cn_attr317,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names318,
	types318,
	attr_flags318,
	gtypes318,
	(uint16) 4096,
	cn_attr318,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names319,
	types319,
	attr_flags319,
	gtypes319,
	(uint16) 4096,
	cn_attr319,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names320,
	types320,
	attr_flags320,
	gtypes320,
	(uint16) 4096,
	cn_attr320,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names321,
	types321,
	attr_flags321,
	gtypes321,
	(uint16) 4096,
	cn_attr321,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names322,
	types322,
	attr_flags322,
	gtypes322,
	(uint16) 4096,
	cn_attr322,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names323,
	types323,
	attr_flags323,
	gtypes323,
	(uint16) 0,
	cn_attr323,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names325,
	types325,
	attr_flags325,
	gtypes325,
	(uint16) 0,
	cn_attr325,
	12,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names326,
	types326,
	attr_flags326,
	gtypes326,
	(uint16) 4096,
	cn_attr326,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names327,
	types327,
	attr_flags327,
	gtypes327,
	(uint16) 4096,
	cn_attr327,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names328,
	types328,
	attr_flags328,
	gtypes328,
	(uint16) 4096,
	cn_attr328,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names329,
	types329,
	attr_flags329,
	gtypes329,
	(uint16) 4096,
	cn_attr329,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names330,
	types330,
	attr_flags330,
	gtypes330,
	(uint16) 4096,
	cn_attr330,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names331,
	types331,
	attr_flags331,
	gtypes331,
	(uint16) 4096,
	cn_attr331,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names332,
	types332,
	attr_flags332,
	gtypes332,
	(uint16) 4096,
	cn_attr332,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names333,
	types333,
	attr_flags333,
	gtypes333,
	(uint16) 4096,
	cn_attr333,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names334,
	types334,
	attr_flags334,
	gtypes334,
	(uint16) 4096,
	cn_attr334,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_QUEUE",
	names335,
	types335,
	attr_flags335,
	gtypes335,
	(uint16) 0,
	cn_attr335,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"QUEUE",
	names336,
	types336,
	attr_flags336,
	gtypes336,
	(uint16) 4096,
	cn_attr336,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DISPENSER",
	names337,
	types337,
	attr_flags337,
	gtypes337,
	(uint16) 4096,
	cn_attr337,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names338,
	types338,
	attr_flags338,
	gtypes338,
	(uint16) 0,
	cn_attr338,
	20,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names339,
	types339,
	attr_flags339,
	gtypes339,
	(uint16) 0,
	cn_attr339,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names340,
	types340,
	attr_flags340,
	gtypes340,
	(uint16) 0,
	cn_attr340,
	20,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names341,
	types341,
	attr_flags341,
	gtypes341,
	(uint16) 8965,
	cn_attr341,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names342,
	types342,
	attr_flags342,
	gtypes342,
	(uint16) 8448,
	cn_attr342,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names343,
	types343,
	attr_flags343,
	gtypes343,
	(uint16) 0,
	cn_attr343,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 6,
	"INDEXABLE_ITERATION_CURSOR",
	names344,
	types344,
	attr_flags344,
	gtypes344,
	(uint16) 0,
	cn_attr344,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 6,
	"INDEXABLE_ITERATION_CURSOR",
	names348,
	types348,
	attr_flags348,
	gtypes348,
	(uint16) 0,
	cn_attr348,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names351,
	types351,
	attr_flags351,
	gtypes351,
	(uint16) 4096,
	cn_attr351,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names352,
	types352,
	attr_flags352,
	gtypes352,
	(uint16) 4096,
	cn_attr352,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names353,
	types353,
	attr_flags353,
	gtypes353,
	(uint16) 4096,
	cn_attr353,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names354,
	types354,
	attr_flags354,
	gtypes354,
	(uint16) 4096,
	cn_attr354,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names355,
	types355,
	attr_flags355,
	gtypes355,
	(uint16) 4096,
	cn_attr355,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names356,
	types356,
	attr_flags356,
	gtypes356,
	(uint16) 4096,
	cn_attr356,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names357,
	types357,
	attr_flags357,
	gtypes357,
	(uint16) 4096,
	cn_attr357,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names358,
	types358,
	attr_flags358,
	gtypes358,
	(uint16) 4096,
	cn_attr358,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names359,
	types359,
	attr_flags359,
	gtypes359,
	(uint16) 4096,
	cn_attr359,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names360,
	types360,
	attr_flags360,
	gtypes360,
	(uint16) 4096,
	cn_attr360,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names361,
	types361,
	attr_flags361,
	gtypes361,
	(uint16) 4096,
	cn_attr361,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names362,
	types362,
	attr_flags362,
	gtypes362,
	(uint16) 0,
	cn_attr362,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names363,
	types363,
	attr_flags363,
	gtypes363,
	(uint16) 0,
	cn_attr363,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names364,
	types364,
	attr_flags364,
	gtypes364,
	(uint16) 0,
	cn_attr364,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names365,
	types365,
	attr_flags365,
	gtypes365,
	(uint16) 8965,
	cn_attr365,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names366,
	types366,
	attr_flags366,
	gtypes366,
	(uint16) 8448,
	cn_attr366,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names367,
	types367,
	attr_flags367,
	gtypes367,
	(uint16) 0,
	cn_attr367,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 6,
	"INDEXABLE_ITERATION_CURSOR",
	names369,
	types369,
	attr_flags369,
	gtypes369,
	(uint16) 0,
	cn_attr369,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names373,
	types373,
	attr_flags373,
	gtypes373,
	(uint16) 0,
	cn_attr373,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names374,
	types374,
	attr_flags374,
	gtypes374,
	(uint16) 4096,
	cn_attr374,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names375,
	types375,
	attr_flags375,
	gtypes375,
	(uint16) 4096,
	cn_attr375,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names376,
	types376,
	attr_flags376,
	gtypes376,
	(uint16) 4096,
	cn_attr376,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names377,
	types377,
	attr_flags377,
	gtypes377,
	(uint16) 4096,
	cn_attr377,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names378,
	types378,
	attr_flags378,
	gtypes378,
	(uint16) 4096,
	cn_attr378,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names379,
	types379,
	attr_flags379,
	gtypes379,
	(uint16) 4096,
	cn_attr379,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names380,
	types380,
	attr_flags380,
	gtypes380,
	(uint16) 4096,
	cn_attr380,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names381,
	types381,
	attr_flags381,
	gtypes381,
	(uint16) 4096,
	cn_attr381,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names382,
	types382,
	attr_flags382,
	gtypes382,
	(uint16) 4096,
	cn_attr382,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names383,
	types383,
	attr_flags383,
	gtypes383,
	(uint16) 4096,
	cn_attr383,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names384,
	types384,
	attr_flags384,
	gtypes384,
	(uint16) 4096,
	cn_attr384,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names385,
	types385,
	attr_flags385,
	gtypes385,
	(uint16) 0,
	cn_attr385,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names387,
	types387,
	attr_flags387,
	gtypes387,
	(uint16) 0,
	cn_attr387,
	12,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names388,
	types388,
	attr_flags388,
	gtypes388,
	(uint16) 4096,
	cn_attr388,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names389,
	types389,
	attr_flags389,
	gtypes389,
	(uint16) 4096,
	cn_attr389,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names390,
	types390,
	attr_flags390,
	gtypes390,
	(uint16) 4096,
	cn_attr390,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names391,
	types391,
	attr_flags391,
	gtypes391,
	(uint16) 4096,
	cn_attr391,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names392,
	types392,
	attr_flags392,
	gtypes392,
	(uint16) 4096,
	cn_attr392,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names393,
	types393,
	attr_flags393,
	gtypes393,
	(uint16) 4096,
	cn_attr393,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names394,
	types394,
	attr_flags394,
	gtypes394,
	(uint16) 4096,
	cn_attr394,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names395,
	types395,
	attr_flags395,
	gtypes395,
	(uint16) 4096,
	cn_attr395,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names396,
	types396,
	attr_flags396,
	gtypes396,
	(uint16) 4096,
	cn_attr396,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names397,
	types397,
	attr_flags397,
	gtypes397,
	(uint16) 0,
	cn_attr397,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CELL",
	names398,
	types398,
	attr_flags398,
	gtypes398,
	(uint16) 0,
	cn_attr398,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names399,
	types399,
	attr_flags399,
	gtypes399,
	(uint16) 0,
	cn_attr399,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names400,
	types400,
	attr_flags400,
	gtypes400,
	(uint16) 8965,
	cn_attr400,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names401,
	types401,
	attr_flags401,
	gtypes401,
	(uint16) 8448,
	cn_attr401,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names402,
	types402,
	attr_flags402,
	gtypes402,
	(uint16) 0,
	cn_attr402,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names403,
	types403,
	attr_flags403,
	gtypes403,
	(uint16) 0,
	cn_attr403,
	20,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names404,
	types404,
	attr_flags404,
	gtypes404,
	(uint16) 0,
	cn_attr404,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names405,
	types405,
	attr_flags405,
	gtypes405,
	(uint16) 0,
	cn_attr405,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names406,
	types406,
	attr_flags406,
	gtypes406,
	(uint16) 0,
	cn_attr406,
	28,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names407,
	types407,
	attr_flags407,
	gtypes407,
	(uint16) 8965,
	cn_attr407,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names408,
	types408,
	attr_flags408,
	gtypes408,
	(uint16) 8448,
	cn_attr408,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names409,
	types409,
	attr_flags409,
	gtypes409,
	(uint16) 0,
	cn_attr409,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 6,
	"INDEXABLE_ITERATION_CURSOR",
	names411,
	types411,
	attr_flags411,
	gtypes411,
	(uint16) 0,
	cn_attr411,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names415,
	types415,
	attr_flags415,
	gtypes415,
	(uint16) 0,
	cn_attr415,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names416,
	types416,
	attr_flags416,
	gtypes416,
	(uint16) 4096,
	cn_attr416,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names417,
	types417,
	attr_flags417,
	gtypes417,
	(uint16) 4096,
	cn_attr417,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names418,
	types418,
	attr_flags418,
	gtypes418,
	(uint16) 4096,
	cn_attr418,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names419,
	types419,
	attr_flags419,
	gtypes419,
	(uint16) 4096,
	cn_attr419,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names420,
	types420,
	attr_flags420,
	gtypes420,
	(uint16) 4096,
	cn_attr420,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names421,
	types421,
	attr_flags421,
	gtypes421,
	(uint16) 4096,
	cn_attr421,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names422,
	types422,
	attr_flags422,
	gtypes422,
	(uint16) 4096,
	cn_attr422,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names423,
	types423,
	attr_flags423,
	gtypes423,
	(uint16) 4096,
	cn_attr423,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names424,
	types424,
	attr_flags424,
	gtypes424,
	(uint16) 4096,
	cn_attr424,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names425,
	types425,
	attr_flags425,
	gtypes425,
	(uint16) 4096,
	cn_attr425,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names426,
	types426,
	attr_flags426,
	gtypes426,
	(uint16) 4096,
	cn_attr426,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names427,
	types427,
	attr_flags427,
	gtypes427,
	(uint16) 0,
	cn_attr427,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names429,
	types429,
	attr_flags429,
	gtypes429,
	(uint16) 0,
	cn_attr429,
	12,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names430,
	types430,
	attr_flags430,
	gtypes430,
	(uint16) 4096,
	cn_attr430,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names431,
	types431,
	attr_flags431,
	gtypes431,
	(uint16) 4096,
	cn_attr431,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names432,
	types432,
	attr_flags432,
	gtypes432,
	(uint16) 4096,
	cn_attr432,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names433,
	types433,
	attr_flags433,
	gtypes433,
	(uint16) 4096,
	cn_attr433,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names434,
	types434,
	attr_flags434,
	gtypes434,
	(uint16) 4096,
	cn_attr434,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names435,
	types435,
	attr_flags435,
	gtypes435,
	(uint16) 4096,
	cn_attr435,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names436,
	types436,
	attr_flags436,
	gtypes436,
	(uint16) 4096,
	cn_attr436,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names437,
	types437,
	attr_flags437,
	gtypes437,
	(uint16) 4096,
	cn_attr437,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names438,
	types438,
	attr_flags438,
	gtypes438,
	(uint16) 4096,
	cn_attr438,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names439,
	types439,
	attr_flags439,
	gtypes439,
	(uint16) 4096,
	cn_attr439,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names440,
	types440,
	attr_flags440,
	gtypes440,
	(uint16) 8965,
	cn_attr440,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names441,
	types441,
	attr_flags441,
	gtypes441,
	(uint16) 8448,
	cn_attr441,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names442,
	types442,
	attr_flags442,
	gtypes442,
	(uint16) 0,
	cn_attr442,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names443,
	types443,
	attr_flags443,
	gtypes443,
	(uint16) 0,
	cn_attr443,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names444,
	types444,
	attr_flags444,
	gtypes444,
	(uint16) 8965,
	cn_attr444,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names445,
	types445,
	attr_flags445,
	gtypes445,
	(uint16) 8448,
	cn_attr445,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names446,
	types446,
	attr_flags446,
	gtypes446,
	(uint16) 0,
	cn_attr446,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names447,
	types447,
	attr_flags447,
	gtypes447,
	(uint16) 0,
	cn_attr447,
	20,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names448,
	types448,
	attr_flags448,
	gtypes448,
	(uint16) 0,
	cn_attr448,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names449,
	types449,
	attr_flags449,
	gtypes449,
	(uint16) 0,
	cn_attr449,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names450,
	types450,
	attr_flags450,
	gtypes450,
	(uint16) 0,
	cn_attr450,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names451,
	types451,
	attr_flags451,
	gtypes451,
	(uint16) 0,
	cn_attr451,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names452,
	types452,
	attr_flags452,
	gtypes452,
	(uint16) 8965,
	cn_attr452,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names453,
	types453,
	attr_flags453,
	gtypes453,
	(uint16) 8448,
	cn_attr453,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names454,
	types454,
	attr_flags454,
	gtypes454,
	(uint16) 0,
	cn_attr454,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names455,
	types455,
	attr_flags455,
	gtypes455,
	(uint16) 0,
	cn_attr455,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names456,
	types456,
	attr_flags456,
	gtypes456,
	(uint16) 0,
	cn_attr456,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 6,
	"INDEXABLE_ITERATION_CURSOR",
	names458,
	types458,
	attr_flags458,
	gtypes458,
	(uint16) 0,
	cn_attr458,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names462,
	types462,
	attr_flags462,
	gtypes462,
	(uint16) 0,
	cn_attr462,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names463,
	types463,
	attr_flags463,
	gtypes463,
	(uint16) 4096,
	cn_attr463,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names464,
	types464,
	attr_flags464,
	gtypes464,
	(uint16) 4096,
	cn_attr464,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names465,
	types465,
	attr_flags465,
	gtypes465,
	(uint16) 4096,
	cn_attr465,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names466,
	types466,
	attr_flags466,
	gtypes466,
	(uint16) 4096,
	cn_attr466,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names467,
	types467,
	attr_flags467,
	gtypes467,
	(uint16) 4096,
	cn_attr467,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names468,
	types468,
	attr_flags468,
	gtypes468,
	(uint16) 4096,
	cn_attr468,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names469,
	types469,
	attr_flags469,
	gtypes469,
	(uint16) 4096,
	cn_attr469,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names470,
	types470,
	attr_flags470,
	gtypes470,
	(uint16) 4096,
	cn_attr470,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names471,
	types471,
	attr_flags471,
	gtypes471,
	(uint16) 4096,
	cn_attr471,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names472,
	types472,
	attr_flags472,
	gtypes472,
	(uint16) 4096,
	cn_attr472,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names473,
	types473,
	attr_flags473,
	gtypes473,
	(uint16) 4096,
	cn_attr473,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names474,
	types474,
	attr_flags474,
	gtypes474,
	(uint16) 0,
	cn_attr474,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names476,
	types476,
	attr_flags476,
	gtypes476,
	(uint16) 0,
	cn_attr476,
	12,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names477,
	types477,
	attr_flags477,
	gtypes477,
	(uint16) 4096,
	cn_attr477,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names478,
	types478,
	attr_flags478,
	gtypes478,
	(uint16) 4096,
	cn_attr478,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names479,
	types479,
	attr_flags479,
	gtypes479,
	(uint16) 4096,
	cn_attr479,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names480,
	types480,
	attr_flags480,
	gtypes480,
	(uint16) 4096,
	cn_attr480,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names481,
	types481,
	attr_flags481,
	gtypes481,
	(uint16) 4096,
	cn_attr481,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names482,
	types482,
	attr_flags482,
	gtypes482,
	(uint16) 4096,
	cn_attr482,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names483,
	types483,
	attr_flags483,
	gtypes483,
	(uint16) 4096,
	cn_attr483,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names484,
	types484,
	attr_flags484,
	gtypes484,
	(uint16) 4096,
	cn_attr484,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names485,
	types485,
	attr_flags485,
	gtypes485,
	(uint16) 4096,
	cn_attr485,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names486,
	types486,
	attr_flags486,
	gtypes486,
	(uint16) 0,
	cn_attr486,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names487,
	types487,
	attr_flags487,
	gtypes487,
	(uint16) 0,
	cn_attr487,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names488,
	types488,
	attr_flags488,
	gtypes488,
	(uint16) 8965,
	cn_attr488,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names489,
	types489,
	attr_flags489,
	gtypes489,
	(uint16) 8448,
	cn_attr489,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names490,
	types490,
	attr_flags490,
	gtypes490,
	(uint16) 0,
	cn_attr490,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names491,
	types491,
	attr_flags491,
	gtypes491,
	(uint16) 0,
	cn_attr491,
	20,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names492,
	types492,
	attr_flags492,
	gtypes492,
	(uint16) 8965,
	cn_attr492,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names493,
	types493,
	attr_flags493,
	gtypes493,
	(uint16) 8448,
	cn_attr493,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names494,
	types494,
	attr_flags494,
	gtypes494,
	(uint16) 0,
	cn_attr494,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names495,
	types495,
	attr_flags495,
	gtypes495,
	(uint16) 0,
	cn_attr495,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names496,
	types496,
	attr_flags496,
	gtypes496,
	(uint16) 0,
	cn_attr496,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names497,
	types497,
	attr_flags497,
	gtypes497,
	(uint16) 0,
	cn_attr497,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names498,
	types498,
	attr_flags498,
	gtypes498,
	(uint16) 0,
	cn_attr498,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names499,
	types499,
	attr_flags499,
	gtypes499,
	(uint16) 0,
	cn_attr499,
	28,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names500,
	types500,
	attr_flags500,
	gtypes500,
	(uint16) 8965,
	cn_attr500,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names501,
	types501,
	attr_flags501,
	gtypes501,
	(uint16) 8448,
	cn_attr501,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names502,
	types502,
	attr_flags502,
	gtypes502,
	(uint16) 0,
	cn_attr502,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names503,
	types503,
	attr_flags503,
	gtypes503,
	(uint16) 0,
	cn_attr503,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names504,
	types504,
	attr_flags504,
	gtypes504,
	(uint16) 0,
	cn_attr504,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names505,
	types505,
	attr_flags505,
	gtypes505,
	(uint16) 0,
	cn_attr505,
	20,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names506,
	types506,
	attr_flags506,
	gtypes506,
	(uint16) 8965,
	cn_attr506,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names507,
	types507,
	attr_flags507,
	gtypes507,
	(uint16) 8448,
	cn_attr507,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names508,
	types508,
	attr_flags508,
	gtypes508,
	(uint16) 0,
	cn_attr508,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CELL",
	names509,
	types509,
	attr_flags509,
	gtypes509,
	(uint16) 0,
	cn_attr509,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names510,
	types510,
	attr_flags510,
	gtypes510,
	(uint16) 4096,
	cn_attr510,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names511,
	types511,
	attr_flags511,
	gtypes511,
	(uint16) 4096,
	cn_attr511,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names512,
	types512,
	attr_flags512,
	gtypes512,
	(uint16) 0,
	cn_attr512,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names513,
	types513,
	attr_flags513,
	gtypes513,
	(uint16) 0,
	cn_attr513,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names514,
	types514,
	attr_flags514,
	gtypes514,
	(uint16) 0,
	cn_attr514,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 17,
	(long) 17,
	"HASH_TABLE",
	names515,
	types515,
	attr_flags515,
	gtypes515,
	(uint16) 0,
	cn_attr515,
	60,
	6L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 6,
	"HASH_TABLE_ITERATION_CURSOR",
	names518,
	types518,
	attr_flags518,
	gtypes518,
	(uint16) 0,
	cn_attr518,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"LINKED_LIST",
	names519,
	types519,
	attr_flags519,
	gtypes519,
	(uint16) 0,
	cn_attr519,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 2,
	(long) 2,
	"LINKABLE",
	names520,
	types520,
	attr_flags520,
	gtypes520,
	(uint16) 0,
	cn_attr520,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"LINKED_LIST_CURSOR",
	names521,
	types521,
	attr_flags521,
	gtypes521,
	(uint16) 0,
	cn_attr521,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 8,
	(long) 7,
	"LINKED_LIST_ITERATION_CURSOR",
	names522,
	types522,
	attr_flags522,
	gtypes522,
	(uint16) 0,
	cn_attr522,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names523,
	types523,
	attr_flags523,
	gtypes523,
	(uint16) 0,
	cn_attr523,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names524,
	types524,
	attr_flags524,
	gtypes524,
	(uint16) 0,
	cn_attr524,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names525,
	types525,
	attr_flags525,
	gtypes525,
	(uint16) 0,
	cn_attr525,
	20,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names526,
	types526,
	attr_flags526,
	gtypes526,
	(uint16) 0,
	cn_attr526,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 13,
	(long) 13,
	"FUNCTION",
	names527,
	types527,
	attr_flags527,
	gtypes527,
	(uint16) 0,
	cn_attr527,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names528,
	types528,
	attr_flags528,
	gtypes528,
	(uint16) 0,
	cn_attr528,
	12,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names529,
	types529,
	attr_flags529,
	gtypes529,
	(uint16) 4096,
	cn_attr529,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names530,
	types530,
	attr_flags530,
	gtypes530,
	(uint16) 4096,
	cn_attr530,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names531,
	types531,
	attr_flags531,
	gtypes531,
	(uint16) 0,
	cn_attr531,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names533,
	types533,
	attr_flags533,
	gtypes533,
	(uint16) 0,
	cn_attr533,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names534,
	types534,
	attr_flags534,
	gtypes534,
	(uint16) 0,
	cn_attr534,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names536,
	types536,
	attr_flags536,
	gtypes536,
	(uint16) 4096,
	cn_attr536,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names537,
	types537,
	attr_flags537,
	gtypes537,
	(uint16) 4096,
	cn_attr537,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names538,
	types538,
	attr_flags538,
	gtypes538,
	(uint16) 4096,
	cn_attr538,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names539,
	types539,
	attr_flags539,
	gtypes539,
	(uint16) 4096,
	cn_attr539,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names540,
	types540,
	attr_flags540,
	gtypes540,
	(uint16) 0,
	cn_attr540,
	20,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names541,
	types541,
	attr_flags541,
	gtypes541,
	(uint16) 8965,
	cn_attr541,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names542,
	types542,
	attr_flags542,
	gtypes542,
	(uint16) 8448,
	cn_attr542,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names543,
	types543,
	attr_flags543,
	gtypes543,
	(uint16) 0,
	cn_attr543,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names544,
	types544,
	attr_flags544,
	gtypes544,
	(uint16) 0,
	cn_attr544,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names545,
	types545,
	attr_flags545,
	gtypes545,
	(uint16) 0,
	cn_attr545,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names546,
	types546,
	attr_flags546,
	gtypes546,
	(uint16) 0,
	cn_attr546,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names547,
	types547,
	attr_flags547,
	gtypes547,
	(uint16) 0,
	cn_attr547,
	20,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names548,
	types548,
	attr_flags548,
	gtypes548,
	(uint16) 0,
	cn_attr548,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names549,
	types549,
	attr_flags549,
	gtypes549,
	(uint16) 0,
	cn_attr549,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 6,
	"INDEXABLE_ITERATION_CURSOR",
	names550,
	types550,
	attr_flags550,
	gtypes550,
	(uint16) 0,
	cn_attr550,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names554,
	types554,
	attr_flags554,
	gtypes554,
	(uint16) 4096,
	cn_attr554,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names555,
	types555,
	attr_flags555,
	gtypes555,
	(uint16) 4096,
	cn_attr555,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names556,
	types556,
	attr_flags556,
	gtypes556,
	(uint16) 4096,
	cn_attr556,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names557,
	types557,
	attr_flags557,
	gtypes557,
	(uint16) 4096,
	cn_attr557,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names558,
	types558,
	attr_flags558,
	gtypes558,
	(uint16) 4096,
	cn_attr558,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names559,
	types559,
	attr_flags559,
	gtypes559,
	(uint16) 4096,
	cn_attr559,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names560,
	types560,
	attr_flags560,
	gtypes560,
	(uint16) 4096,
	cn_attr560,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names561,
	types561,
	attr_flags561,
	gtypes561,
	(uint16) 4096,
	cn_attr561,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names562,
	types562,
	attr_flags562,
	gtypes562,
	(uint16) 4096,
	cn_attr562,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names563,
	types563,
	attr_flags563,
	gtypes563,
	(uint16) 4096,
	cn_attr563,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names564,
	types564,
	attr_flags564,
	gtypes564,
	(uint16) 4096,
	cn_attr564,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names565,
	types565,
	attr_flags565,
	gtypes565,
	(uint16) 0,
	cn_attr565,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names568,
	types568,
	attr_flags568,
	gtypes568,
	(uint16) 0,
	cn_attr568,
	12,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names569,
	types569,
	attr_flags569,
	gtypes569,
	(uint16) 4096,
	cn_attr569,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names570,
	types570,
	attr_flags570,
	gtypes570,
	(uint16) 4096,
	cn_attr570,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names571,
	types571,
	attr_flags571,
	gtypes571,
	(uint16) 4096,
	cn_attr571,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names572,
	types572,
	attr_flags572,
	gtypes572,
	(uint16) 4096,
	cn_attr572,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names573,
	types573,
	attr_flags573,
	gtypes573,
	(uint16) 4096,
	cn_attr573,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names574,
	types574,
	attr_flags574,
	gtypes574,
	(uint16) 4096,
	cn_attr574,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names575,
	types575,
	attr_flags575,
	gtypes575,
	(uint16) 4096,
	cn_attr575,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names576,
	types576,
	attr_flags576,
	gtypes576,
	(uint16) 4096,
	cn_attr576,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names577,
	types577,
	attr_flags577,
	gtypes577,
	(uint16) 4096,
	cn_attr577,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"SET",
	names578,
	types578,
	attr_flags578,
	gtypes578,
	(uint16) 4096,
	cn_attr578,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names579,
	types579,
	attr_flags579,
	gtypes579,
	(uint16) 0,
	cn_attr579,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 2,
	(long) 2,
	"COUNTABLE_SEQUENCE",
	names580,
	types580,
	attr_flags580,
	gtypes580,
	(uint16) 4096,
	cn_attr580,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"COUNTABLE",
	names581,
	types581,
	attr_flags581,
	gtypes581,
	(uint16) 4096,
	cn_attr581,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INFINITE",
	names582,
	types582,
	attr_flags582,
	gtypes582,
	(uint16) 4096,
	cn_attr582,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 18,
	(long) 18,
	"STRING_TABLE",
	names583,
	types583,
	attr_flags583,
	gtypes583,
	(uint16) 0,
	cn_attr583,
	60,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 6,
	"HASH_TABLE_ITERATION_CURSOR",
	names584,
	types584,
	attr_flags584,
	gtypes584,
	(uint16) 0,
	cn_attr584,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 17,
	(long) 17,
	"HASH_TABLE",
	names586,
	types586,
	attr_flags586,
	gtypes586,
	(uint16) 0,
	cn_attr586,
	60,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names588,
	types588,
	attr_flags588,
	gtypes588,
	(uint16) 4096,
	cn_attr588,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names589,
	types589,
	attr_flags589,
	gtypes589,
	(uint16) 0,
	cn_attr589,
	20,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names590,
	types590,
	attr_flags590,
	gtypes590,
	(uint16) 4096,
	cn_attr590,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names591,
	types591,
	attr_flags591,
	gtypes591,
	(uint16) 4096,
	cn_attr591,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names592,
	types592,
	attr_flags592,
	gtypes592,
	(uint16) 0,
	cn_attr592,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names593,
	types593,
	attr_flags593,
	gtypes593,
	(uint16) 0,
	cn_attr593,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names594,
	types594,
	attr_flags594,
	gtypes594,
	(uint16) 0,
	cn_attr594,
	28,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names595,
	types595,
	attr_flags595,
	gtypes595,
	(uint16) 8965,
	cn_attr595,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names596,
	types596,
	attr_flags596,
	gtypes596,
	(uint16) 8448,
	cn_attr596,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names597,
	types597,
	attr_flags597,
	gtypes597,
	(uint16) 0,
	cn_attr597,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 6,
	"INDEXABLE_ITERATION_CURSOR",
	names599,
	types599,
	attr_flags599,
	gtypes599,
	(uint16) 0,
	cn_attr599,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names603,
	types603,
	attr_flags603,
	gtypes603,
	(uint16) 0,
	cn_attr603,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names604,
	types604,
	attr_flags604,
	gtypes604,
	(uint16) 4096,
	cn_attr604,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names605,
	types605,
	attr_flags605,
	gtypes605,
	(uint16) 4096,
	cn_attr605,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names606,
	types606,
	attr_flags606,
	gtypes606,
	(uint16) 4096,
	cn_attr606,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names607,
	types607,
	attr_flags607,
	gtypes607,
	(uint16) 4096,
	cn_attr607,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names608,
	types608,
	attr_flags608,
	gtypes608,
	(uint16) 4096,
	cn_attr608,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names609,
	types609,
	attr_flags609,
	gtypes609,
	(uint16) 4096,
	cn_attr609,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names610,
	types610,
	attr_flags610,
	gtypes610,
	(uint16) 4096,
	cn_attr610,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names611,
	types611,
	attr_flags611,
	gtypes611,
	(uint16) 4096,
	cn_attr611,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names612,
	types612,
	attr_flags612,
	gtypes612,
	(uint16) 4096,
	cn_attr612,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names613,
	types613,
	attr_flags613,
	gtypes613,
	(uint16) 4096,
	cn_attr613,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names614,
	types614,
	attr_flags614,
	gtypes614,
	(uint16) 4096,
	cn_attr614,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names615,
	types615,
	attr_flags615,
	gtypes615,
	(uint16) 0,
	cn_attr615,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names617,
	types617,
	attr_flags617,
	gtypes617,
	(uint16) 0,
	cn_attr617,
	12,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names618,
	types618,
	attr_flags618,
	gtypes618,
	(uint16) 4096,
	cn_attr618,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names619,
	types619,
	attr_flags619,
	gtypes619,
	(uint16) 4096,
	cn_attr619,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names620,
	types620,
	attr_flags620,
	gtypes620,
	(uint16) 4096,
	cn_attr620,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names621,
	types621,
	attr_flags621,
	gtypes621,
	(uint16) 4096,
	cn_attr621,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names622,
	types622,
	attr_flags622,
	gtypes622,
	(uint16) 4096,
	cn_attr622,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names623,
	types623,
	attr_flags623,
	gtypes623,
	(uint16) 4096,
	cn_attr623,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names624,
	types624,
	attr_flags624,
	gtypes624,
	(uint16) 4096,
	cn_attr624,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names625,
	types625,
	attr_flags625,
	gtypes625,
	(uint16) 4096,
	cn_attr625,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names626,
	types626,
	attr_flags626,
	gtypes626,
	(uint16) 4096,
	cn_attr626,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names627,
	types627,
	attr_flags627,
	gtypes627,
	(uint16) 0,
	cn_attr627,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names628,
	types628,
	attr_flags628,
	gtypes628,
	(uint16) 0,
	cn_attr628,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names629,
	types629,
	attr_flags629,
	gtypes629,
	(uint16) 0,
	cn_attr629,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names630,
	types630,
	attr_flags630,
	gtypes630,
	(uint16) 0,
	cn_attr630,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names631,
	types631,
	attr_flags631,
	gtypes631,
	(uint16) 8965,
	cn_attr631,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names632,
	types632,
	attr_flags632,
	gtypes632,
	(uint16) 8448,
	cn_attr632,
	4,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names633,
	types633,
	attr_flags633,
	gtypes633,
	(uint16) 0,
	cn_attr633,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names634,
	types634,
	attr_flags634,
	gtypes634,
	(uint16) 0,
	cn_attr634,
	20,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names635,
	types635,
	attr_flags635,
	gtypes635,
	(uint16) 0,
	cn_attr635,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names636,
	types636,
	attr_flags636,
	gtypes636,
	(uint16) 0,
	cn_attr636,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names637,
	types637,
	attr_flags637,
	gtypes637,
	(uint16) 0,
	cn_attr637,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names638,
	types638,
	attr_flags638,
	gtypes638,
	(uint16) 0,
	cn_attr638,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names640,
	types640,
	attr_flags640,
	gtypes640,
	(uint16) 0,
	cn_attr640,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names641,
	types641,
	attr_flags641,
	gtypes641,
	(uint16) 4096,
	cn_attr641,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names642,
	types642,
	attr_flags642,
	gtypes642,
	(uint16) 4096,
	cn_attr642,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names643,
	types643,
	attr_flags643,
	gtypes643,
	(uint16) 4096,
	cn_attr643,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names644,
	types644,
	attr_flags644,
	gtypes644,
	(uint16) 4096,
	cn_attr644,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names645,
	types645,
	attr_flags645,
	gtypes645,
	(uint16) 0,
	cn_attr645,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names647,
	types647,
	attr_flags647,
	gtypes647,
	(uint16) 0,
	cn_attr647,
	12,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names648,
	types648,
	attr_flags648,
	gtypes648,
	(uint16) 4096,
	cn_attr648,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names649,
	types649,
	attr_flags649,
	gtypes649,
	(uint16) 4096,
	cn_attr649,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names650,
	types650,
	attr_flags650,
	gtypes650,
	(uint16) 4096,
	cn_attr650,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names651,
	types651,
	attr_flags651,
	gtypes651,
	(uint16) 4096,
	cn_attr651,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names652,
	types652,
	attr_flags652,
	gtypes652,
	(uint16) 4096,
	cn_attr652,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names653,
	types653,
	attr_flags653,
	gtypes653,
	(uint16) 4096,
	cn_attr653,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names654,
	types654,
	attr_flags654,
	gtypes654,
	(uint16) 4096,
	cn_attr654,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names655,
	types655,
	attr_flags655,
	gtypes655,
	(uint16) 0,
	cn_attr655,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names656,
	types656,
	attr_flags656,
	gtypes656,
	(uint16) 0,
	cn_attr656,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CELL",
	names657,
	types657,
	attr_flags657,
	gtypes657,
	(uint16) 0,
	cn_attr657,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPE",
	names658,
	types658,
	attr_flags658,
	gtypes658,
	(uint16) 0,
	cn_attr658,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names659,
	types659,
	attr_flags659,
	gtypes659,
	(uint16) 0,
	cn_attr659,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names660,
	types660,
	attr_flags660,
	gtypes660,
	(uint16) 0,
	cn_attr660,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names661,
	types661,
	attr_flags661,
	gtypes661,
	(uint16) 0,
	cn_attr661,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 7,
	(long) 6,
	"INDEXABLE_ITERATION_CURSOR",
	names662,
	types662,
	attr_flags662,
	gtypes662,
	(uint16) 0,
	cn_attr662,
	28,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names666,
	types666,
	attr_flags666,
	gtypes666,
	(uint16) 4096,
	cn_attr666,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names667,
	types667,
	attr_flags667,
	gtypes667,
	(uint16) 4096,
	cn_attr667,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names668,
	types668,
	attr_flags668,
	gtypes668,
	(uint16) 4096,
	cn_attr668,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names669,
	types669,
	attr_flags669,
	gtypes669,
	(uint16) 4096,
	cn_attr669,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names670,
	types670,
	attr_flags670,
	gtypes670,
	(uint16) 4096,
	cn_attr670,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names671,
	types671,
	attr_flags671,
	gtypes671,
	(uint16) 4096,
	cn_attr671,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names672,
	types672,
	attr_flags672,
	gtypes672,
	(uint16) 4096,
	cn_attr672,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names673,
	types673,
	attr_flags673,
	gtypes673,
	(uint16) 4096,
	cn_attr673,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names674,
	types674,
	attr_flags674,
	gtypes674,
	(uint16) 4096,
	cn_attr674,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names675,
	types675,
	attr_flags675,
	gtypes675,
	(uint16) 4096,
	cn_attr675,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names676,
	types676,
	attr_flags676,
	gtypes676,
	(uint16) 4096,
	cn_attr676,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names677,
	types677,
	attr_flags677,
	gtypes677,
	(uint16) 0,
	cn_attr677,
	4,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names680,
	types680,
	attr_flags680,
	gtypes680,
	(uint16) 0,
	cn_attr680,
	12,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names681,
	types681,
	attr_flags681,
	gtypes681,
	(uint16) 4096,
	cn_attr681,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names682,
	types682,
	attr_flags682,
	gtypes682,
	(uint16) 4096,
	cn_attr682,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names683,
	types683,
	attr_flags683,
	gtypes683,
	(uint16) 4096,
	cn_attr683,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names684,
	types684,
	attr_flags684,
	gtypes684,
	(uint16) 4096,
	cn_attr684,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names685,
	types685,
	attr_flags685,
	gtypes685,
	(uint16) 4096,
	cn_attr685,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names686,
	types686,
	attr_flags686,
	gtypes686,
	(uint16) 4096,
	cn_attr686,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names687,
	types687,
	attr_flags687,
	gtypes687,
	(uint16) 4096,
	cn_attr687,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names688,
	types688,
	attr_flags688,
	gtypes688,
	(uint16) 4096,
	cn_attr688,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names689,
	types689,
	attr_flags689,
	gtypes689,
	(uint16) 4096,
	cn_attr689,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 1,
	(long) 1,
	"CELL",
	names690,
	types690,
	attr_flags690,
	gtypes690,
	(uint16) 0,
	cn_attr690,
	4,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names691,
	types691,
	attr_flags691,
	gtypes691,
	(uint16) 0,
	cn_attr691,
	20,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0}
	,
	NULL
},};


#ifdef __cplusplus
}
#endif
