/*
 * Class HASH_TABLE_ITERATION_CURSOR [G#1, G#2]
 */

#include "eif_macros.h"


#ifdef __cplusplus
extern "C" {
#endif

static const EIF_TYPE_INDEX egt_0_239 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX egt_1_239 [] = {0xFF01,183,238,0xFFF8,1,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX egt_2_239 [] = {0xFF01,238,0xFFF8,1,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX egt_3_239 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_4_239 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_5_239 [] = {0xFF01,238,0xFFF8,1,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX egt_6_239 [] = {0xFF01,238,0xFFF8,1,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX egt_7_239 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_8_239 [] = {0xFF01,9,0xFFFF};
static const EIF_TYPE_INDEX egt_9_239 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX egt_10_239 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX egt_11_239 [] = {0xFF01,10,0xFFFF};
static const EIF_TYPE_INDEX egt_12_239 [] = {238,0xFFF8,1,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX egt_13_239 [] = {0xFF01,238,0xFFF8,1,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX egt_14_239 [] = {0xFF01,238,0xFFF8,1,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX egt_15_239 [] = {0xFF01,238,0xFFF8,1,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX egt_16_239 [] = {0xFF01,238,0xFFF8,1,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX egt_17_239 [] = {0xFF01,238,0xFFF8,1,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX egt_18_239 [] = {0xFF01,233,0xFFF8,1,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX egt_19_239 [] = {0xFF01,115,0xFFFF};
static const EIF_TYPE_INDEX egt_20_239 [] = {0xFF01,179,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_21_239 [] = {0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_22_239 [] = {0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_23_239 [] = {0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_24_239 [] = {0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX egt_25_239 [] = {0xFFF8,2,0xFFFF};


static const struct desc_info desc_239[] = {
	{EIF_GENERIC(NULL), 0xFFFFFFFF, 0xFFFFFFFF},
	{EIF_GENERIC(egt_0_239), 0, 0xFFFFFFFF},
	{EIF_GENERIC(egt_1_239), 1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 2, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 3, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 4, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 5, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 6, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 7, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 8, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 9, 0xFFFFFFFF},
	{EIF_GENERIC(egt_2_239), 10, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 11, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 12, 0xFFFFFFFF},
	{EIF_GENERIC(egt_3_239), 13, 0xFFFFFFFF},
	{EIF_GENERIC(egt_4_239), 14, 0xFFFFFFFF},
	{EIF_GENERIC(egt_5_239), 15, 0xFFFFFFFF},
	{EIF_GENERIC(egt_6_239), 16, 0xFFFFFFFF},
	{EIF_GENERIC(egt_7_239), 17, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 18, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 19, 0xFFFFFFFF},
	{EIF_GENERIC(egt_8_239), 20, 0xFFFFFFFF},
	{EIF_GENERIC(egt_9_239), 21, 0xFFFFFFFF},
	{EIF_GENERIC(egt_10_239), 22, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 23, 0xFFFFFFFF},
	{EIF_GENERIC(egt_11_239), 24, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 25, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 26, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 27, 0xFFFFFFFF},
	{EIF_GENERIC(egt_12_239), 28, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0143 /*161*/), 29, 0xFFFFFFFF},
	{EIF_GENERIC(egt_13_239), 30, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2675, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0xF5 /*122*/), 2677, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0xF5 /*122*/), 2678, 12},
	{EIF_NON_GENERIC(0xF5 /*122*/), 2654, 16},
	{EIF_NON_GENERIC(0xF5 /*122*/), 2655, 20},
	{EIF_NON_GENERIC(0xF5 /*122*/), 2656, 24},
	{EIF_GENERIC(egt_14_239), 2658, 0xFFFFFFFF},
	{EIF_GENERIC(egt_15_239), 2659, 0xFFFFFFFF},
	{EIF_GENERIC(egt_16_239), 2660, 0xFFFFFFFF},
	{EIF_GENERIC(egt_17_239), 2661, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x010D /*134*/), 2662, 8},
	{EIF_NON_GENERIC(0x013D /*158*/), 2664, 4},
	{EIF_NON_GENERIC(0x013D /*158*/), 2665, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 2666, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 2667, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2668, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2669, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2670, 0xFFFFFFFF},
	{EIF_GENERIC(egt_18_239), 2958, 0},
	{EIF_GENERIC(egt_19_239), 2673, 0xFFFFFFFF},
	{EIF_GENERIC(egt_20_239), 2657, 0xFFFFFFFF},
	{EIF_GENERIC(egt_21_239), -1, 0xFFFFFFFF},
	{EIF_GENERIC(egt_22_239), 2954, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 2956, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 2957, 0xFFFFFFFF},
	{EIF_GENERIC(egt_23_239), -1, 0xFFFFFFFF},
	{EIF_GENERIC(egt_24_239), 2955, 0xFFFFFFFF},
	{EIF_GENERIC(egt_25_239), -1, 0xFFFFFFFF},
};
void Init239(void)
{
	IDSC(desc_239, 0, 238);
	IDSC(desc_239 + 1, 2, 238);
	IDSC(desc_239 + 32, 130, 238);
	IDSC(desc_239 + 52, 67, 238);
	IDSC(desc_239 + 54, 68, 238);
	IDSC(desc_239 + 58, 209, 238);
}


#ifdef __cplusplus
}
#endif
