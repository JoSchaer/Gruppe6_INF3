/*
 * Class BILINEAR [INTEGER_32]
 */

#include "eif_macros.h"


#ifdef __cplusplus
extern "C" {
#endif

static const EIF_TYPE_INDEX egt_0_257 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX egt_1_257 [] = {0xFF01,183,256,122,0xFFFF};
static const EIF_TYPE_INDEX egt_2_257 [] = {0xFF01,256,122,0xFFFF};
static const EIF_TYPE_INDEX egt_3_257 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_4_257 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_5_257 [] = {0xFF01,256,122,0xFFFF};
static const EIF_TYPE_INDEX egt_6_257 [] = {0xFF01,256,122,0xFFFF};
static const EIF_TYPE_INDEX egt_7_257 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_8_257 [] = {0xFF01,9,0xFFFF};
static const EIF_TYPE_INDEX egt_9_257 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX egt_10_257 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX egt_11_257 [] = {0xFF01,10,0xFFFF};
static const EIF_TYPE_INDEX egt_12_257 [] = {256,122,0xFFFF};
static const EIF_TYPE_INDEX egt_13_257 [] = {0xFF01,256,122,0xFFFF};
static const EIF_TYPE_INDEX egt_14_257 [] = {0xFF01,223,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_15_257 [] = {0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_16_257 [] = {0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_17_257 [] = {0xFFF8,1,0xFFFF};


static const struct desc_info desc_257[] = {
	{EIF_GENERIC(NULL), 1338, 0xFFFFFFFF},
	{EIF_GENERIC(egt_0_257), 0, 0xFFFFFFFF},
	{EIF_GENERIC(egt_1_257), 1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 2, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 3, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 4, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 5, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 6, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 7, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 8, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 9, 0xFFFFFFFF},
	{EIF_GENERIC(egt_2_257), 10, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 11, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 12, 0xFFFFFFFF},
	{EIF_GENERIC(egt_3_257), 13, 0xFFFFFFFF},
	{EIF_GENERIC(egt_4_257), 14, 0xFFFFFFFF},
	{EIF_GENERIC(egt_5_257), 15, 0xFFFFFFFF},
	{EIF_GENERIC(egt_6_257), 16, 0xFFFFFFFF},
	{EIF_GENERIC(egt_7_257), 17, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 18, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 19, 0xFFFFFFFF},
	{EIF_GENERIC(egt_8_257), 20, 0xFFFFFFFF},
	{EIF_GENERIC(egt_9_257), 21, 0xFFFFFFFF},
	{EIF_GENERIC(egt_10_257), 22, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 23, 0xFFFFFFFF},
	{EIF_GENERIC(egt_11_257), 24, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 25, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 26, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 27, 0xFFFFFFFF},
	{EIF_GENERIC(egt_12_257), 28, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0143 /*161*/), 29, 0xFFFFFFFF},
	{EIF_GENERIC(egt_13_257), 30, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 1200, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 1062, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 1063, 0},
	{EIF_NON_GENERIC(0x013D /*158*/), 1064, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 1065, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 1066, 0xFFFFFFFF},
	{EIF_GENERIC(egt_14_257), 1198, 0xFFFFFFFF},
	{EIF_GENERIC(egt_15_257), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0xF5 /*122*/), 1201, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 1337, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0xF5 /*122*/), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0xF5 /*122*/), 1190, 0xFFFFFFFF},
	{EIF_GENERIC(egt_16_257), 1191, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 1192, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(egt_17_257), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 1336, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 1194, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 1195, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 1196, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 1197, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 1202, 0xFFFFFFFF},
};
void Init257(void)
{
	IDSC(desc_257, 0, 256);
	IDSC(desc_257 + 1, 2, 256);
	IDSC(desc_257 + 32, 196, 256);
	IDSC(desc_257 + 41, 46, 256);
	IDSC(desc_257 + 50, 135, 256);
	IDSC(desc_257 + 57, 194, 256);
}


#ifdef __cplusplus
}
#endif
