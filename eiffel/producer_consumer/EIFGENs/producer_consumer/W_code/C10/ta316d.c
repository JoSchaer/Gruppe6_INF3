/*
 * Class TABLE_ITERABLE [INTEGER_32, INTEGER_32]
 */

#include "eif_macros.h"


#ifdef __cplusplus
extern "C" {
#endif

static const EIF_TYPE_INDEX egt_0_316 [] = {0xFF01,175,0xFFFF};
static const EIF_TYPE_INDEX egt_1_316 [] = {0xFF01,186,315,126,126,0xFFFF};
static const EIF_TYPE_INDEX egt_2_316 [] = {0xFF01,315,126,126,0xFFFF};
static const EIF_TYPE_INDEX egt_3_316 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_4_316 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_5_316 [] = {0xFF01,315,126,126,0xFFFF};
static const EIF_TYPE_INDEX egt_6_316 [] = {0xFF01,315,126,126,0xFFFF};
static const EIF_TYPE_INDEX egt_7_316 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_8_316 [] = {0xFF01,10,0xFFFF};
static const EIF_TYPE_INDEX egt_9_316 [] = {0xFF01,175,0xFFFF};
static const EIF_TYPE_INDEX egt_10_316 [] = {0xFF01,175,0xFFFF};
static const EIF_TYPE_INDEX egt_11_316 [] = {0xFF01,11,0xFFFF};
static const EIF_TYPE_INDEX egt_12_316 [] = {315,126,126,0xFFFF};
static const EIF_TYPE_INDEX egt_13_316 [] = {0xFF01,315,126,126,0xFFFF};
static const EIF_TYPE_INDEX egt_14_316 [] = {0xFF01,316,0xFFF8,1,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX egt_15_316 [] = {0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_16_316 [] = {0xFFF8,2,0xFFFF};


static const struct desc_info desc_316[] = {
	{EIF_GENERIC(NULL), 0xFFFFFFFF, 0xFFFFFFFF},
	{EIF_GENERIC(egt_0_316), 0, 0xFFFFFFFF},
	{EIF_GENERIC(egt_1_316), 1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013F /*159*/), 2, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013F /*159*/), 3, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013F /*159*/), 4, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013F /*159*/), 5, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013F /*159*/), 6, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013F /*159*/), 7, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013F /*159*/), 8, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013F /*159*/), 9, 0xFFFFFFFF},
	{EIF_GENERIC(egt_2_316), 10, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 11, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 12, 0xFFFFFFFF},
	{EIF_GENERIC(egt_3_316), 13, 0xFFFFFFFF},
	{EIF_GENERIC(egt_4_316), 14, 0xFFFFFFFF},
	{EIF_GENERIC(egt_5_316), 15, 0xFFFFFFFF},
	{EIF_GENERIC(egt_6_316), 16, 0xFFFFFFFF},
	{EIF_GENERIC(egt_7_316), 17, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 18, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 19, 0xFFFFFFFF},
	{EIF_GENERIC(egt_8_316), 20, 0xFFFFFFFF},
	{EIF_GENERIC(egt_9_316), 21, 0xFFFFFFFF},
	{EIF_GENERIC(egt_10_316), 22, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 23, 0xFFFFFFFF},
	{EIF_GENERIC(egt_11_316), 24, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 25, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 26, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 27, 0xFFFFFFFF},
	{EIF_GENERIC(egt_12_316), 28, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x014B /*165*/), 29, 0xFFFFFFFF},
	{EIF_GENERIC(egt_13_316), 30, 0xFFFFFFFF},
	{EIF_GENERIC(egt_14_316), -1, 0xFFFFFFFF},
	{EIF_GENERIC(egt_15_316), -1, 0xFFFFFFFF},
	{EIF_GENERIC(egt_16_316), -1, 0xFFFFFFFF},
};
void Init316(void)
{
	IDSC(desc_316, 0, 315);
	IDSC(desc_316 + 1, 2, 315);
	IDSC(desc_316 + 32, 69, 315);
	IDSC(desc_316 + 34, 183, 315);
}


#ifdef __cplusplus
}
#endif
