/*
 * Class READABLE_INDEXABLE [NATURAL_8]
 */

#include "eif_macros.h"


#ifdef __cplusplus
extern "C" {
#endif

static const EIF_TYPE_INDEX egt_0_519 [] = {0xFF01,175,0xFFFF};
static const EIF_TYPE_INDEX egt_1_519 [] = {0xFF01,186,518,144,0xFFFF};
static const EIF_TYPE_INDEX egt_2_519 [] = {0xFF01,518,144,0xFFFF};
static const EIF_TYPE_INDEX egt_3_519 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_4_519 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_5_519 [] = {0xFF01,518,144,0xFFFF};
static const EIF_TYPE_INDEX egt_6_519 [] = {0xFF01,518,144,0xFFFF};
static const EIF_TYPE_INDEX egt_7_519 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_8_519 [] = {0xFF01,10,0xFFFF};
static const EIF_TYPE_INDEX egt_9_519 [] = {0xFF01,175,0xFFFF};
static const EIF_TYPE_INDEX egt_10_519 [] = {0xFF01,175,0xFFFF};
static const EIF_TYPE_INDEX egt_11_519 [] = {0xFF01,11,0xFFFF};
static const EIF_TYPE_INDEX egt_12_519 [] = {518,144,0xFFFF};
static const EIF_TYPE_INDEX egt_13_519 [] = {0xFF01,518,144,0xFFFF};
static const EIF_TYPE_INDEX egt_14_519 [] = {0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_15_519 [] = {0xFF01,119,0xFFFF};
static const EIF_TYPE_INDEX egt_16_519 [] = {0xFF01,519,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_17_519 [] = {0xFFF8,1,0xFFFF};


static const struct desc_info desc_519[] = {
	{EIF_GENERIC(NULL), 0xFFFFFFFF, 0xFFFFFFFF},
	{EIF_GENERIC(egt_0_519), 0, 0xFFFFFFFF},
	{EIF_GENERIC(egt_1_519), 1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013F /*159*/), 2, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013F /*159*/), 3, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013F /*159*/), 4, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013F /*159*/), 5, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013F /*159*/), 6, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013F /*159*/), 7, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013F /*159*/), 8, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013F /*159*/), 9, 0xFFFFFFFF},
	{EIF_GENERIC(egt_2_519), 10, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 11, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 12, 0xFFFFFFFF},
	{EIF_GENERIC(egt_3_519), 13, 0xFFFFFFFF},
	{EIF_GENERIC(egt_4_519), 14, 0xFFFFFFFF},
	{EIF_GENERIC(egt_5_519), 15, 0xFFFFFFFF},
	{EIF_GENERIC(egt_6_519), 16, 0xFFFFFFFF},
	{EIF_GENERIC(egt_7_519), 17, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 18, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 19, 0xFFFFFFFF},
	{EIF_GENERIC(egt_8_519), 20, 0xFFFFFFFF},
	{EIF_GENERIC(egt_9_519), 21, 0xFFFFFFFF},
	{EIF_GENERIC(egt_10_519), 22, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 23, 0xFFFFFFFF},
	{EIF_GENERIC(egt_11_519), 24, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 25, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 26, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 27, 0xFFFFFFFF},
	{EIF_GENERIC(egt_12_519), 28, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x014B /*165*/), 29, 0xFFFFFFFF},
	{EIF_GENERIC(egt_13_519), 30, 0xFFFFFFFF},
	{EIF_GENERIC(egt_14_519), -1, 0xFFFFFFFF},
	{EIF_GENERIC(egt_15_519), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013F /*159*/), -1, 0xFFFFFFFF},
	{EIF_GENERIC(egt_16_519), 3043, 0xFFFFFFFF},
	{EIF_GENERIC(egt_17_519), -1, 0xFFFFFFFF},
};
void Init519(void)
{
	IDSC(desc_519, 0, 518);
	IDSC(desc_519 + 1, 2, 518);
	IDSC(desc_519 + 32, 45, 518);
	IDSC(desc_519 + 35, 69, 518);
}


#ifdef __cplusplus
}
#endif
