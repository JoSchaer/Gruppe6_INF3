note
	description: "Summary description for {PRODUCER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PRODUCER
inherit
	THREAD
	rename
		make as producer
	end
create
	make
feature
	make(name:INTEGER;new_parking:BUFFER; new_consumers:LINKED_LIST[CONSUMER])
	do
		producer
		buffer:=new_parking
		consumers:= new_consumers
		producer_number:=name
		awake:= TRUE
		loop_moving:= TRUE
	end
feature
	buffer: BUFFER
	producer_number: INTEGER
	consumers:LINKED_LIST[CONSUMER]
	awake: BOOLEAN
	loop_moving: BOOLEAN

	execute -- Thread function doing the producing
		local
			x: INTEGER
			i:INTEGER
			asleep: DOUBLE
			r:RANDOM
			car:CAR
		do
			from

			until
				not loop_moving
			loop
				if awake
				then
					create r.make
					x:=1
					r.set_seed (2)
					asleep := r.double_item*1_000_000_000_000

					if buffer.full
					then
			   			if buffer.buffer_empty
			   			then
			   				from -- wake all consumers
			   					i:=1
			   				until
			   					i>=consumers.count
			   				loop
			   					consumers.at (i).set_awake (TRUE)
			   					i:=i+1
			   				end
			    		end
			    		create car.make (producer_number)
			   			buffer.push(car) -- parking car out of a parking space

					else
						set_awake(FALSE) -- set awake false if buffer is empty
					end
					current.sleep (asleep.truncated_to_integer_64)-- let Thread sleep for a random amount of seconds
				end
			end
		end
	set_awake(wake:BOOLEAN)
		do
			awake:=wake
		end
end
