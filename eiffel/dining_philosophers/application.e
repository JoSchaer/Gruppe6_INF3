note
	description: "DINING_PHILOSOPHERS_PROBLEM application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE}

	philosopher_count: INTEGER = 5

	food_sessions: INTEGER = 5

	philospher_list: LINKED_LIST [PHILOSOPHER]

	run_philosopher (act_philosopher: PHILOSOPHER)
		-- launchs the act_philosopher as new Thread
		do
			act_philosopher.launch
		end

	join_to_main (act_philosopher: PHILOSOPHER)
		--join every single Thread this Thread again
		do
			act_philosopher.join_all
		end

feature

	make
		local -- Method Variables
			first_chopstick: CHOPSTICK
			left_chopstick: CHOPSTICK
			right_chopstick: CHOPSTICK
			single_philosopher: PHILOSOPHER
			loop_counter: INTEGER

		do
			print ("The Philosopher meeting starts%N")
			create philospher_list.make
			from
				loop_counter := 1
				create first_chopstick.make
				left_chopstick := first_chopstick
			until
				loop_counter>philosopher_count
			loop
				if loop_counter<philosopher_count then
					create right_chopstick.make
				else
					right_chopstick := first_chopstick
				end
				create single_philosopher.make (food_sessions,left_chopstick, right_chopstick, loop_counter)
				philospher_list.extend (single_philosopher)
				loop_counter := loop_counter +1
			end
			-- run and join every Philosopher(agent) from/to main
			philospher_list.do_all (agent run_philosopher)
			philospher_list.do_all (agent join_to_main)
		end


end
