/*
 * Class TABLE_ITERATION_CURSOR [INTEGER_32, INTEGER_32]
 */

#include "eif_macros.h"


#ifdef __cplusplus
extern "C" {
#endif

static const EIF_TYPE_INDEX egt_0_244 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX egt_1_244 [] = {0xFF01,183,243,122,122,0xFFFF};
static const EIF_TYPE_INDEX egt_2_244 [] = {0xFF01,243,122,122,0xFFFF};
static const EIF_TYPE_INDEX egt_3_244 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_4_244 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_5_244 [] = {0xFF01,243,122,122,0xFFFF};
static const EIF_TYPE_INDEX egt_6_244 [] = {0xFF01,243,122,122,0xFFFF};
static const EIF_TYPE_INDEX egt_7_244 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_8_244 [] = {0xFF01,9,0xFFFF};
static const EIF_TYPE_INDEX egt_9_244 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX egt_10_244 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX egt_11_244 [] = {0xFF01,10,0xFFFF};
static const EIF_TYPE_INDEX egt_12_244 [] = {243,122,122,0xFFFF};
static const EIF_TYPE_INDEX egt_13_244 [] = {0xFF01,243,122,122,0xFFFF};
static const EIF_TYPE_INDEX egt_14_244 [] = {0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX egt_15_244 [] = {0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX egt_16_244 [] = {0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX egt_17_244 [] = {0xFFF8,1,0xFFFF};


static const struct desc_info desc_244[] = {
	{EIF_GENERIC(NULL), 0xFFFFFFFF, 0xFFFFFFFF},
	{EIF_GENERIC(egt_0_244), 0, 0xFFFFFFFF},
	{EIF_GENERIC(egt_1_244), 1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 2, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 3, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 4, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 5, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 6, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 7, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 8, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), 9, 0xFFFFFFFF},
	{EIF_GENERIC(egt_2_244), 10, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 11, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 12, 0xFFFFFFFF},
	{EIF_GENERIC(egt_3_244), 13, 0xFFFFFFFF},
	{EIF_GENERIC(egt_4_244), 14, 0xFFFFFFFF},
	{EIF_GENERIC(egt_5_244), 15, 0xFFFFFFFF},
	{EIF_GENERIC(egt_6_244), 16, 0xFFFFFFFF},
	{EIF_GENERIC(egt_7_244), 17, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 18, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 19, 0xFFFFFFFF},
	{EIF_GENERIC(egt_8_244), 20, 0xFFFFFFFF},
	{EIF_GENERIC(egt_9_244), 21, 0xFFFFFFFF},
	{EIF_GENERIC(egt_10_244), 22, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 23, 0xFFFFFFFF},
	{EIF_GENERIC(egt_11_244), 24, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 25, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 26, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 27, 0xFFFFFFFF},
	{EIF_GENERIC(egt_12_244), 28, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0143 /*161*/), 29, 0xFFFFFFFF},
	{EIF_GENERIC(egt_13_244), 30, 0xFFFFFFFF},
	{EIF_GENERIC(egt_14_244), -1, 0xFFFFFFFF},
	{EIF_GENERIC(egt_15_244), -1, 0xFFFFFFFF},
	{EIF_GENERIC(egt_16_244), -1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x013D /*158*/), -1, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), -1, 0xFFFFFFFF},
	{EIF_GENERIC(egt_17_244), -1, 0xFFFFFFFF},
};
void Init244(void)
{
	IDSC(desc_244, 0, 243);
	IDSC(desc_244 + 1, 2, 243);
	IDSC(desc_244 + 32, 209, 243);
	IDSC(desc_244 + 34, 68, 243);
}


#ifdef __cplusplus
}
#endif
