note
	description: "Summary description for {CHOPSTICK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CHOPSTICK
create
	make
feature

	stick_mutex : MUTEX
	make

		do
			create stick_mutex.make
		end

feature

	take_stick (philosopher : PHILOSOPHER) : BOOLEAN
		do
			Result := stick_mutex.try_lock
		end

	drop_stick (philospher : PHILOSOPHER)
		do
			stick_mutex.unlock
		end

end
