﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parser
{
    class Evaluator
    {
        /*Evaluator works along the AST and evaulates each node 
        * the result is either the number or the result of the operation
        */
        private double result = 0;
        private int unaryCounter = 0;

        public double evaluate(AST nodeToken){
            result = 0;
            if(nodeToken is BinaryOp){
                result = eval_binary((BinaryOp)nodeToken);
            }else if (nodeToken is Value){
                if(nodeToken is Word){
                    result = eval_word((Word)nodeToken);
                }else if (nodeToken is Number){
                    result = eval_number((Number)nodeToken);
                }else if(nodeToken is Unary){
                    result = eval_unary((Unary)nodeToken);  
                }
                    
            }
            return result; 
        }
        /*
         * Evaluates the numbers and returns each value
         */
        private double eval_number(Number num){
            String val = num.value;
            for (int i =0; i< val.Length; i++)
            {
                int help_num = 0;
                switch (val[i].ToString())
                {

                    case "1":
                        help_num = 1;
                        break;
                    case "2":
                        help_num = 2;
                        break;
                    case "3":
                        help_num = 3;
                        break;
                    case "4":
                        help_num = 4;
                        break;
                    case "5":
                        help_num = 5;
                        break;
                    case "6":
                        help_num = 6;
                        break;
                    case "7":
                        help_num = 7;
                        break;
                    case "8":
                        help_num = 8;
                        break;
                    case "9":
                        help_num = 9;
                        break;
                }
                result += help_num * Math.Pow(10, val.Length - i - 1);
            }
            return result;            
        }
        /*Evaluates Unarys and counts them */
        private double eval_unary(Unary uni){
            
            return uni.value.Length;
        }
        /*Evaluates the word and gives back the correct color*/
        private double eval_word(Word word){
            result = -1.0;
            switch(word.value){
                case"one":
                case"eins":
                case"uno":
                    result = 1;
                    break;
                case"two":
                case"zwei":
                case"dos":
                    result = 2;
                    break;
                case"drei":
                case"three":
                case"tres":
                    result = 3;
                    break;
            }
            return result; 
        }
        /*Evaluates a binary operation depending on the operation 
        the equation is */
        private double eval_binary(BinaryOp nodeToken){

            result = 0;
            //gets both numbers for left and right hand argument 
            double num1 = this.evaluate(nodeToken.left);
            double num2 = this.evaluate(nodeToken.right);
            //goes through operators and at the correct one does the equation
            if (nodeToken.op.toString().Equals("+"))
            {
                result = num1 + num2;
            }
            else if (nodeToken.op.toString().Equals("-"))
            {
                result = num1 - num2;
            }
            else if (nodeToken.op.toString().Equals("*"))
            {
                result = num1 * num2;
            }
            else if (nodeToken.op.toString().Equals("/")){
                if(num2 == 0)
                {
                    Console.Error.WriteLine("Cannot be divided by zero");
                }
                else {
                    result = num1 / num2;
                }
                
            }

            return result;
        }

    }
}
