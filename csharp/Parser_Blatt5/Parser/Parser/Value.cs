﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser
{
    class Value : Expression
    {
        /*VALUE:= NUMBER | UNARY | WORD*/
        public readonly String value;
        public Value(String value)
        {
            this.value = value;
        }
    }
}
