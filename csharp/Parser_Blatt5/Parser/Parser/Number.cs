﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Parser
{
    class Number : Value
    {
        /*NUMBER := DIGIT_WOZ, {DIGIT}*/
        public Number(String value) : base(value) { }
    }
}
