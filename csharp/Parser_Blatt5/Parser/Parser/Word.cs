﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser
{
    class Word : Value
    {
        /*WORD:= "one" | "two" | "three" | "eins" | "zwei" | "drei"*/
        public Word(String value): base(value)
        {
        }
    }
}
