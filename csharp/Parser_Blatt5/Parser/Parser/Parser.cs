﻿using System;
using System.Collections.Generic;

namespace Parser
{
    public class Parser
    {
        //each rule in the EBNF is a AST Element
        /*
        EXPRESSION := ("(", EXPRESSION, ")") | BINARY_OP | VALUE
        BINARY_OP  := EXPRESSION, OPERATOR, EXPRESSION
        VALUE:= NUMBER | UNARY | WORD
        NUMBER := DIGIT_WOZ, {DIGIT}
        WORD:= "one" | "two" | "three" | "eins" | "zwei" | "drei"
        DIGIT_WOZ :="1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
        DIGIT:= DIGIT_WOZ | "0"
        UNARY:= "|", [UNARY]
        OPERATOR:= "+" | "-" | "*" |"/"
        */
        private int current;
        private List<Token> tsList;
        private AST ast;

        public Parser(){
            
        }
        /*Takes the token information and follows along the EBNF 
        Through out each it compares if it is a word, number or operation
        0 does not count as a number in our case*/
        public AST parse(List<Token> ts){
            try{
                this.tsList = ts;
                current = 0;
                ast = expression();
            }catch(Exception e){
                Console.WriteLine(e.Message);
            }
            return ast;

        }
        /*a Expression can either be inside () or a binary operation or a value*/
        private Expression expression() {
            Expression express = null;
            if(tsList[current].getValue() == "("){
                current++;
                express = expression();
                if(tsList[current].getValue() == ")"){
                    current++;
                    if(current < tsList.Count){
                        express = binaryOp(express);
                    }
                }else{
                    throw new SystemException("Expected ')' but did not recieve, these are bad math skills");
                }
            }else if ( current < tsList.Count-1 && isToken(tsList[current])){
                express = binaryOp(null);
            }
            if(express == null){
                express = value(tsList[current]);
            }
            return express;
        }
        /*A binary Op in an AST contains a left value and a right value 
        as well as an operation in the middle*/
        private BinaryOp binaryOp(Expression ex){
            BinaryOp binOp =null; 
            Expression left;

            if(ex != null){
                left = ex;
            }else{
                left = value(tsList[current]);
            }
            Operator o = expressionOperator(tsList[current]);
            if(o!=null){
                Expression right = expression();
                binOp = new BinaryOp(left, o, right); 
            }else{
                current--;
            }
            return binOp;
        }
        /*A value is a part if a binary operation and can either be a 
        number, word or unary*/
        private Value value(Token ts){
            Value v = null;
            if(ts is NumberToken){
                v = new Number(ts.getValue());
                current++;
            }
            else if(ts is StringToken){
                v = new Word(ts.getValue());
                current++;
            }
            else if(ts is SpecialToken){
                v = new Unary("1");
            }else{
                throw new SystemException("These were not the expected values. Only found:" + ts.getValue());
            }
            return v;
        }

        private bool isToken(Token ts)
        {
            return number(ts) || word(ts) || unary(ts);
        }
        /*is saved in the ast as a number and more specifically as a digit with or 
        without 0*/
        private bool number(Token ts){
            bool result = false;
            if(digitWoz(ts.getValue()[0])){
                result = true;
                foreach(char c in ts.getValue()){
                    if(!digit(c)){
                        result = false;
                    }
                }
            }
            return result;

        }
        /*Is saved in the AST as word*/
        private bool word(Token ts){
            bool result = false;
            switch(ts.getValue()){
                case"one":
                case"two":
                case"three":
                case"eins":
                case"zwei":
                case"drei":
                case"uno":
                case"dos":
                case"tres":
                    result = true;
                    break;
            }
            return result;

        }
        /*NumberToken is turned into a digit or rather is saved in the 
        AST as a Digit*/ 
        private bool digitWoz(char d){
            bool result = false;
            switch(d){
                case'1':
                case'2':
                case'3':
                case'4':
                case'5':
                case'6':
                case'7':
                case'8':
                case'9':
                    result = true;
                    break;
            }
            return result;
        }
        /*Here is the difference between a digit that is zero or not zero*/
        private bool digit(char d){
            return d == '0' || digitWoz(d);
        }
        /*checks for a Unary Operator*/
        private bool unary(Token ts)
        {
            bool result = false;
            if(ts.getValue() == "|")
            {
                result = true;
            }
            return result;
        }
        /* Each Special Token is made into an operator
             but if the Special token is a | the amout of following 
             | is counted to add them all together
         */
        private Operator expressionOperator(Token ts){
            Operator o = null;
            switch(ts.getValue()){
                case"+":
                case"-":
                case"*":
                case"/":
                    o = new Operator(ts.getValue());
                    current++;
                    break;
                case")":
                    break;
                case "|":
                    if (current < tsList.Count -1 && tsList[++current].getValue() == "|")
                    {
                        o = new Operator("+");
                    }else
                    {
                        o = new Operator(tsList[current++].getValue());
                    }
                    break;
                default: throw new SystemException("False Input. These are bad math skills - in Parser with expressions Operator");
            }
            return o;
        }
    }
}
