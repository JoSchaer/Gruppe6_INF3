﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parser
{
    public abstract class Token
    {
        /*
         * get Value from the single Token T is a Wildcard,
         * because every Token returns a other Type 
         */
        public abstract String getValue();


    
    }
}
