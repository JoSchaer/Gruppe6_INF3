﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser
{
    class Unary : Value
    {
        /*UNARY:= "|", [UNARY]*/
        public Unary(String value): base(value) { }
    }
}
