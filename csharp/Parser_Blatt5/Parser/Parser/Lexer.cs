﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;

namespace Parser
{

    class Lexer
    {
        private List<Token> tokens = new List<Token>();

        public List<Token> lex (String s)
        {
            int counter = 0;
            String numberLine;
            String stringLine;
          
           
            while (counter < s.Length)
            {
                //make single or multiple numberTokens
                if (isNumber(s[counter].ToString()))
                {
                    numberLine = s[counter].ToString();
                    counter++;
                    try {while (isNumber(s[counter].ToString()))
                    {
                        numberLine += s[counter].ToString();
                        counter++;
                    }
                    }
                    catch (Exception e) {
                        Console.WriteLine(" ");

                    }
                    
                    tokens.Add(new NumberToken(numberLine));
                }
                //make single or multiple stringTokens
                else if (isString(s[counter].ToString()))
                {
                    stringLine = s[counter].ToString();
                    counter++;
                    try
                    {
                        while (isString(s[counter].ToString()))
                    {
                        stringLine += s[counter].ToString();
                        counter++;
                    }

                    }catch(Exception e)
                    {
                        Console.WriteLine("  ");
                    }
                   

                    tokens.Add(new StringToken(stringLine));
                }
                //Makes single spezial Tokens
                else if (isSpezial(s[counter].ToString()))
                {
                    tokens.Add(new SpecialToken(s[counter].ToString()));
                    counter++;
                }

                //ignore all other cases like tab, whitespace, ...
                else
                {
                    counter++;
                }
                
            }
            return this.tokens;
        }

        private bool isNumber(String singleChar)
        {
            //all chars between 1 and 9
            Regex aNumber = new Regex("[0-9]");
            return aNumber.IsMatch(singleChar);
        }

        private bool isString(String singleChar)
        {
            //all chars between A-Z and a-z
            Regex aString = new Regex("[A-z]");
            return aString.IsMatch(singleChar);
        }

        private bool isSpezial(String singleChar)
        {
            Regex aSpezial = new Regex("|()+-*/");
            return aSpezial.IsMatch(singleChar);
        }
    }

}
