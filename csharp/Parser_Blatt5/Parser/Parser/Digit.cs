﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser
{
    class Digit : Value
    {
        /*DIGIT:= DIGIT_WOZ | "0"*/
        public Digit(String value) : base(value) { }
    }
}
