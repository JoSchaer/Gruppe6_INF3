﻿using System;
using System.Collections.Generic;

namespace Parser
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * With a list of String from the input file
            the lexer goes in and makes each string line into 
            a list of tokens, depending on what is contained in the string
            */
            Lexer lexer = new Lexer();
            List<String> inputExpression = new List<String>();

            
           String inputLine = args[0];
           String line;

            //reads in file and adds the line string to string
            System.IO.StreamReader fileIn = new System.IO.StreamReader(inputLine);
            while ((line = fileIn.ReadLine())!=null){
                if(line.Length > 0){
                    inputExpression.Add(line);
                }
            }
            fileIn.Close();
            
            /*for each line in the file a token list is created
             * this list is then parsed into the different parts of 
             * an abstract syntax tree, later the parsed tokens 
             * are given to the evualtor which gives it a direct numerical number
            */
            for (int i = 0; i < inputExpression.Count; i++){
                
                Console.WriteLine("In line " + (i + 1) + " : " + inputExpression[i]);
                List<Token> tokens = lexer.lex(inputExpression[i]);
                Parser parser = new Parser();
                Evaluator evaluator = new Evaluator();
                foreach(Token tok in tokens)
                {
                    Console.WriteLine(tok.ToString()+"  "+ tok.getValue().ToString());
                }
                AST ast = parser.parse(tokens);
                if(ast != null){
                    Console.WriteLine("This is the end result: "+evaluator.evaluate(ast) + "\n"); 
                }
                tokens.Clear();
            }
            
           

            Console.ReadLine();
             




           

        }
    }
}
