﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parser
{
    class SpecialToken : Token
    {
        /*Contains the mathmatical operations: +,-,*,/ 
        as well as the Unary | 
        all these are contained singularly
        */
        private String value;

        public SpecialToken (String value)
        {
            this.value = value;
        }

        public override String getValue()
        {
            return this.value;
        }

    }
}
