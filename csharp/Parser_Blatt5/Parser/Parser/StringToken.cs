﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parser
{
    /*
    This token if filled with chars that all from a word
    it only takes in the words one/two/three/eins/zwei/drei/uno/dos/tres
    */

    class StringToken : Token
    {
        private String value;

        public StringToken(String value)
        {
            this.value = value;
        }
        public override String getValue()
        {
            return this.value;
        }

    }
}
