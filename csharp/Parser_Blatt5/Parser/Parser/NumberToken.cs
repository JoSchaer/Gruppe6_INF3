﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parser
{
    class NumberToken : Token
    {
        /*Each Token contains a number*/
        private String value;

        public NumberToken (String value)
        {
            this.value = value;
        }
        public override String getValue()
        {
            return this.value;
        }
        public int getIntValue(){
            return Int32.Parse(this.value);
        }
    }
}
