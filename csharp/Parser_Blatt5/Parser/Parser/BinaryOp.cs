﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser
{
    class BinaryOp : Expression
    {
        /*BINARY_OP  := EXPRESSION, OPERATOR, EXPRESSION*/
        public readonly Expression left;
        public readonly Expression right;
        public readonly Operator op;

        public BinaryOp(Expression left, Operator op, Expression right)
        {
            this.left = left;
            this.right = right;
            this.op = op;
        }
    }
}
