﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parser
{
    class Operator : AST
    {
        /*OPERATOR:= "+" | "-" | "*" |"/"*/
        public readonly String op;
        
        public Operator(String op)
        {
            this.op = op;
        }
        public String toString()
        {
            return this.op;
        }
    }
}
