﻿using System;
namespace QuadTree
{
    public class Rectangle : Shape
    {
        private Point upperLeft;
        private float width, height;
        private int id;

        //Constructor with upperLeft point width and height and id of the rectangle
        public Rectangle(Point upperLeft, float width, float height, int id)
        {
            
            this.upperLeft = upperLeft;
            this.width = width;
            this.height = height;
            this.id = id;
          
        }

        //constructor for boundingBox
        public Rectangle(Point upperLeft, float width, float height)
        {

            this.upperLeft = upperLeft;
            this.width = width;
            this.height = height;

        }




        //returns the point, width and height of the rectangle in a Tuple
        public (Point,float,float, int) getShapeInformation()
        {
            return (upperLeft,width,height, id);
        }

        public override int getId()
        {
            return id;
        }

        //returns the bounding box of this object
        public override Rectangle boundingBox()
        {
            return this;
        }

        //return this object
        public override Shape getShape()
        {
            return this;
        }
        

        //Check if Shape s and this rectangle are colliding
        public override bool intersects(Shape s)
        {  
        /*
            First checking all X coordinates to see if it is inside
            //xR1<xR2
            bool a = this.upperLeft.getX() < s.boundingBox().getShapeInformation().Item1.getX();

            //xR2<xR1Max
            bool b = s.boundingBox().getShapeInformation().Item1.getX() < this.upperLeft.getX() + this.width;

            //xR2 < xR1
            bool c = s.boundingBox().getShapeInformation().Item1.getX() < this.upperLeft.getX();

            //xR1 < xR2Max

            bool d = this.upperLeft.getX() < s.boundingBox().getShapeInformation().Item1.getX() + s.boundingBox().getShapeInformation().Item2;


            Then checking all Y coordinates to see if it is inside
            //yR1 < yR2
            bool e = this.upperLeft.getY() < s.boundingBox().getShapeInformation().Item1.getY();

            //yR2 < yR1Max
            bool f = s.boundingBox().getShapeInformation().Item1.getY() < this.upperLeft.getY() + this.height;

            //yR2<yR1
            bool g = s.boundingBox().getShapeInformation().Item1.getY() < this.upperLeft.getY();

            //yR1 < yR2Max

            bool h = this.upperLeft.getY() < s.boundingBox().getShapeInformation().Item1.getY() + s.boundingBox().getShapeInformation().Item3;


            returning if any of the coordinates intersect
            return everythingTogether = ((a && b) || (c && d)) && ((e && f) || (g && h));

        */

            return ((this.upperLeft.getX() < s.boundingBox().getShapeInformation().Item1.getX() && s.boundingBox().getShapeInformation().Item1.getX() < this.upperLeft.getX() + this.width) || (s.boundingBox().getShapeInformation().Item1.getX() < this.upperLeft.getX() && this.upperLeft.getX() < s.boundingBox().getShapeInformation().Item1.getX() + s.boundingBox().getShapeInformation().Item2)) && ((this.upperLeft.getY() < s.boundingBox().getShapeInformation().Item1.getY() && s.boundingBox().getShapeInformation().Item1.getY() < this.upperLeft.getY() + this.height) || (s.boundingBox().getShapeInformation().Item1.getY() < this.upperLeft.getY() && this.upperLeft.getY() < s.boundingBox().getShapeInformation().Item1.getY() + s.boundingBox().getShapeInformation().Item3));


        }

    }
}
