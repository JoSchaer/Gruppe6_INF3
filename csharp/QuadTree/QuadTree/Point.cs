﻿using System;
namespace QuadTree
{
    public class Point
    {
        private float x, y;

        public Point(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public float getX()
        {
            return this.x;
        }

        public float getY()
        {
            return this.y;
        }
    }
}
