﻿using System;
using System.Collections.Generic;
namespace QuadTree
{

    public class Quadtree
    {
        private List<Quadtree> children = new List<Quadtree>();
        private List<Shape> elements = new List<Shape>();
        private Rectangle area;
        private int minElements;
        private int maxElements;

      
        //Constructor of QuadTree. Gets an area and the min/max values of elements
        public Quadtree(Rectangle area, int minElements, int maxElements)
        {
            this.area = area;
            this.minElements = minElements;
            this.maxElements = maxElements;
        }

        //returns x,y,width and height of the QuadTree
        public (float, float, float,float) getX_Y_Width_Height()
        {
            return (this.area.getShapeInformation().Item1.getX(), this.area.getShapeInformation().Item1.getX(), this.area.getShapeInformation().Item2, this.area.getShapeInformation().Item3);
        }

        public int getElementSize()
        {
            return this.elements.Count;
        }

        public List<Quadtree> getChildren()
        {
            return this.children;
        }

        //each element has an ID to indentify it, a list of all ID of the elements is returned
        public List<int> getElementIds()
        {
            List<int> elementIds = new List<int>();

            foreach(Shape actShape in this.elements)
            {
                elementIds.Add(actShape.getId());
            }

            foreach (Quadtree actChild in children)
            {
                foreach (int actId in actChild.getElementIds())
                {
                    elementIds.Add(actId);
                }    
            }
            return elementIds;
        }

        //returns the number of children of the tree
        public int getChildrenSize()
        {
            return this.children.Count;
        }


        //returns a random element to delete
        public Shape getRandomElement()
        {
            Shape randomShape = null;
            if (this.elements.Count == 0)
            {

                for(int i = 0; i <children.Count; i++)
                {
                    if (this.children[i].elements.Count > 0 || children[i].children.Count > 0)
                    {
                        randomShape = this.children[i].children.Count >= 1 ? children[i].getRandomElement() : this.children[i].elements[0];
                    }
                }

            }
            else
            {
                randomShape = this.elements[new Random().Next(0, elements.Count - 1)];
            }

            return randomShape;
        }
        
        //Count each element of this tree and it's childs and return this value
        public int count()
        {
            int counter = 0;
            if(children.Count ==0){
                counter = elements.Count;
            }else{
                foreach (Quadtree child in children)
                {
                   
                   counter+=child.elements.Count;
                }  
            }

            return counter;
        }

        //Add an element to the tree and split if the limit reached
        public void insertShape(Shape s)
        {
            if (!elements.Contains(s) && s != null)
            {
                elements.Add(s);

                if(!elements.Contains(s)){
                    Console.Write("The Shape was not inserted");
                }
            }else{
                Console.Write("Shape is either already contained or null");
            }

            //pre-condition for the split method
            if(elements.Count >= maxElements)
            {
                this.split();
            }
        }

        //Delete an element and merge the Tree if it does not have enough elements inside
        public void removeShape(Shape s)
        {
            if (elements.Count != 0 && elements.Contains(s) && s != null)
            {
                elements.Remove(s);
            }
            else
            {
                for (int i = 0; i < children.Count; i++)
                {
                    if (children[i].elements.Count != 0 && children[i].elements.Contains(s))
                    {
                        children[i].elements.Remove(s);
                    }
                }
            }

            if(elements.Contains(s)){
                Console.Write("Shape was not deleted");
            }
            //pre-condition for the merge methode
            if(count() <= minElements)
            {
                merge();
            }
        } 

        // Split the tree in four new subtrees
        public void split()
        {
            children.Add(new Quadtree(new Rectangle(new Point 
                (area.getShapeInformation().Item1.getX(), area.getShapeInformation().Item1.getY()), 
                    area.getShapeInformation().Item2 /2, area.getShapeInformation().Item3 /2),this.minElements, this.maxElements));

            children.Add(new Quadtree(new Rectangle(new Point
                (area.getShapeInformation().Item1.getX() + area.getShapeInformation().Item2 /2, 
                    area.getShapeInformation().Item1.getY()),
                        area.getShapeInformation().Item2 / 2, area.getShapeInformation().Item3 / 2), this.minElements, this.maxElements));

            children.Add(new Quadtree(new Rectangle(new Point
                (area.getShapeInformation().Item1.getX(), area.getShapeInformation().Item1.getY() + 
                    area.getShapeInformation().Item3 /2),
                        area.getShapeInformation().Item2 / 2, area.getShapeInformation().Item3 / 2), this.minElements, this.maxElements));

            children.Add(new Quadtree(new Rectangle(new Point
                (area.getShapeInformation().Item1.getX() + area.getShapeInformation().Item2 / 2, 
                    area.getShapeInformation().Item1.getY() + area.getShapeInformation().Item3 / 2),
                        area.getShapeInformation().Item2 / 2, area.getShapeInformation().Item3 / 2), this.minElements, this.maxElements));


            //add the single elements to the new QuadTree  
            for(int i= 0; i< children.Count; i++)
            {
                for(int j = 0; j < elements.Count; j++)
                {
                    if (children[i].inside(elements[j]))
                    {
                        children[i].insertShape(elements[j]);
                    }
                }
            }
            this.elements.Clear();

            if(!(this.elements.Count == 0)){
                Console.Write("The Quadtree wasn't splited");
            }

        }

        //if the min limit of elements reached the tree gets merged
        public void merge()
        {
            for (int i = 0; i < children.Count; i++)
            {
                if (children[i].children.Count > 0)
                { 
                    children[i].merge();
                }
                else
                {
                    if(children[i].elements.Count < minElements)
                    {
                        for(int j = 0; j < children[i].elements.Count; j++)
                        {
                            this.insertShape(children[i].elements[j]);

                        }
                       children[i].elements.Clear();
                    }
                }
            }
            children.Clear();

            if(!(children.Count == 0)){
                Console.Write("The merge didn't work");
            }
        }

        /*
        *checks if s is left, right, over or under our Area, returns true if shape is inside Area 
        */
        public bool inside(Shape s)
        {
            
            return !(s.boundingBox().getShapeInformation().Item1.getX()+ s.boundingBox().getShapeInformation().Item2 < this.area.getShapeInformation().Item1.getX()
            || s.boundingBox().getShapeInformation().Item1.getX() > this.area.getShapeInformation().Item1.getX() + this.area.getShapeInformation().Item2
                ||s.boundingBox().getShapeInformation().Item1.getY() + s.boundingBox().getShapeInformation().Item3 < this.area.getShapeInformation().Item1.getY()
                   || s.boundingBox().getShapeInformation().Item1.getY()> this.area.getShapeInformation().Item1.getY() + this.area.getShapeInformation().Item3);

        }

        //with using the intersects methode of a shape, it gives all the 
        //Shapes that collide with Shape s
        public List<Shape> collision(Shape s)
        {
            List<Shape> collidedShapes = new List<Shape>();

           foreach(Quadtree child in children)
            {
                for (int i = 0; i < child.elements.Count; i++)
                {
                    if(!child.elements[i].Equals(s) && child.elements[i].intersects(s))
                    {
                        collidedShapes.Add(child.elements[i]);
                    }
                }
            }
            return collidedShapes;
        }

    }
}
