﻿using System;
namespace QuadTree
{
    public abstract class Shape : Shapeable
    {
        //Does not have/need a constructor in this instance, because it is only further
        //implemented/actually defined in the lower classes


        public abstract int getId();
        //Makes a smallest possible quad to enclose the shape
        public abstract Rectangle boundingBox();

        //implements the interface Shapable just abstract
        public abstract Shape getShape();


        //checks if the bounding box of the shape intersect with the 
        //bounding box of the given shape
        //Note: abstract methodes have to be public
        public abstract bool intersects(Shape s);


        
    }
}
