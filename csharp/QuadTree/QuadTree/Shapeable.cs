﻿using System;
namespace QuadTree
{
    public interface Shapeable
    {
       Shape getShape();
    }
}
