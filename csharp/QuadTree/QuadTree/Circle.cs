﻿namespace QuadTree
{
    public class Circle : Shape
    {
        private float radius;
        private Point center;
        private int id;

        public Circle(Point center, float radius, int id)
        {
            this.center = center;
            this.radius = radius;
            this.id = id;
        }


        public override int getId()
        {
            return id;
        }
        //create an new rectangle as boundingbox and return this.
        public override Rectangle boundingBox()
        {
            Rectangle boundBox = new Rectangle(new Point(this.center.getX()-radius,this.center.getY()-radius), 2*radius, 2*radius);
            return boundBox;
        }


        // returns this shape
        public override Shape getShape()
        {
            return this;
        }

        //check if the boundingbox of this Object and s are overlapping
        public override bool intersects(Shape s)
        {
            return this.boundingBox().intersects(s);
        }
    }
}
